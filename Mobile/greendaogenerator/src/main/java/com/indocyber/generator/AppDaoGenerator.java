package com.indocyber.generator;
import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class AppDaoGenerator {
    public static void main(String[] args) {
        Schema schema=new Schema(1,"com.indocyber.dipostarsurvey.db");
        schema.enableKeepSectionsByDefault();
        addTables(schema);
        try {
            new DaoGenerator().generateAll(schema,"../app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static void addTables(final Schema schema) {
        addTrackingTable(schema);
        addLoginAuditTable(schema);
        addOrderEntryTable(schema);
        addFormTable(schema);
        addParamsTable(schema);
        addPostCodeTable(schema);
    }
    private static Entity addPostCodeTable(final Schema schema){
        Entity postCode=schema.addEntity("postcode");
        postCode.addStringProperty("prov_param");
        postCode.addStringProperty("kab_param");
        postCode.addStringProperty("kec_param");
        postCode.addStringProperty("kel_param");
        postCode.addStringProperty("post_code");
        return postCode;
    }
    private static Entity addParamsTable(final Schema schema) {
        Entity params = schema.addEntity("params");
        params.addStringProperty("param_id").primaryKey().notNull();
        params.addStringProperty("param_condition");
        params.addStringProperty("param_description");
        params.addStringProperty("parent_id");
        params.addLongProperty("order");
        return params;
    }
    private static Entity addFormTable(Schema schema){
        Entity forms=schema.addEntity("forms");
        forms.addStringProperty("form_id").primaryKey().notNull();
        forms.addStringProperty("form_name");
        forms.addStringProperty("form_label");
        forms.addIntProperty("form_order");
        Entity rows=addRowTable(schema);
        forms.addToMany(rows,rows.getProperties().get(2));
        return forms;
    }
    private static Entity addRowTable(Schema schema){
        Entity rows=schema.addEntity("rows");
        rows.addStringProperty("row_id").primaryKey().notNull();
        rows.addIntProperty("row_order");
        rows.addStringProperty("row_visibility");
        rows.addStringProperty("form_id");
        Entity field=addFieldTable(schema);
        rows.addToMany(field,field.getProperties().get(9));
        return rows;
    }
    private static Entity addFieldTable(Schema schema){
        Entity rows=schema.addEntity("fields");
        rows.addStringProperty("field_id").primaryKey().notNull();
        rows.addStringProperty("field_name");
        rows.addStringProperty("field_type");
        rows.addStringProperty("field_visibility");
        rows.addStringProperty("field_label");
        rows.addStringProperty("field_global_value");
        rows.addStringProperty("field_weight");
        rows.addStringProperty("field_store");
        rows.addStringProperty("field_extra");
        rows.addStringProperty("row_id");
        Entity events=addEventTable(schema);
        rows.addToMany(events,events.getProperties().get(3));
        return rows;
    }
    private static Entity addEventTable(Schema schema){
        Entity events=schema.addEntity("events");
        events.addStringProperty("event_id").primaryKey().notNull();
        events.addStringProperty("event_type");
        events.addStringProperty("event_component");
        events.addStringProperty("event_component_target");
        events.addStringProperty("event_data");
        events.addStringProperty("form_id");
        return events;
    }
    private static Entity addTrackingTable(final Schema schema) {
        Entity tracking = schema.addEntity("Tracking");
        tracking.addLongProperty("tracking_id").primaryKey().notNull();
        tracking.addStringProperty("tracking_date");
        tracking.addStringProperty("position");
        tracking.addStringProperty("user_id");
        return tracking;
    }
    private static Entity addLoginAuditTable(final Schema schema) {
        Entity log = schema.addEntity("login_logs");
        log.addLongProperty("log_id").primaryKey().autoincrement().notNull();
        log.addStringProperty("login_date");
        log.addStringProperty("login");
        log.addStringProperty("logout");
        log.addStringProperty("user_id");
        return log;
    }
    private static Entity addOrderEntryTable(final Schema schema) {
        Entity order = schema.addEntity("order_entries");
        order.addStringProperty("order_id").primaryKey().notNull();
        order.addStringProperty("fpp_no");
        order.addStringProperty("nama_pemesan");
        order.addStringProperty("alamat");
        order.addStringProperty("nohp");
        order.addStringProperty("notes");
        order.addStringProperty("attachment");
        order.addStringProperty("created_date");
        order.addDateProperty("inserted_date");
        order.addStringProperty("plan_date");
        order.addBooleanProperty("enabled");
        order.addStringProperty("user_id");
        order.addStringProperty("spk_filename");
        order.addStringProperty("last_data");
        Entity activity=addActivityTable(schema);
        order.addToMany(activity,activity.getProperties().get(1));
        return order;
    }
    private static Entity addActivityTable(final Schema schema) {
        Entity activity = schema.addEntity("activities");
        activity.addStringProperty("activity_id").primaryKey().notNull();
        activity.addStringProperty("order_id").notNull();
        activity.addStringProperty("fpp_no");
        activity.addStringProperty("nama_pemesan");
        activity.addStringProperty("alamat");
        activity.addStringProperty("nohp");
        activity.addStringProperty("notes");
        activity.addStringProperty("attachment");
        activity.addStringProperty("body").notNull();
        activity.addDateProperty("created_date");
        activity.addDateProperty("updated_date");
        activity.addDateProperty("plan_date");
        activity.addStringProperty("send_status");
        activity.addStringProperty("mobile_status");
        activity.addIntProperty("is_upload");
        activity.addStringProperty("user_id");
        Entity log=addActivityLogsTable(schema);
        activity.addToMany(log,log.getProperties().get(1));
        return activity;
    }
    private static Entity addActivityLogsTable(final Schema schema) {
        Entity log = schema.addEntity("activity_logs");
        log.addStringProperty("log_id").primaryKey().notNull();
        log.addStringProperty("activity_id").notNull();
        log.addDateProperty("log_date");
        log.addStringProperty("status");
        log.addStringProperty("user_id");
        return log;
    }
}
