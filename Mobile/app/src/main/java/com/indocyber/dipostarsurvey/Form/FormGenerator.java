package com.indocyber.dipostarsurvey.Form;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.Activity.FormInput;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Library.Calculate;
import com.indocyber.dipostarsurvey.Library.Dialog;
import com.indocyber.dipostarsurvey.Library.Formatting;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.UI.KangEventHandler;
import com.indocyber.dipostarsurvey.UI.RowsUI;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.events;
import com.indocyber.dipostarsurvey.db.eventsDao;
import com.indocyber.dipostarsurvey.db.postcode;
import com.indocyber.dipostarsurvey.db.postcodeDao;
import com.indocyber.dipostarsurvey.db.rows;
import com.indocyber.dipostarsurvey.db.rowsDao;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Indocyber on 31/10/2017.
 */

public class FormGenerator extends Fragment implements Step,KangEventHandler {
    private String formID;
    private LinearLayout mainLayout;
    public HashMap<String,JSONObject> validationList;
    private List<events> eventsList;
    private DaoSession daoSession;
    public FormGenerator() {
    }
    public static FormGenerator newInstance(String formID) {
        FormGenerator fragment = new FormGenerator();
        Bundle args=new Bundle();
        args.putString("form_id",formID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        formID=getArguments().getString("form_id");
        validationList=new HashMap<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final AppActivity appActivity=((AppActivity)getActivity());
        daoSession=((App)appActivity.getApplication()).getDbSession();
        rowsDao rowsDao=daoSession.getRowsDao();
        eventsDao eventsDao=daoSession.getEventsDao();
        Query<rows> rowsQuery=rowsDao.queryBuilder()
                .where(com.indocyber.dipostarsurvey.db.rowsDao.Properties.Form_id.eq(formID))
                .orderAsc(com.indocyber.dipostarsurvey.db.rowsDao.Properties.Row_order)
                .build();
        Query<events> eventsQuery=eventsDao.queryBuilder()
                .where(com.indocyber.dipostarsurvey.db.eventsDao.Properties.Form_id.eq(formID))
                .build();
        eventsList=eventsQuery.list();
        ScrollView scrollView=new ScrollView(appActivity);
        LinearLayout.LayoutParams scrollParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,1);
        scrollView.setLayoutParams(scrollParams);
        mainLayout=new LinearLayout(appActivity);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        mainLayout.setLayoutParams(layoutParams);
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        List<rows> rowsList=rowsQuery.list();
        for (int i=0;i<rowsList.size();i++){
            if(((FormInput)getActivity()).readonly){
                View v=RowsUI.create(appActivity,mainLayout,rowsList.get(i),false,this,this);
                if(v!=null){
                    mainLayout.addView(v);
                }
            }else {
                View v=RowsUI.create(appActivity,mainLayout,rowsList.get(i),true,this,this);
                if(v!=null){
                    mainLayout.addView(v);
                }
            }
        }
        mainLayout.setPadding(0,0,0,30);
        scrollView.addView(mainLayout);
        return scrollView;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {

        Set<String> validKeys=validationList.keySet();
        for (String s : validKeys) {
            try {
                boolean required=validationList.get(s).getBoolean("required");
                String required_msg=validationList.get(s).getString("required_message");
                String regex=validationList.get(s).getString("regex");
                String regex_msg=validationList.get(s).getString("regex_message");
                if(required){
                    if(!((AppActivity)getActivity()).tempDataStore.containsKey(s)){
                        return new VerificationError(required_msg);
                    }else {
                        if(((AppActivity)getActivity()).tempDataStore.get(s).equals("")){
                            return new VerificationError(required_msg);
                        }
                    }
                }
                if(!regex.equals("")){
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(((AppActivity)getActivity()).tempDataStore.get(s));
                    if (matcher.find()) {
                        return new VerificationError(regex_msg);
                    }
                }
            } catch (JSONException e) {
                FirebaseCrash.log(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onUIEvent(View view, String event, String tagUI, String values) {
        ((FormInput)getActivity()).autoSave();
        for (int i=0;i<eventsList.size();i++) {
            events eventData = eventsList.get(i);
            if (event.equals(eventData.getEvent_type()) && tagUI.equals(eventData.getEvent_component())) {
//                Log.i("EVENT",event+" => "+tagUI+"  \n "+values);
//                Log.i("EVENT HANDLE",eventData.getEvent_type()+" => "+eventData.getEvent_component_target());
                try {
                    JSONObject jsonObject = new JSONObject(eventData.getEvent_data());
                    switch (jsonObject.getString("type")) {
                        case "count":
                            EditText toCal = ((EditText) mainLayout.findViewWithTag(eventData.getEvent_component_target()));
                            toCal.setFilters(new InputFilter[]{new InputFilter.LengthFilter(23)});
                            toCal.setText(Formatting.currency(Calculate.exec(jsonObject.getString("formula"),
                                    ((AppActivity) getActivity()).tempDataStore,
                                    jsonObject.getJSONArray("varList"))));
                            break;
                        case "set_location":
                            EditText toSetLoc = ((EditText) mainLayout.findViewWithTag(eventData.getEvent_component_target()));
                            toSetLoc.setText(((App)getActivity().getApplicationContext()).location);
                            break;
                        case "set_postcode":
                            if (((AppActivity) getActivity()).tempDataStore
                                    .containsKey(jsonObject.getString("prov_param"))
                                    &&
                                    ((AppActivity) getActivity()).tempDataStore
                                            .containsKey(jsonObject.getString("kab_param"))
                                    &&
                                    ((AppActivity) getActivity()).tempDataStore
                                            .containsKey(jsonObject.getString("kec_param"))
                                    &&
                                    ((AppActivity) getActivity()).tempDataStore
                                            .containsKey(jsonObject.getString("kel_param"))
                                    ) {

                                postcodeDao postcodeDao = daoSession.getPostcodeDao();
                                Query<postcode> postcodeQuery = postcodeDao.queryBuilder()
                                        .where(com.indocyber.dipostarsurvey.db.postcodeDao.Properties.Prov_param.eq(((AppActivity) getActivity()).tempDataStore
                                                .get(jsonObject.getString("prov_param"))))
                                        .where(com.indocyber.dipostarsurvey.db.postcodeDao.Properties.Kab_param.eq(((AppActivity) getActivity()).tempDataStore
                                                .get(jsonObject.getString("kab_param"))))
                                        .where(com.indocyber.dipostarsurvey.db.postcodeDao.Properties.Kec_param.eq(((AppActivity) getActivity()).tempDataStore
                                                .get(jsonObject.getString("kec_param"))))
                                        .where(com.indocyber.dipostarsurvey.db.postcodeDao.Properties.Kel_param.eq(((AppActivity) getActivity()).tempDataStore
                                                .get(jsonObject.getString("kel_param"))))
                                        .build();
                                List<postcode> postcodeList = postcodeQuery.list();
                                if (postcodeList.size() > 0) {
                                    EditText toSetPostCode = ((EditText) mainLayout.findViewWithTag(eventData.getEvent_component_target()));
                                    toSetPostCode.setText(postcodeList.get(0).getPost_code());
                                }
                            }
                            break;
                        case "enable":
                            View toEna = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            toEna.setEnabled(false);
                            toEna.setClickable(false);
                            break;
                        case "enable_when":
                            View toEnable = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if (toEnable != null) {
                                if (((AppActivity) getActivity()).tempDataStore
                                        .containsKey(jsonObject.getString("key"))) {
                                    if (((AppActivity) getActivity()).tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        toEnable.setEnabled(true);
                                        toEnable.setClickable(true);
                                        if (toEnable instanceof Spinner) {
                                            LinearLayout layout = (LinearLayout) mainLayout.findViewWithTag(eventData.getEvent_component_target() + "-main");
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white_focus, null));
                                            } else {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white_focus));
                                            }
                                        }
                                    } else {
                                        toEnable.setEnabled(false);
                                        toEnable.setClickable(false);
                                        if (toEnable instanceof Spinner) {
                                            LinearLayout layout = (LinearLayout) mainLayout.findViewWithTag(eventData.getEvent_component_target() + "-main");
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white, null));
                                            } else {
                                                layout.setBackground(getActivity().getResources()
                                                        .getDrawable(R.drawable.ic_rounded_white));
                                            }

                                        }
                                    }
                                }
                            }
                            if (mainLayout.findViewWithTag(eventData.getEvent_component_target()).getClass() == RadioGroup.class) {
                                ViewGroup toEnables = (ViewGroup) mainLayout.findViewWithTag(eventData.getEvent_component_target());
                                if (toEnables != null) {
                                    if (((AppActivity) getActivity()).tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        enableChilds(toEnables, true);
                                    } else {
                                        enableChilds(toEnables, false);
                                    }
                                }
                            }
                            break;
                        case "disable":
                            View toDis = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            toDis.setEnabled(false);
                            toDis.setClickable(false);
                            break;
                        case "disable_when":
                            View toDisable = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if (toDisable != null) {
                                if (((AppActivity) getActivity()).tempDataStore
                                        .containsKey(jsonObject.getString("key"))) {
                                    if (((AppActivity) getActivity()).tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        toDisable.setEnabled(false);
                                        toDisable.setClickable(false);
                                    } else {
                                        toDisable.setEnabled(true);
                                        toDisable.setClickable(true);
                                    }
                                }
                            }
                            if (mainLayout.findViewWithTag(eventData.getEvent_component_target()).getClass() == RadioGroup.class) {
                                ViewGroup toDisables = (ViewGroup) mainLayout.findViewWithTag(eventData.getEvent_component_target());
                                if (toDisables != null) {
                                    if (((AppActivity) getActivity()).tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        enableChilds(toDisables, false);
                                    } else {
                                        enableChilds(toDisables, true);
                                    }
                                }
                            }
                            break;
                        //TODO : INSERT DB
                        case "invisible":
                            View toInvis = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            toInvis.setVisibility(View.GONE);
                            break;
                        case "invisible_when":
                            View toInvisWhen = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if(((AppActivity) getActivity()).tempDataStore
                                    .containsKey(jsonObject.getString("key"))) {
                                if (((AppActivity) getActivity()).tempDataStore
                                        .containsKey(jsonObject.getString("key"))) {
                                    if (((AppActivity) getActivity()).tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        toInvisWhen.setVisibility(View.GONE);
                                    } else {
                                        toInvisWhen.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                            break;
                        case "visible":
                            View vis = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            vis.setVisibility(View.VISIBLE);
                            break;
                        case "visible_when":
                            View visWhen = mainLayout.findViewWithTag(eventData.getEvent_component_target());
                            if(((AppActivity) getActivity()).tempDataStore
                                    .containsKey(jsonObject.getString("key"))){
                                    Log.i("MUST",jsonObject.getString("key")
                                            +" = "+
                                            jsonObject.getString("value"));
                                    Log.i("CURRENT",jsonObject.getString("key")+" = "+
                                            ((AppActivity) getActivity()).tempDataStore
                                                    .get(jsonObject.getString("key")));
                                    if (((AppActivity) getActivity()).tempDataStore
                                            .get(jsonObject.getString("key"))
                                            .equals(jsonObject.getString("value"))) {
                                        if(visWhen.getClass().equals(Button.class)){
                                            visWhen.setVisibility(View.VISIBLE);
                                        }else{
                                            View visWhenMain = mainLayout.findViewWithTag(
                                                    eventData.getEvent_component_target()+"-layout");
                                            if(visWhenMain!=null){
                                                visWhenMain.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    } else {
                                        if(visWhen.getClass().equals(Button.class)){
                                            visWhen.setVisibility(View.GONE);
                                        }else{
                                            View visWhenMain = mainLayout.findViewWithTag(
                                                    eventData.getEvent_component_target()+"-layout");
                                            if(visWhenMain!=null){
                                                visWhenMain.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                }else{
                                    Log.i("NO VALUE",jsonObject.getString("key"));
                                }
                            break;
                        case "alert":
                            Dialog.alert(getActivity(), this, jsonObject.getString("message"), jsonObject.getString("tag"));
                            break;
                        case "confirm":
                            Dialog.confirm(getActivity(), this,
                                    jsonObject.getString("message"),
                                    jsonObject.getString("yes"),
                                    jsonObject.getString("no"),
                                    jsonObject.getString("tag"));
                            break;
                        case "submit":
                            ((FormInput) getActivity()).save();
                            break;
                        case "save":
                            ((FormInput) getActivity()).saveActivity("DRAFT");
                            ((FormInput) getActivity()).finish();
                            break;
                        case "cancel":
                            ((FormInput) getActivity()).backDialog();
                            break;
                        case "tunda_kunjungan":
                            ((FormInput) getActivity()).tundaKunjungan();
                            break;
                        case "pickup":
                            ((FormInput) getActivity()).setupPickUp();
                            break;

                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
        }
    }
    public void enableChilds(ViewGroup viewGroup,boolean enable){
        final int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = viewGroup.getChildAt(i);
            child.setEnabled(enable);
        }
    }
}
