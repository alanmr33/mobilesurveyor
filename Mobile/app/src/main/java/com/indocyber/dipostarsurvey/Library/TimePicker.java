package com.indocyber.dipostarsurvey.Library;

import android.app.*;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.indocyber.dipostarsurvey.R;

/**
 * Created by Indocyber on 14/12/2017.
 */

public class TimePicker extends DialogFragment {
    private  String time;
    private OnSetValue setValue;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        final android.app.Dialog dialog=new android.app.Dialog(getActivity());
        dialog.setContentView(R.layout.timepicker);
        final android.widget.TimePicker timePicker= (android.widget.TimePicker) dialog.findViewById(R.id.timePicker);
        Button btnOK= (Button) dialog.findViewById(R.id.buttonDPOK);
        Button btnCancel= (Button) dialog.findViewById(R.id.buttonDPCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    String hour=(timePicker.getHour()+1>=10)? String.valueOf(timePicker.getHour()+1) : "0"+String.valueOf(timePicker.getHour()+1);
                    String minute=(timePicker.getMinute()+1>=10)? String.valueOf(timePicker.getMinute()+1) : "0"+String.valueOf(timePicker.getMinute()+1);
                    time=hour+":"+minute;
                    setValue.OnSet(time);
                }else {
                    String hour=(timePicker.getCurrentHour()+1>=10)? String.valueOf(timePicker.getCurrentHour()+1) : "0"+String.valueOf(timePicker.getCurrentHour()+1);
                    String minute=(timePicker.getCurrentMinute()+1>=10)? String.valueOf(timePicker.getCurrentMinute()+1) : "0"+String.valueOf(timePicker.getCurrentMinute()+1);
                    time=hour+":"+minute;
                    setValue.OnSet(time);
                }
                dialog.dismiss();
            }
        });
        return dialog;
    }
    public void setOnSetvalue(OnSetValue setValue){
        this.setValue=setValue;
    }
    public String getTime(){
        return time;
    }
}
