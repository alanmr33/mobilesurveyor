package com.indocyber.dipostarsurvey.UI;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.fields;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Indocyber on 31/10/2017.
 */

abstract class ButtonUI {

    public static final Button create(Activity activity, View view, final fields props, boolean enable, final KangEventHandler eventHandler, Fragment fragment){
        final Button component=new Button(activity);
        float weight=1;
        if(!props.getField_weight().replace(",",".").equals("")){
            weight=Float.parseFloat(props.getField_weight().replace(",","."));
        }
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,weight);
        layoutParams.bottomMargin=2;
        layoutParams.topMargin=2;
        layoutParams.leftMargin=2;
        layoutParams.rightMargin=2;
        component.setLayoutParams(layoutParams);
        if(props.getField_visibility().equalsIgnoreCase("visible")){
            component.setVisibility(View.VISIBLE);
        }else{
            component.setVisibility(View.GONE);
        }
        if(!enable){
            component.setVisibility(View.GONE);
        }
        component.setTag(props.getField_name());
        component.setText(props.getField_label());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            component.setTextColor(activity.getResources().getColor(R.color.colorWhite,null));
        }else {
            component.setTextColor(activity.getResources().getColor(R.color.colorWhite));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            component.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark, null));
        }else {
            component.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }
        try {
            JSONObject extras = new JSONObject(props.getField_extra());
            if(extras.has("color")){
                switch (extras.getString("color")){
                    case "primary" :
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setTextColor(activity.getResources().getColor(R.color.colorWhite,null));
                        }else {
                            component.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark, null));
                        }else {
                            component.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                        }
                        break;
                    case "secondary" :
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setTextColor(activity.getResources().getColor(R.color.colorWhite,null));
                        }else {
                            component.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setBackgroundColor(activity.getResources().getColor(R.color.colorSecondaryDark, null));
                        }else {
                            component.setBackgroundColor(activity.getResources().getColor(R.color.colorSecondaryDark));
                        }
                        break;
                    case "gray" :
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setTextColor(activity.getResources().getColor(R.color.colorWhite,null));
                        }else {
                            component.setTextColor(activity.getResources().getColor(R.color.colorWhite));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setBackgroundColor(activity.getResources().getColor(R.color.colorGray, null));
                        }else {
                            component.setBackgroundColor(activity.getResources().getColor(R.color.colorGray));
                        }
                        break;
                    case "none" :
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setTextColor(activity.getResources().getColor(R.color.colorGray,null));
                        }else {
                            component.setTextColor(activity.getResources().getColor(R.color.colorGray));
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            component.setBackgroundColor(activity.getResources().getColor(android.R.color.transparent, null));
                        }else {
                            component.setBackgroundColor(activity.getResources().getColor(android.R.color.transparent));
                        }
                        break;
                }
            }
        }catch (JSONException e){
            FirebaseCrash.log(e.getMessage());
        }
        component.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventHandler.onUIEvent(component,"click",props.getField_name(),"");
            }
        });
        if(enable) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    eventHandler.onUIEvent(component, "init", props.getField_name(), "");
                }
            }, 1000);
        }
        return component;
    }
}
