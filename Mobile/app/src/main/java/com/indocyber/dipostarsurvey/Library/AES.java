package com.indocyber.dipostarsurvey.Library;
import com.google.firebase.crash.FirebaseCrash;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Indocyber on 23/10/2017.
 */
public class AES {
    public static final byte[] encBytes(byte[] srcBytes, byte[] key,
                                        byte[] newIv) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec iv = new IvParameterSpec(newIv);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(srcBytes);
        return encrypted;
    }

    public static final String encText(String sSrc, byte[] key, byte[] newIv)
            throws Exception {
        byte[] srcBytes = sSrc.getBytes("utf-8");
        byte[] encrypted = encBytes(srcBytes, key, newIv);
        return Base64.encode(encrypted);
    }

    public static final byte[] decBytes(byte[] srcBytes, byte[] key,
                                        byte[] newIv) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec iv = new IvParameterSpec(newIv);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(srcBytes);
        return encrypted;
    }

    public static final String decText(String sSrc, byte[] key, byte[] newIv)
            throws Exception {
        byte[] srcBytes = Base64.decode(sSrc);
        byte[] decrypted = decBytes(srcBytes, key, newIv);
        return new String(decrypted, "utf-8");
    }
    /*
    key 16 digit
     */
    public static String encryptText(String s,String key){
        byte[] ivk = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        try {
            return encText(s,key.getBytes("UTF-8"),ivk);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
        return "";
    }
    public static String decryptText(String s,String key){
        byte[] ivk = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        try {
            return decText(s,key.getBytes("UTF-8"),ivk);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
        return "";
    }
    public static String generateText(String text,int counter){
        counter=999999;
        Random random=new Random();
        counter=random.nextInt(counter);
        String counTXT;
        if(counter<=9){
            counTXT="00000"+String.valueOf(counter);
        }else if(counter>=10 && counter<=99){
            counTXT="0000"+String.valueOf(counter);
        }else if(counter>=100 && counter <=999){
            counTXT="000"+String.valueOf(counter);
        }else if(counter>=1000 && counter<=9999){
            counTXT="00"+String.valueOf(counter);
        }else if(counter>=10000 &&counter <=99999){
            counTXT="0"+String.valueOf(counter);
        }else{
            counTXT=String.valueOf(counter);
        }
        String key = "alanrahman"+counTXT;
        String regex = "(?i)((?:=|U\\s*R\\s*L\\s*\\()\\s*[^>]*\\s*S\\s*C\\s*R\\s*I\\s*P\\s*T\\s*:|&colon;|[\\s\\S]allowscriptaccess[\\s\\S]|[\\s\\S]src[\\s\\S]|[\\s\\S]data:text\\/html[\\s\\S]|[\\s\\S]xlink:href[\\s\\S]|[\\s\\S]base64[\\s\\S]|[\\s\\S]xmlns[\\s\\S]|[\\s\\S]xhtml[\\s\\S]|[\\s\\S]style[\\s\\S]|<style[^>]*>[\\s\\S]*?|[\\s\\S]@import[\\s\\S]|<applet[^>]*>[\\s\\S]*?|<meta[^>]*>[\\s\\S]*?|<object[^>]*>[\\s\\S]*?)";
        String string = encryptText(text,key);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(string);

        if (!matcher.find()) {
            return string+"!"+key;
        }else{
            return generateText(text,counter+1);
        }

    }
    public static String realText(String text){
        String[] txt=text.split("!");
        return decryptText(txt[0],txt[1]);
    }
}
