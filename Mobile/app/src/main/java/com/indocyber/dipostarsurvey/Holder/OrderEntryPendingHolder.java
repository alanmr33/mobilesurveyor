package com.indocyber.dipostarsurvey.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.dipostarsurvey.R;

/**
 * Created by Indocyber on 26/07/2017.
 */

public class OrderEntryPendingHolder extends RecyclerView.ViewHolder {
    public TextView txtOrderID, txtFppNo,txtNama,txtAlamat,txtNoHP,txtPlanDate,txtStatus;
    public LinearLayout mainLayout;
    public OrderEntryPendingHolder(View itemView) {
        super(itemView);
        txtPlanDate= (TextView) itemView.findViewById(R.id.txtPlanDate);
        txtOrderID= (TextView) itemView.findViewById(R.id.txtNewOrderID);
        txtFppNo = (TextView) itemView.findViewById(R.id.txtFppNo);
        txtNama= (TextView) itemView.findViewById(R.id.txtNewOrderNama);
        txtAlamat= (TextView) itemView.findViewById(R.id.txtNewOrderAlamat);
        txtNoHP= (TextView) itemView.findViewById(R.id.txtNewOrderNoHP);
        txtStatus= (TextView) itemView.findViewById(R.id.txtStatus);
        mainLayout= (LinearLayout) itemView.findViewById(R.id.itemNewOrder);
    }
}
