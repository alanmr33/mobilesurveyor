package com.indocyber.dipostarsurvey.Library;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;

import com.google.firebase.crash.FirebaseCrash;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Indocyber on 01/11/2017.
 */

public abstract class FileCreator {
    public static final File createImages(Activity activity,String fileName,String orderID){
        File storageDir = activity.getExternalFilesDir("images/"+orderID.replace("/","_"));
        File file = null;
        try {
            if(!storageDir.exists()){
                storageDir.exists();
            }
            file = new File(storageDir.getAbsolutePath()+"/"+fileName+".cam");
            if(!file.exists()){
                file.createNewFile();
            }
        } catch (IOException e) {
            FirebaseCrash.log(e.getMessage());
        }
        return file;
    }
    public static final File createSPK(Context ctx, String fileName, boolean isPDF){
        String ext=(isPDF)? ".pdf" : ".jpg";
        File storageDir = ctx.getExternalFilesDir("spk-files");
        File file = null;
        try {
            if(!storageDir.exists()){
                storageDir.exists();
            }
            file = new File(storageDir.getAbsolutePath()+"/"+fileName+ext);
            if(!file.exists()){
                file.createNewFile();
            }
        } catch (IOException e) {
            FirebaseCrash.log(e.getMessage());
        }
        return file;
    }

}
