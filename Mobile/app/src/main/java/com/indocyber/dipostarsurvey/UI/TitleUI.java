package com.indocyber.dipostarsurvey.UI;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.db.fields;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class TitleUI {
    public static final View create(Activity activity, View view, final fields props, boolean enable, final KangEventHandler eventHandler, Fragment fragment){
        final TextView component=new TextView(activity);
        if(props.getField_visibility().equalsIgnoreCase("visible")){
            component.setVisibility(View.VISIBLE);
        }else{
            component.setVisibility(View.GONE);
        }
        if(!enable){
            component.setEnabled(false);
        }
        component.setTextSize(18f);
        component.setTypeface(Typeface.DEFAULT_BOLD);
        JSONObject extras= null;
        try {
            extras = new JSONObject(props.getField_extra());
            if(extras.has("font-size")){
                component.setTextSize(Float.parseFloat(extras.getString("font-size")));
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        component.setTag(props.getField_name());
        component.setText(props.getField_label());
        float weight=1;
        if(!props.getField_weight().replace(",",".").equals("")){
            weight=Float.parseFloat(props.getField_weight().replace(",","."));
        }
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,weight);
        component.setLayoutParams(layoutParams);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                eventHandler.onUIEvent(component,"init",props.getField_name(),"");
            }
        },1000);
        return component;
    }
}
