package com.indocyber.dipostarsurvey.Form;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Activity.DetailKTP;
import com.indocyber.dipostarsurvey.Activity.DetailSPK;
import com.indocyber.dipostarsurvey.Activity.FormInput;
import com.indocyber.dipostarsurvey.Activity.Main;
import com.indocyber.dipostarsurvey.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.view.View.GONE;

public class Form1 extends Fragment implements Step {
    private EditText txtFPPNO,txtNamaPemesan,txtNoHP,txtAlamat,txtNotes,txtPlanDate,txtOrderID;
    private Button nextForm,cancelBtn,detailSPK;
    private TextView txtTipePemesan;
    private RadioGroup formType,actType;
    public Form1() {
    }
    public static Form1 newInstance() {
        Form1 fragment = new Form1();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        View view=inflater.inflate(R.layout.fragment_form1, container, false);
        txtNotes= (EditText) view.findViewById(R.id.edt_notes);
        txtTipePemesan= (TextView) view.findViewById(R.id.txtTipePemesan);
        txtNotes.setEnabled(false);
        txtNotes.setText(((FormInput)getActivity()).orderEntries.getNotes());
        txtOrderID= (EditText) view.findViewById(R.id.edt_orderID);
        txtOrderID.setEnabled(false);
        txtOrderID.setText(((FormInput)getActivity()).orderEntries.getOrder_id());
        txtFPPNO= (EditText) view.findViewById(R.id.edt_fppNo);
        txtFPPNO.setEnabled(false);
        txtFPPNO.setText(((FormInput)getActivity()).orderEntries.getFpp_no());
        txtPlanDate= (EditText) view.findViewById(R.id.edt_planDate);
        txtPlanDate.setEnabled(false);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formated=new SimpleDateFormat("dd MMM yyyy HH:mm");
        try {
            if(((FormInput)getActivity()).orderEntries.getPlan_date()!=null) {
                Date plan = dateFormat.parse(((FormInput) getActivity()).orderEntries.getPlan_date());
                txtPlanDate.setText(formated.format(plan));
            }else{
                txtPlanDate.setText("-");
            }
        } catch (ParseException e) {
            FirebaseCrash.log(e.getMessage());
        }
        txtNamaPemesan= (EditText) view.findViewById(R.id.edt_namaPemesan);
        txtNamaPemesan.setEnabled(false);
        txtNamaPemesan.setText(((FormInput)getActivity()).orderEntries.getNama_pemesan());
        txtNoHP= (EditText) view.findViewById(R.id.edt_noTelepon);
        txtNoHP.setEnabled(false);
        txtNoHP.setText(((FormInput)getActivity()).orderEntries.getNohp());
        txtAlamat= (EditText) view.findViewById(R.id.edt_alamatSurvey);
        txtAlamat.setEnabled(false);
        txtAlamat.setText(((FormInput)getActivity()).orderEntries.getAlamat());
        formType= (RadioGroup) view.findViewById(R.id.rg_tipePemesan);
        formType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if(checkedId==R.id.rb_perorangan){
                    ((FormInput)getActivity()).setCompanyForm(false);
                }else{
                    ((FormInput)getActivity()).setCompanyForm(true);
                }
            }
        });
        actType= (RadioGroup) view.findViewById(R.id.rg_tipeSurvey);
        actType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if(checkedId==R.id.rb_cancel){
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","66");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","FPP Dibatalkan");
                    if(((FormInput)getActivity()).orderEntries.getOrder_id().contains("SO")){
                        ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL1");
                    }else{
                        ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL2");
                    }
                }else if(checkedId==R.id.rb_survey){
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","65");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Survey Selesai");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL3");
                }else{
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","75");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Sign Off Document");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL4");
                }
            }
        });
        if(((FormInput)getActivity()).readonly){
           view.findViewById(R.id.rb_perorangan).setEnabled(false);
           view.findViewById(R.id.rb_perusahaan).setEnabled(false);
           view.findViewById(R.id.rb_cancel).setEnabled(false);
           view.findViewById(R.id.rb_survey).setEnabled(false);
           view.findViewById(R.id.rb_signoff).setEnabled(false);
        }
        if(((FormInput)getActivity()).tempDataStore.containsKey("SurveyStatus")){
            if(((FormInput)getActivity()).orderEntries.getOrder_id().contains("SO")){
                actType.check(R.id.rb_signoff);
                formType.setVisibility(GONE);
                view.findViewById(R.id.rb_survey).setVisibility(GONE);
                txtTipePemesan.setVisibility(GONE);
                if(((FormInput)getActivity()).tempDataStore.get("SurveyStatus").equals("66")){
                    actType.check(R.id.rb_cancel);
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","66");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Survey Dibatalkan");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL1");
                }else{
                    actType.check(R.id.rb_signoff);
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","75");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Sign Off Document");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL4");
                }
            }else{
                actType.check(R.id.rb_survey);
                formType.setVisibility(View.VISIBLE);
                view.findViewById(R.id.rb_signoff).setVisibility(GONE);
                txtTipePemesan.setVisibility(View.VISIBLE);
                if(((FormInput)getActivity()).tempDataStore.get("SurveyStatus").equals("66")){
                    actType.check(R.id.rb_cancel);
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","66");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Survey Dibatalkan");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL2");
                }else{
                    actType.check(R.id.rb_survey);
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","65");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Survey Selesai");
                    ((FormInput)getActivity()).tempDataStore.put("SurveyStatusLabel","SSL3");
                }
            }
        }else{
            if(((FormInput)getActivity()).orderEntries.getOrder_id().contains("SO")){
                actType.check(R.id.rb_signoff);
                ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","75");
                ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Sign Off Document");
                formType.setVisibility(GONE);
                view.findViewById(R.id.rb_survey).setVisibility(GONE);
                txtTipePemesan.setVisibility(GONE);
                ((RadioButton) view.findViewById(R.id.rb_cancel)).setText("Kontrak Dibatalkan");
            }else{
                actType.check(R.id.rb_survey);
                ((FormInput)getActivity()).tempDataStore.put("SurveyStatus","65");
                ((FormInput)getActivity()).tempDataStore.put("SurveyStatus-readable","Survey Selesai");
                formType.setVisibility(View.VISIBLE);
                view.findViewById(R.id.rb_signoff).setVisibility(GONE);
                txtTipePemesan.setVisibility(View.VISIBLE);
                ((RadioButton) view.findViewById(R.id.rb_cancel)).setText("Survey Dibatalkan");
            }
        }
        if(((FormInput)getActivity()).isCompany){
           formType.check(R.id.rb_perusahaan);
        }else{
           formType.check(R.id.rb_perorangan);
        }
        nextForm= (Button) view.findViewById(R.id.btn_next);
        nextForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FormInput)getActivity()).nextForm();
            }
        });
        detailSPK= (Button) view.findViewById(R.id.buttonSPK);
        if(((FormInput)getActivity()).orderEntries.getAttachment()==null){
            detailSPK.setText("Menunggu Lampiran");
            detailSPK.setVisibility(View.VISIBLE);
        }else if(((FormInput)getActivity()).orderEntries.getAttachment().equals("-")){
            detailSPK.setVisibility(GONE);
        }else if(((FormInput)getActivity()).orderEntries.getAttachment()!=null && !((FormInput)getActivity()).orderEntries.getAttachment().equals("-")){
            detailSPK.setVisibility(View.VISIBLE);
            detailSPK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent spk=new Intent(getActivity(), DetailSPK.class);
                    spk.putExtra("ORDER_ID",((FormInput)getActivity()).orderEntries.getOrder_id());
                    startActivity(spk);
                }
            });
        }
        cancelBtn= (Button) view.findViewById(R.id.btn_cancel);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FormInput)getActivity()).backDialog();
            }
        });
        if(((FormInput)getActivity()).orderEntries.getOrder_id().contains("SO")){
            ((RadioButton) view.findViewById(R.id.rb_cancel)).setText("Kontrak Dibatalkan");
        }else{
            actType.check(R.id.rb_survey);
            ((RadioButton) view.findViewById(R.id.rb_cancel)).setText("Survey Dibatalkan");
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
