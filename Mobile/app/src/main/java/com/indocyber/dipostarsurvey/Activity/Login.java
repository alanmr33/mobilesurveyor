package com.indocyber.dipostarsurvey.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Config.Global;
import com.indocyber.dipostarsurvey.Library.AES;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.login_logs;
import com.indocyber.dipostarsurvey.db.login_logsDao;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.indocyber.dipostarsurvey.db.order_entriesDao;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;

public class Login extends AppActivity implements View.OnClickListener {
    private Button btnLogin,btnSetting;
    private EditText txtUsername,txtPassword;
    private CheckBox showPassword;
    private TextView txtError;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*
        init component
         */
        txtError= (TextView) findViewById(R.id.textViewError);
        btnLogin= (Button) findViewById(R.id.buttonLogin);
        txtUsername= (EditText) findViewById(R.id.editTextLoginUserID);
        txtPassword= (EditText) findViewById(R.id.editTextLoginPassword);
        showPassword= (CheckBox) findViewById(R.id.checkBoxShow);
        showPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    txtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    txtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    txtPassword.setSelection(txtPassword.length());
                }
            }
        });
        //set onclick listener to this class
        btnLogin.setOnClickListener(this);
        loading=new ProgressDialog(this);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setCancelable(false);
    }
    /*
    for all click event below
     */
    @Override
    public void onClick(View v) {
        /*
        choose event by element
         */
        switch (v.getId()){
            case R.id.buttonLogin:
                login();
                break;
        }
    }
    public  void  login(){
        /*
                store text temp
                 */
        String username=txtUsername.getText().toString();
        String password=txtPassword.getText().toString();
                /*
                validate input
                 */
        if (username.equals("")){
            txtError.setText("User ID harus diisi");
            txtError.setVisibility(View.VISIBLE);
            return;
        }
        if (password.equals("")){
            txtError.setText("Password harus diisi");
            txtError.setVisibility(View.VISIBLE);
            return;

        }
        try {
            if((session.getUserdata().has("userID"))){
                if(username.equals(session.getUserdata().has("userID"))
                        &&
                        password.equals(session.getLastPassword())){
                    checkVersion();
                }else{
                    session.setUserData(new JSONObject());
                    login();
                }
            }else {
                        /*
                        encrypt password
                         */
                StringBuffer sb = new StringBuffer();
                        /*
                        set loading
                         */
                loading.setMessage("Mengirim Informasi Login ...");
                loading.show();
                        /*
                        store paramerters
                         */
                RequestParams params = new RequestParams();
                JSONObject data = new JSONObject();
                try {
                    data.put("device_id", session.getDevice());
                    data.put("password", password);
                    data.put("username", username);
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                String textToSend = AES.generateText(data.toString(), 0);
                params.put("data", textToSend);
                httpClient.addHeader("API-KEY", config.APP_ID);
                httpClient.post(session.getUrl() + config.LOGIN_ENDPOINT, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try {
                            String data = new String(responseBody, StandardCharsets.UTF_8);
                            JSONObject response = new JSONObject(AES.realText(data));
                                /*
                                if login succes and valid login
                                 */
                            if (response.getBoolean("status")) {
                                    /*
                                    store token and userdata
                                     */
                                session.setToken(response.getString("token"));
                                Log.i("TOKEN", response.getString("token"));
                                session.setUserData(response.getJSONArray("data").getJSONObject(0));
                                checkVersion();
                            } else {
                                loading.hide();
                                txtError.setText(response.getString("message"));
                                txtError.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            loading.hide();
                            Log.e(getClass().getSimpleName(), e.getMessage());
                            txtError.setText("Client Error");
                            txtError.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        loading.hide();
                        txtError.setText("Server Error");
                        txtError.setVisibility(View.VISIBLE);
                    }
                });
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void checkVersion(){
        loading.setMessage("Checking App Version ...");
        httpClient.addHeader("API-KEY",config.APP_ID);
        RequestParams params=new RequestParams();
        params.put("version",session.getVersion());
        httpClient.get(session.getUrl()+new Global().SYNC_ENDPOINT, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                loading.dismiss();
                String data=new String(responseBody, StandardCharsets.UTF_8);
                try {
                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    JSONObject response=new JSONObject(AES.realText(data));
                    String logicVersion=response.getString("version");
                    final String packVersion=response.getString("pack_version");
                    String currentVersion="V."+pInfo.versionName;
                    if(!session.getVersion().equals(logicVersion) && currentVersion.equals(packVersion)){
                        Intent configuration=new Intent(Login.this,Configuration.class);
                        startActivity(configuration);
                    }else if(session.getVersion().equals(logicVersion) && !currentVersion.equals(packVersion)){
                        AlertDialog.Builder builder=new AlertDialog.Builder(Login.this);
                        builder.setMessage("App terbaru tersedia ("+packVersion+").");
                        builder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                downloadLatestApp(packVersion);
                            }
                        });
                        builder.setNegativeButton("Ingatkan Nanti", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                setLoginData();
                                Intent intent=new Intent(Login.this,Main.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                        AlertDialog dialog=builder.create();
                        dialog.show();
                    }else{
                        Log.i("LOG","GO TO LOGIN");
                        setLoginData();
                        Intent intent=new Intent(Login.this,Main.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException | PackageManager.NameNotFoundException e) {
                    FirebaseCrash.log(e.getMessage());
                    setLoginData();
                    Intent intent=new Intent(Login.this,Main.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                loading.dismiss();
            }
        });
    }
    public void setLoginData(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        session.setLastLogin(dateFormat.format(Calendar.getInstance().getTime()));
        session.setLogin(true);
        session.setSynced(true);
        DaoSession daoSession=((App)getApplication()).getDbSession();
        login_logsDao login_logsDao=daoSession.getLogin_logsDao();
        login_logs newLogs=new login_logs();
        newLogs.setLog_id(Calendar.getInstance().getTimeInMillis());
        SimpleDateFormat dateFormatA=new SimpleDateFormat("yyyy-MM-dd");
        newLogs.setLogin_date(dateFormatA.format(Calendar.getInstance().getTime()));
        SimpleDateFormat dateFormatB=new SimpleDateFormat("HH:mm:ss");
        newLogs.setLogin(dateFormatB.format(Calendar.getInstance().getTime()));
        try {
            newLogs.setUser_id(session.getUserdata().getString("userID"));
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        login_logsDao.insert(newLogs);
        if(!txtPassword.getText().toString().equals("") && txtPassword.getText()==null) {
            session.setLastPassword(txtPassword.getText().toString());
        }
    }
    public void downloadLatestApp(final String version){
        loading.setMessage("Download app terbaru ...");
        loading.show();
        httpClient.addHeader("API-KEY",config.APP_ID);
        RequestParams params=new RequestParams();
        params.put("version",version);
        httpClient.get(session.getUrl()+new Global().APK_ENDPOINT, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                loading.dismiss();
                File apk=new File(Environment.getExternalStorageDirectory()+"/MSS.apk");
                try {
                    if(!apk.exists()){
                        apk.createNewFile();
                    }
                    FileOutputStream fos=new FileOutputStream(apk);
                    fos.write(responseBody);
                    fos.close();
                    Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                            .setDataAndType(Uri.parse("file://"+apk.getAbsolutePath()),
                                    "application/vnd.android.package-archive");
                    startActivity(promptInstall);
                    finish();
                } catch (IOException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                loading.dismiss();
            }
        });
    }
}
