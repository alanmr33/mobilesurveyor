package com.indocyber.dipostarsurvey.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.dipostarsurvey.R;

/**
 * Created by Indocyber on 24/07/2017.
 */

public class OrderEntryNewHolder extends RecyclerView.ViewHolder {
    public TextView txtOrderID, txtfppNo ,txtNama,txtAlamat,txtNoHP,txtPlanDate;
    public LinearLayout mainLayout;
    public OrderEntryNewHolder(View itemView) {
        super(itemView);
        txtPlanDate= (TextView) itemView.findViewById(R.id.txtPlanDate);
        txtOrderID= (TextView) itemView.findViewById(R.id.txtNewOrderID);
        txtfppNo = (TextView) itemView.findViewById(R.id.txtFppNo);
        txtNama= (TextView) itemView.findViewById(R.id.txtNewOrderNama);
        txtAlamat= (TextView) itemView.findViewById(R.id.txtNewOrderAlamat);
        txtNoHP= (TextView) itemView.findViewById(R.id.txtNewOrderNoHP);
        mainLayout= (LinearLayout) itemView.findViewById(R.id.itemNewOrder);
    }
}
