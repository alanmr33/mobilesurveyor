package com.indocyber.dipostarsurvey.Config;

/**
 * Created by Indocyber on 14/07/2017.
 */

public class Global {
    public final String PREF_NAME="columbia-app";
    public final String APP_ID="wiliam-alan";
    public final String API="https://mss.dipostar.com/APIV10";
    public final int SPLASH=500;
    public final int CODE_PERMISSION=1000;
    public final String LOGIN_ENDPOINT="/login";
    public final String CPASSWORD_ENDPOINT="/user/settings";
    public final String TASK_ENDPOINT="/user/task";
    public final String TASK_UPLOAD_ENDPOINT="/user/task/upload";
    public final String LOG_ENDPOINT="/user/tracking";
    public final String LOGIN_LOG_ENDPOINT="/user/log";
    public final String DOWNLOAD_ENDPOINT="/user/downloadSPK";
    public final String DOWNLOAD_LD_ENDPOINT="/user/downloadLastPhoto";
    public final String SYNC_ENDPOINT="/sync";
    public final String APK_ENDPOINT="/downloadAPK";
    public final String SYNC_OBJ_ENDPOINT="/sync/get_object";
    public final int TRACKING_TIME=5*60*1000;
    public final int SYNC_TIME=60*1000;
    public final int GETTASK_TIME=3*60*1000;
    public final int LOGS_TIME=5*60*1000;
}
