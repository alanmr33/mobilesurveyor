package com.indocyber.dipostarsurvey.Service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Library.Resizer;
import com.indocyber.dipostarsurvey.Library.Utils;
import com.indocyber.dipostarsurvey.db.activitiesDao;
import com.indocyber.dipostarsurvey.db.login_logs;
import com.indocyber.dipostarsurvey.db.login_logsDao;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Config.Global;
import com.indocyber.dipostarsurvey.Library.AES;
import com.indocyber.dipostarsurvey.Library.FileCreator;
import com.indocyber.dipostarsurvey.Library.Session;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.Tracking;
import com.indocyber.dipostarsurvey.db.TrackingDao;
import com.indocyber.dipostarsurvey.db.activities;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.greenrobot.greendao.query.Query;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.spec.ECField;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Indocyber on 15/12/2017.
 */
/*
TODO : UPLOAD NEED TO BE IMPROVE
 */
public class AppService extends Service {
    private boolean initTask=true;
    private AppServiceBinder mBinder=new AppServiceBinder();
    private DaoSession daoSession;
    private Session session;
    private AsyncHttpClient httpClient=new AsyncHttpClient();
    public Global config=new Global();
    private TrackingDao trackingDao;
    private login_logsDao login_logsDao;
    private com.indocyber.dipostarsurvey.db.activitiesDao activitiesDao;
    private com.indocyber.dipostarsurvey.db.order_entriesDao order_entriesDao;
    private JSONArray uploadJobs=new JSONArray();
    private JSONArray ldJobs=new JSONArray();
    private boolean initLogs=true;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        try {
            daoSession= ((App)getApplication()).getDbSession();
            trackingDao=daoSession.getTrackingDao();
            activitiesDao=daoSession.getActivitiesDao();
            order_entriesDao=daoSession.getOrder_entriesDao();
            login_logsDao=daoSession.getLogin_logsDao();
            session=new Session(getApplicationContext());
            tracking();
            uploadResult(true);
            if(session.getJobs().has(session.getUserdata().getString("userID"))){
                uploadJobs=session.getJobs().getJSONArray(session.getUserdata().getString("userID"));
                uploadResultPhoto();
            }
            if(session.getJobs().has(session.getUserdata().getString("userID")+"-LD")){
                uploadJobs=session.getJobs().getJSONArray(session.getUserdata().getString("userID")+"-LD");
                try {
                    downloadLastDokumen();
                } catch (InterruptedException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
            Log.i("UPLOAD Q",uploadJobs.toString());
            Log.i("LD Q",ldJobs.toString());
            getTask();
            clean();
            uploadLogs();
            downloadFile();
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
    public class AppServiceBinder extends Binder{
        public AppService getService(){
            return AppService.this;
        }
    }
    /*
    Clean Last Order
     */
    public void clean(){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, -30);
            com.indocyber.dipostarsurvey.db.order_entriesDao entriesDao = daoSession.getOrder_entriesDao();
            com.indocyber.dipostarsurvey.db.activitiesDao activitiesDao = daoSession.getActivitiesDao();
            Query<activities> activitiesQuery = null;
            activitiesQuery = activitiesDao.queryBuilder()
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("SENT"))
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Updated_date.le(calendar.getTime()))
                    .build();
            List<activities> activitiesList = activitiesQuery.list();
            for (int i = 0; i < activitiesList.size(); i++) {
                Query<order_entries> query = null;
                query = entriesDao.queryBuilder()
                        .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.Order_id
                                .eq(activitiesList.get(i).getOrder_id()))
                        .build();
                List<order_entries> result = query.list();
                if(result.size() > 0) {
                    Query<activities> activitiesQueryO = null;
                    activitiesQueryO = activitiesDao.queryBuilder()
                            .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.notEq("SENT"))
                            .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Order_id.eq(result.get(0).getOrder_id()))
                            .build();
                    List<activities> activitiesListO = activitiesQueryO.list();
                    if (activitiesListO.size() == 0) {
                        if (result.get(0).getAttachment() != null && result.get(0).getAttachment().equals("-")) {
                            File spkF = getExternalFilesDir("spk-files");
                            File att = new File(spkF.getAbsolutePath() + "/" + result.get(0).getAttachment());
                            if (att.exists()) {
                                att.delete();
                            }
                        }
                        File f = getExternalFilesDir("images/" + activitiesList.get(i).getOrder_id()
                                .replace("/", "_"));
                        if (f.exists()) {
                            f.delete();
                        }
                        activitiesDao.delete(activitiesList.get(i));
                        order_entriesDao.delete(result.get(0));
                    }
                }
            }
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    /*
    Get Order Survey
     */
    public void getTask(){
        try{
            int time;
            if (initTask) {
                time = 1000;
                initTask = false;
            } else {
                time = config.GETTASK_TIME;
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(session.isLogin()) {
                        RequestParams params = new RequestParams();
                        params.put("token", session.getToken());
                        httpClient.addHeader("API-KEY", config.APP_ID);
                        httpClient.get(session.getUrl() + config.TASK_ENDPOINT, params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                try {
                                    String data = new String(responseBody, StandardCharsets.UTF_8);
                                    JSONArray response = new JSONArray(AES.realText(data));

                                    if (response != null) {
                                        Log.i("GET TASK", "Success " + response.length());
                                        for (int i = 0; i < response.length(); i++) {
                                            final JSONObject respon1 = response.getJSONObject(i);
                                            final int finalI = i;
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        if (!respon1.getString("status").equals("94")
                                                                && !respon1.getString("status").equals("96")) {
                                                            if (respon1.getString("userID")
                                                                    .equals(session.getUserdata().getString("userID"))) {
                                                                com.indocyber.dipostarsurvey.db.order_entriesDao entriesDao = daoSession.getOrder_entriesDao();
                                                                Query<order_entries> query = entriesDao.queryBuilder()
                                                                        .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.Order_id.eq(respon1.getString("orderID")))
                                                                        .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                                                        .build();
                                                                List<order_entries> order_entries = query.list();
                                                                if (order_entries.size() == 0) {
                                                                    order_entries order = new order_entries();
                                                                    order.setOrder_id(respon1.getString("orderID"));
                                                                    order.setFpp_no(respon1.getString("fppNo"));
                                                                    order.setNama_pemesan(respon1.getString("namaPemesan"));
                                                                    order.setNohp(respon1.getString("noHP"));
                                                                    order.setCreated_date(respon1.getString("createdDate"));
                                                                    if (respon1.has("planDate")) {
                                                                        order.setPlan_date(respon1.getString("planDate"));
                                                                    }
                                                                    order.setAlamat(respon1.getString("alamatPemesan"));
                                                                    if (respon1.has("spkFilename")) {
                                                                        order.setSpk_filename(respon1.getString("spkFilename"));
                                                                    }
                                                                    order.setInserted_date(Calendar.getInstance().getTime());
                                                                    order.setEnabled(true);
                                                                    JSONObject last = new JSONObject();
                                                                    if (respon1.has("lastData")) {
                                                                        order.setLast_data(respon1.getString("lastData"));
                                                                        last = new JSONObject(respon1.getString("lastData"));
                                                                    }
                                                                    order.setUser_id(session.getUserdata().getString("userID"));
                                                                    if (respon1.has("notes")) {
                                                                        order.setNotes(respon1.getString("notes"));
                                                                    }
                                                                    entriesDao.insertOrReplace(order);
                                                                    if (last.has("survey-orderId")) {
                                                                        String Photo1 = (last.has("Photo1-file")) ? last.getString("Photo1-file") : "";
                                                                        String Photo2 = (last.has("Photo2-file")) ? last.getString("Photo2-file") : "";
                                                                        String Photo3 = (last.has("Photo3-file")) ? last.getString("Photo3-file") : "";
                                                                        String Photo4 = (last.has("Photo4-file")) ? last.getString("Photo4-file") : "";
                                                                        String Photo5 = (last.has("Photo5-file")) ? last.getString("Photo5-file") : "";
                                                                        String Photo6 = (last.has("Photo6-file")) ? last.getString("Photo6-file") : "";
                                                                        String Photo7 = (last.has("Photo7-file")) ? last.getString("Photo7-file") : "";
                                                                        String Photo8 = (last.has("Photo8-file")) ? last.getString("Photo8-file") : "";
                                                                        String Photo9 = (last.has("Photo9-file")) ? last.getString("Photo9-file") : "";
                                                                        String Photo10 = (last.has("Photo10-file")) ? last.getString("Photo10-file") : "";
                                                                        if (!Photo1.equals("")) {
                                                                            JSONObject photo1 = new JSONObject();
                                                                            photo1.put("orderID", last.getString("survey-orderId"));
                                                                            photo1.put("type", "Photo1");
                                                                            photo1.put("filename", Photo1);
                                                                            addLDJob(photo1);
                                                                        }
                                                                        if (!Photo2.equals("")) {
                                                                            JSONObject photo2 = new JSONObject();
                                                                            photo2.put("orderID", last.getString("survey-orderId"));
                                                                            photo2.put("type", "Photo2");
                                                                            photo2.put("filename", Photo2);
                                                                            addLDJob(photo2);
                                                                        }
                                                                        if (!Photo3.equals("")) {
                                                                            JSONObject photo3 = new JSONObject();
                                                                            photo3.put("orderID", last.getString("survey-orderId"));
                                                                            photo3.put("type", "Photo3");
                                                                            photo3.put("filename", Photo3);
                                                                            addLDJob(photo3);
                                                                        }
                                                                        if (!Photo4.equals("")) {
                                                                            JSONObject photo4 = new JSONObject();
                                                                            photo4.put("orderID", last.getString("survey-orderId"));
                                                                            photo4.put("type", "Photo4");
                                                                            photo4.put("filename", Photo4);
                                                                            addLDJob(photo4);
                                                                        }
                                                                        if (!Photo5.equals("")) {
                                                                            JSONObject photo5 = new JSONObject();
                                                                            photo5.put("orderID", last.getString("survey-orderId"));
                                                                            photo5.put("type", "Photo5");
                                                                            photo5.put("filename", Photo5);
                                                                            addLDJob(photo5);
                                                                        }
                                                                        if (!Photo6.equals("")) {
                                                                            JSONObject photo6 = new JSONObject();
                                                                            photo6.put("orderID", last.getString("survey-orderId"));
                                                                            photo6.put("type", "Photo6");
                                                                            photo6.put("filename", Photo6);
                                                                            addLDJob(photo6);
                                                                        }
                                                                        if (!Photo7.equals("")) {
                                                                            JSONObject photo7 = new JSONObject();
                                                                            photo7.put("orderID", last.getString("survey-orderId"));
                                                                            photo7.put("type", "Photo7");
                                                                            photo7.put("filename", Photo7);
                                                                            addLDJob(photo7);
                                                                        }
                                                                        if (!Photo8.equals("")) {
                                                                            JSONObject photo8 = new JSONObject();
                                                                            photo8.put("orderID", last.getString("survey-orderId"));
                                                                            photo8.put("type", "Photo8");
                                                                            photo8.put("filename", Photo8);
                                                                            addLDJob(photo8);
                                                                        }
                                                                        if (!Photo9.equals("")) {
                                                                            JSONObject photo9 = new JSONObject();
                                                                            photo9.put("orderID", last.getString("survey-orderId"));
                                                                            photo9.put("type", "Photo9");
                                                                            photo9.put("filename", Photo9);
                                                                            addLDJob(photo9);
                                                                        }
                                                                        if (!Photo10.equals("")) {
                                                                            JSONObject photo10 = new JSONObject();
                                                                            photo10.put("orderID", last.getString("survey-orderId"));
                                                                            photo10.put("type", "Photo10");
                                                                            photo10.put("filename", Photo10);
                                                                            addLDJob(photo10);
                                                                        }
                                                                        try {
                                                                            downloadLastDokumen();
                                                                        } catch (InterruptedException e) {
                                                                            FirebaseCrash.log(e.getMessage());
                                                                        }
                                                                    }
                                                                } else {
                                                                    if ((respon1.getString("status").equals("91") ||
                                                                            respon1.getString("status").equals("90"))) {
                                                                        activitiesDao activitiesDao = daoSession.getActivitiesDao();
                                                                        Query<activities> queryact = activitiesDao.queryBuilder()
                                                                                .whereOr(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("DRAFT"),
                                                                                        com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("PENDING"))
                                                                                .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                                                                .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Order_id.eq(respon1.getString("orderID")))
                                                                                .build();
                                                                        List<activities> activitiesList = queryact.list();
                                                                        if (activitiesList.size() == 0) {
                                                                            order_entries order = order_entries.get(0);
                                                                            order.setOrder_id(respon1.getString("orderID"));
                                                                            order.setFpp_no(respon1.getString("fppNo"));
                                                                            order.setNama_pemesan(respon1.getString("namaPemesan"));
                                                                            order.setNohp(respon1.getString("noHP"));
                                                                            order.setCreated_date(respon1.getString("createdDate"));
                                                                            order.setPlan_date(respon1.getString("planDate"));
                                                                            order.setAlamat(respon1.getString("alamatPemesan"));
                                                                            order.setInserted_date(Calendar.getInstance().getTime());
                                                                            order.setEnabled(true);
                                                                            order_entriesDao.update(order);
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                boolean deleted = false;
                                                                com.indocyber.dipostarsurvey.db.order_entriesDao entriesDao = daoSession.getOrder_entriesDao();
                                                                Query<order_entries> query = entriesDao.queryBuilder()
                                                                        .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.Order_id.eq(respon1.getString("orderID")))
                                                                        .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.User_id.eq(respon1.getString("oldSurveyorID")))
                                                                        .build();
                                                                List<order_entries> order_entries = query.list();
                                                                if (order_entries.size() > 0) {
                                                                    File spkF = getExternalFilesDir("spk-files");
                                                                    File att = new File(spkF.getAbsolutePath() + "/" + order_entries.get(0).getAttachment());
                                                                    if (att.exists()) {
                                                                        att.delete();
                                                                    }
                                                                    entriesDao.delete(order_entries.get(0));
                                                                    deleted = true;
                                                                }

                                                                activitiesDao activitiesDao = daoSession.getActivitiesDao();
                                                                Query<activities> queryact = activitiesDao.queryBuilder()
                                                                        .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(respon1.getString("oldSurveyorID")))
                                                                        .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Order_id.eq(respon1.getString("orderID")))
                                                                        .build();
                                                                List<activities> activitiesList = queryact.list();
                                                                for (int a = 0; a < activitiesList.size(); a++) {
                                                                    File f = getExternalFilesDir("images/" + activitiesList.get(finalI).getOrder_id()
                                                                            .replace("/", "_"));
                                                                    if (f.exists()) {
                                                                        f.delete();
                                                                    }
                                                                    activitiesDao.delete(activitiesList.get(a));
                                                                    deleted = true;
                                                                }
                                                                if (deleted) {
                                                                    final NotificationManager mNotifyManager =
                                                                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                                                    final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AppService.this);
                                                                    mBuilder.setContentTitle("Order " + respon1.getString("orderID"))
                                                                            .setContentText("Telah dipindahkan ke user lain")
                                                                            .setGroupSummary(true)
                                                                            .setSmallIcon(R.drawable.ic_download);
                                                                    mNotifyManager.notify(300 + finalI, mBuilder.build());
                                                                }
                                                            }
                                                        } else {
                                                            boolean deleted = false;

                                                            activitiesDao activitiesDao = daoSession.getActivitiesDao();
                                                            Query<activities> queryact = activitiesDao.queryBuilder()
                                                                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                                                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Order_id.eq(respon1.getString("orderID")))
                                                                    .build();
                                                            List<activities> activitiesList = queryact.list();
                                                            for (int a = 0; a < activitiesList.size(); a++) {
                                                                File f = getExternalFilesDir("images/" + activitiesList.get(finalI).getOrder_id()
                                                                        .replace("/", "_"));
                                                                if (f.exists()) {
                                                                    f.delete();
                                                                }
                                                                activitiesDao.delete(activitiesList.get(a));
                                                                deleted = true;
                                                            }

                                                            com.indocyber.dipostarsurvey.db.order_entriesDao entriesDao = daoSession.getOrder_entriesDao();
                                                            Query<order_entries> query = entriesDao.queryBuilder()
                                                                    .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.Order_id.eq(respon1.getString("orderID")))
                                                                    .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                                                    .build();
                                                            List<order_entries> order_entries = query.list();
                                                            if (order_entries.size() > 0) {
                                                                entriesDao.delete(order_entries.get(0));
                                                                deleted = true;
                                                            }

                                                            if (deleted) {
                                                                final NotificationManager mNotifyManager =
                                                                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                                                final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AppService.this);
                                                                try {
                                                                    mBuilder.setContentTitle("Order " + respon1.getString("orderID"))
                                                                            .setContentText("Telah ditutup oleh system ")
                                                                            .setGroupSummary(true)
                                                                            .setSmallIcon(R.drawable.ic_download);
                                                                } catch (JSONException e) {
                                                                    FirebaseCrash.log(e.getMessage());
                                                                }
                                                                mNotifyManager.notify(300 + finalI, mBuilder.build());
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        FirebaseCrash.log(e.getMessage());
                                                    }
                                                }
                                            }, finalI * 10);
                                        }
                                    }

                                } catch (Exception e) {
                                    Log.e(getClass().getSimpleName(), e.getMessage());
                                }
                                downloadFile();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                try {
                                    FirebaseCrash.log(statusCode + " FAILED DOWNLOAD TASK FOR USER " + session.getUserdata().getString("userID"));
                                } catch (Exception e) {
                                    FirebaseCrash.log(e.getMessage());
                                }
                            }
                        });
                    }
                    getTask();
                }
            }, time);
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    /*
    Download File Lampiran
     */
    public void downloadFile(){
        try{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(session.isLogin()) {

                        final RequestParams params = new RequestParams();
                        order_entriesDao=daoSession.getOrder_entriesDao();
                        Query<order_entries> order_entriesQuery= null;
                        try {
                            order_entriesQuery = order_entriesDao.queryBuilder()
                                    .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.Attachment.isNull())
                                    .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                    .build();
                        } catch (Exception e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                        final List<order_entries> entriesList=order_entriesQuery.list();
                        params.put("token", session.getToken());
                        httpClient.addHeader("API-KEY", config.APP_ID);
                        for(int a=0;a<entriesList.size();a++){
                            final order_entries order=entriesList.get(a);
                            final Date dateStart=Calendar.getInstance().getTime();
                            final int ax=a;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        JSONObject data=new JSONObject();
                                        data.put("orderID",order.getOrder_id());
                                        params.put("data",AES.generateText(data.toString(),0));
                                        httpClient.post(session.getUrl() + config.DOWNLOAD_ENDPOINT, params, new AsyncHttpResponseHandler() {
                                            @Override
                                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                if(responseBody.length==0){
                                                    order.setAttachment("-");
                                                    order_entriesDao.update(order);
                                                }else{
                                                    final NotificationManager mNotifyManager =
                                                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                                    Date dateEnd=Calendar.getInstance().getTime();
                                                    long diff=((dateEnd.getTime()-dateStart.getTime())/(1000));
                                                    long minute=diff/60;
                                                    long second=diff%60;
                                                    final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AppService.this);
                                                    mBuilder.setContentTitle("Survey Order "+order.getOrder_id())
                                                            .setContentText("Download Lampiran  Selesai ("+minute+"m "+second+"s) ")
                                                            .setGroupSummary(true)
                                                            .setSmallIcon(R.drawable.ic_download);
                                                    if(order.getSpk_filename().contains(".pdf")){
                                                        order.setAttachment(order.getOrder_id().replace("/","_")+".pdf");
                                                        File f= FileCreator.createSPK(AppService.this,order.getOrder_id().replace("/","_"),true);
                                                        try {
                                                            FileOutputStream outputStream=new FileOutputStream(f);
                                                            outputStream.write(responseBody);
                                                            outputStream.close();
                                                        } catch (IOException e) {
                                                            FirebaseCrash.log(e.getMessage());
                                                        }
                                                    }else{
                                                        order.setAttachment(order.getOrder_id().replace("/","_")+".jpg");
                                                        File f= FileCreator.createSPK(AppService.this,order.getOrder_id().replace("/","_"),false);
                                                        try {
                                                            FileOutputStream outputStream=new FileOutputStream(f);
                                                            outputStream.write(responseBody);
                                                            outputStream.close();
                                                        } catch (Exception e) {
                                                            FirebaseCrash.log(e.getMessage());
                                                        }
                                                    }
                                                    order_entriesDao.update(order);
                                                    mNotifyManager.notify(101+ax, mBuilder.build());
                                                }
                                            }

                                            @Override
                                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                order.setAttachment("-");
                                                order_entriesDao.update(order);
                                                Log.e(String.valueOf(statusCode),session.getUrl());
                                            }
                                            @Override
                                            public void onProgress(long bytesWritten, long totalSize) {

                                            }
                                        });
                                    } catch (Exception e) {
                                        FirebaseCrash.log(e.getMessage());
                                    }
                                }
                            },a*3*1000);
                        }
                    }
                }
            },3000);
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    /*
    Tracking
     */
    public void tracking(){
        try{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (session.isLogin()) {
                        RequestParams params = new RequestParams();

                        trackingDao = daoSession.getTrackingDao();
                        final Query<Tracking> query;
                        try {
                            query = trackingDao.queryBuilder()
                                    .where(TrackingDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                    .build();
                            final List<Tracking> locsList = query.list();
                            JSONArray locsArray = new JSONArray();

                            if (locsList != null) {
                                for (int i = 0; i < locsList.size(); i++) {
                                    JSONObject locsObject = new JSONObject();
                                    try {
                                        locsObject.put("date", locsList.get(i).getTracking_date());
                                        locsObject.put("location", locsList.get(i).getPosition());
                                        locsArray.put(i, locsObject);
                                    } catch (JSONException e) {
                                        FirebaseCrash.log(e.getMessage());
                                    }
                                }

                                String textToSend = AES.generateText(locsArray.toString(),0);
                                params.put("locs", textToSend);
                                params.put("token", session.getToken());

                                httpClient.addHeader("API-KEY", config.APP_ID);
                                httpClient.post(session.getUrl() + config.LOG_ENDPOINT, params, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                        Log.i("TRACK", "SUCCESS LOCATION TRACK");
                                        for (int i = 0; i < locsList.size(); i++) {
                                            trackingDao.delete(locsList.get(i));
                                        }
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                        try {
                                            FirebaseCrash.log(statusCode+" FAILED UPLOAD TRACKING FOR USER "+session.getUserdata().getString("userID"));
                                        } catch (Exception e) {
                                            FirebaseCrash.log(e.getMessage());
                                        }
                                    }
                                });
                            }
                        } catch (Exception e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                    }
                    tracking();

                }
            }, new Global().TRACKING_TIME);
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    /*
    Upload Hasil Survey
     */
    public void uploadResult(boolean delayed){
        if(delayed){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        uploadResultPhoto();
                        doUpload();
                    } catch (Exception e) {
                        FirebaseCrash.log(e.getMessage());
                    }
                }
            },new Global().SYNC_TIME);
        }else{
            doUpload();
        }
    }
    public void doUpload(){
        if(session.isLogin()) {
            RequestParams params = new RequestParams();
            activitiesDao=daoSession.getActivitiesDao();
            Query<activities> query = null;
            try {
                query = activitiesDao.queryBuilder()
                        .whereOr(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Send_status.eq("ANTRIAN"),
                                com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Send_status.eq("ANTRIAN ULANG"))
                        .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                        .build();
                List<activities> result = query.list();
                int a=0;
                while (a<result.size()){
                    final activities act=result.get(a);
                    act.setSend_status("MENGIRIM");
                    activitiesDao.update(act);
                    JSONObject data=new JSONObject();
                    params.put("token", session.getToken());
                    try {
                        data.put("activity_id",act.getActivity_id());
                        data.put("body",act.getBody());
                        data.put("order_id",act.getOrder_id());
                        SimpleDateFormat formated=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        data.put("created_date",formated.format(act.getCreated_date()));
                    } catch (JSONException e) {
                        FirebaseCrash.log(e.getMessage());
                    }
                    params.put("data",AES.generateText(data.toString(),0));
                    httpClient.addHeader("API-KEY", config.APP_ID);
                    httpClient.post(session.getUrl() + config.TASK_ENDPOINT, params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String data=new String(responseBody, StandardCharsets.UTF_8);
                            try {
                                JSONObject response=new JSONObject(AES.realText(data));
                                if(response.getBoolean("status")){
                                    JSONObject body=new JSONObject(act.getBody());
                                    String surveyStatus=(body.has("SurveyStatus"))? body.getString("SurveyStatus") : "";
                                    Log.i("TASK RESULT",surveyStatus+" ORDER ID "+act.getOrder_id()+" SENT");
                                    if(surveyStatus.equals("65")) {
                                        act.setIs_upload(0);
                                    }else{
                                        act.setIs_upload(1);
                                    }
                                    if(surveyStatus.equals("65")){
                                        act.setIs_upload(1);
                                        activitiesDao.update(act);
                                        String Photo1=(body.has("Photo1-file"))? body.getString("Photo1-file") : "";
                                        String Photo2=(body.has("Photo2-file"))? body.getString("Photo2-file") : "";
                                        String Photo3=(body.has("Photo3-file"))? body.getString("Photo3-file") : "";
                                        String Photo4=(body.has("Photo4-file"))? body.getString("Photo4-file") : "";
                                        String Photo5=(body.has("Photo5-file"))? body.getString("Photo5-file") : "";
                                        String Photo6=(body.has("Photo6-file"))? body.getString("Photo6-file") : "";
                                        String Photo7=(body.has("Photo7-file"))? body.getString("Photo7-file") : "";
                                        String Photo8=(body.has("Photo8-file"))? body.getString("Photo8-file") : "";
                                        String Photo9=(body.has("Photo9-file"))? body.getString("Photo9-file") : "";
                                        String Photo10=(body.has("Photo10-file"))? body.getString("Photo10-file") : "";
                                        if(!Photo1.equals("")) {
                                            JSONObject photo1=new JSONObject();
                                            photo1.put("orderID",act.getOrder_id());
                                            photo1.put("type","Photo1");
                                            photo1.put("filename",Photo1);
                                            addUploadJob(photo1);
                                        }
                                        if(!Photo2.equals("")) {
                                            JSONObject photo2=new JSONObject();
                                            photo2.put("orderID",act.getOrder_id());
                                            photo2.put("type","Photo2");
                                            photo2.put("filename",Photo2);
                                            addUploadJob(photo2);
                                        }
                                        if(!Photo3.equals("")) {
                                            JSONObject photo3=new JSONObject();
                                            photo3.put("orderID",act.getOrder_id());
                                            photo3.put("type","Photo3");
                                            photo3.put("filename",Photo3);
                                            addUploadJob(photo3);
                                        }
                                        if(!Photo4.equals("")) {
                                            JSONObject photo4=new JSONObject();
                                            photo4.put("orderID",act.getOrder_id());
                                            photo4.put("type","Photo4");
                                            photo4.put("filename",Photo4);
                                            addUploadJob(photo4);
                                        }
                                        if(!Photo5.equals("")) {
                                            JSONObject photo5=new JSONObject();
                                            photo5.put("orderID",act.getOrder_id());
                                            photo5.put("type","Photo5");
                                            photo5.put("filename",Photo5);
                                            addUploadJob(photo5);
                                        }
                                        if(!Photo6.equals("")) {
                                            JSONObject photo6=new JSONObject();
                                            photo6.put("orderID",act.getOrder_id());
                                            photo6.put("type","Photo6");
                                            photo6.put("filename",Photo6);
                                            addUploadJob(photo6);
                                        }
                                        if(!Photo7.equals("")) {
                                            JSONObject photo7=new JSONObject();
                                            photo7.put("orderID",act.getOrder_id());
                                            photo7.put("type","Photo7");
                                            photo7.put("filename",Photo7);
                                            addUploadJob(photo7);
                                        }
                                        if(!Photo8.equals("")) {
                                            JSONObject photo8=new JSONObject();
                                            photo8.put("orderID",act.getOrder_id());
                                            photo8.put("type","Photo8");
                                            photo8.put("filename",Photo8);
                                            addUploadJob(photo8);
                                        }
                                        if(!Photo9.equals("")) {
                                            JSONObject photo9=new JSONObject();
                                            photo9.put("orderID",act.getOrder_id());
                                            photo9.put("type","Photo9");
                                            photo9.put("filename",Photo9);
                                            addUploadJob(photo9);
                                        }
                                        if(!Photo10.equals("")) {
                                            JSONObject photo10=new JSONObject();
                                            photo10.put("orderID",act.getOrder_id());
                                            photo10.put("type","Photo10");
                                            photo10.put("filename",Photo10);
                                            addUploadJob(photo10);
                                        }
                                        if(Photo1.equals("") && Photo2.equals("")
                                                && Photo3.equals("") && Photo4.equals("")
                                                && Photo5.equals("") && Photo6.equals("")
                                                && Photo7.equals("") && Photo8.equals("")
                                                && Photo9.equals("") && Photo10.equals("")){
                                            act.setSend_status("SUKSES");
                                            act.setMobile_status("SENT");
                                        }else{
                                            act.setSend_status("MENUNGGU UPLOAD PHOTO");
                                        }
                                    }else{
                                        act.setSend_status("SUKSES");
                                        act.setMobile_status("SENT");
                                    }
                                    activitiesDao.update(act);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                uploadResultPhoto();
                                            } catch (JSONException e) {
                                               FirebaseCrash.log(e.getMessage());
                                            }
                                        }
                                    },1000);
                                }
                            } catch (JSONException e) {
                                FirebaseCrash.log(e.getMessage());
                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                FirebaseCrash.log(statusCode+" UPLOAD RESULT FOR USER "+session.getUserdata().getString("userID"));
                            } catch (JSONException e) {
                                FirebaseCrash.log(e.getMessage());
                            }
                            act.setSend_status("ANTRIAN ULANG");
                            activitiesDao.update(act);
                        }
                    });
                    a++;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        uploadResult(true);
                    }
                },2000);
            } catch (JSONException e) {
                FirebaseCrash.log(e.getMessage());
            }
        }
    }
    /*
    Upload Result Photo
     */
    public void uploadResultPhoto() throws JSONException {
        try{
        if(session.isLogin()) {
            if (uploadJobs.length() > 0) {
                RequestParams params = new RequestParams();
                File f = new File(uploadJobs.getJSONObject(0).getString("filename"));
                if ((f.length() / 1024) > 45) {
                    Resizer.resizeBitmapFile(uploadJobs.getJSONObject(0).getString("filename"), 300);
                    f = new File(uploadJobs.getJSONObject(0).getString("filename"));
                }
                byte imageData[] = new byte[(int) f.length()];
                try {
                    FileInputStream fis = new FileInputStream(f);
                    fis.read(imageData);
                } catch (IOException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                String base64 = com.indocyber.dipostarsurvey.Library.Base64.encode(imageData);
                params.put("token", session.getToken());
                params.put("photo_name", uploadJobs.getJSONObject(0).getString("type"));
                params.put("order_id", uploadJobs.getJSONObject(0).getString("orderID"));
                try {
                    params.put("image", new String(base64.getBytes(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                httpClient.addHeader("API-KEY", config.APP_ID);
                Log.i("UPLOAD", uploadJobs.getJSONObject(0).toString());
                httpClient.post(session.getUrl() + config.TASK_UPLOAD_ENDPOINT, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        String data = new String(responseBody, StandardCharsets.UTF_8);
                        try {
                            JSONObject response = new JSONObject(AES.realText(data));
                            if (response.getBoolean("status")) {
                                Log.i("UPLOAD SUCCESS", uploadJobs.get(0).toString() + " Success");
                                deleteUploadJob();
                            }
                            Log.i("UPLOAD RESULT", response.toString());
                            uploadResultPhoto();
                        } catch (JSONException e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        try {
                            FirebaseCrash.log(statusCode + " FAILED UPLOAD PHOTO FOR USER " + session.getUserdata().getString("userID"));
                        } catch (JSONException e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                    }
                });
            } else {
                Query<activities> query = activitiesDao.queryBuilder()
                        .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Send_status.eq("MENUNGGU UPLOAD PHOTO"))
                        .build();
                List<activities> result = query.list();
                for (int i = 0; i < result.size(); i++) {
                    result.get(i).setSend_status("SUKSES");
                    result.get(i).setMobile_status("SENT");
                }
                activitiesDao.updateInTx(result);
            }
        }
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    /*
    DOWNLOAD LAST PHOTO
     */
    public  void downloadLastDokumen() throws JSONException, InterruptedException {
        try{
        if(ldJobs.length()>0){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (session.isLogin()) {
                        RequestParams params = new RequestParams();
                        params.put("token", session.getToken());
                        httpClient.addHeader("API-KEY", config.APP_ID);
                        JSONObject data = new JSONObject();
                        try {
                            data.put("orderID", ldJobs.getJSONObject(0).getString("orderID"));
                            data.put("photoType", ldJobs.getJSONObject(0).getString("type"));
                        } catch (JSONException e) {
                            FirebaseCrash.log(e.getMessage());
                        }
                        params.put("data", AES.generateText(data.toString(), 0));
                        httpClient.get(session.getUrl() + config.DOWNLOAD_LD_ENDPOINT, params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                try {
                                    File f = new File(ldJobs.getJSONObject(0).getString("filename"));
                                    if (!f.exists()) {
                                        f.mkdirs();
                                        f.createNewFile();
                                    }
                                    FileOutputStream outputStream = new FileOutputStream(f);
                                    outputStream.write(responseBody);
                                    outputStream.close();
                                    deleteLDJob();
                                    try {
                                        downloadLastDokumen();
                                    } catch (InterruptedException e) {
                                        FirebaseCrash.log(e.getMessage());
                                    }
                                } catch (JSONException | IOException e) {
                                    FirebaseCrash.log(e.getMessage());
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                try {
                                    FirebaseCrash.log(statusCode + " FAILED DOWNLOAD LAST ATTACHMENT FOR USER " + session.getUserdata().getString("userID"));
                                } catch (JSONException e) {
                                    FirebaseCrash.log(e.getMessage());
                                }
                            }
                        });
                    }
                }
            },1000);
        }
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    /*
    Upload Logs
     */
    public void uploadLogs(){
        try{
            int timeout=100;
            if(!initLogs){
                timeout=new Global().LOGS_TIME;
            }else{
                initLogs=false;
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (session.isLogin()) {
                        RequestParams params = new RequestParams();

                        login_logsDao = daoSession.getLogin_logsDao();
                        final Query<login_logs> query;
                        try {

                            query = login_logsDao.queryBuilder()
                                    .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                                    .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Login_date.lt(Utils.dateYesterday()))
                                    .build();
                            final List<login_logs> logList = query.list();
                            JSONArray logArray = new JSONArray();

                            for (int i = 0; i < logList.size(); i++) {
                                JSONObject logObject = new JSONObject();
                                try {
                                    logObject.put("date", logList.get(i).getLogin_date());
                                    logObject.put("login", logList.get(i).getLogin());
                                    logObject.put("logout", logList.get(i).getLogout());
                                    logArray.put(i, logObject);
                                } catch (JSONException e) {
                                    FirebaseCrash.log(e.getMessage());
                                }
                            }
                            Log.i("LOGIN LOG",logArray.toString());
                            String textToSend = AES.generateText(logArray.toString(),0);
                            params.put("histories", textToSend);
                            params.put("token", session.getToken());
                            httpClient.addHeader("API-KEY", config.APP_ID);
                            httpClient.post(session.getUrl() + config.LOGIN_LOG_ENDPOINT, params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    for (int i = 0; i < logList.size(); i++) {
                                        login_logsDao.delete(logList.get(i));
                                    }
                                    uploadLogs();
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    try {
                                        FirebaseCrash.log(statusCode+" LOGIN LOGS FOR USER "+session.getUserdata().getString("userID"));
                                    } catch (Exception e) {
                                        FirebaseCrash.log(e.getMessage());
                                    }
                                    uploadLogs();
                                }
                            });
                        } catch (Exception e) {
                            FirebaseCrash.log(e.getMessage());
                            uploadLogs();
                        }
                    }else {
                        uploadLogs();
                    }
                }
            }, timeout);
        }catch (Exception e){
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void addUploadJob(JSONObject jobs){
        try {
            uploadJobs.put(uploadJobs.length(),jobs);
            JSONObject jobsList=session.getJobs();
            jobsList.put(session.getUserdata().getString("userID"),uploadJobs);
            session.setJobs(jobsList);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void deleteUploadJob(){
        try {
            uploadJobs.remove(0);
            JSONObject jobsList=session.getJobs();
            jobsList.put(session.getUserdata().getString("userID"),uploadJobs);
            session.setJobs(jobsList);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void addLDJob(JSONObject jobs){
        try {
            ldJobs.put(ldJobs.length(),jobs);
            JSONObject jobsList=session.getJobs();
            jobsList.put(session.getUserdata().getString("userID")+"-LD",ldJobs);
            session.setJobs(jobsList);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
    public void deleteLDJob(){
        try {
            ldJobs.remove(0);
            JSONObject jobsList=session.getJobs();
            jobsList.put(session.getUserdata().getString("userID")+"-LD",ldJobs);
            session.setJobs(jobsList);
        } catch (Exception e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
}
