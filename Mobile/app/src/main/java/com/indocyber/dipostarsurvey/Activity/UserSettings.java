package com.indocyber.dipostarsurvey.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.Library.AES;
import com.indocyber.dipostarsurvey.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;

public class
UserSettings extends AppActivity implements View.OnClickListener {
    private EditText txtUserID,txtFullname,txtOPassword,txtNPassword,txtCPassword;
    private Button btnSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        /*
        init all component
         */
        txtUserID= (EditText) findViewById(R.id.editTextUserID);
        txtFullname= (EditText) findViewById(R.id.editTextFullname);
        txtOPassword= (EditText) findViewById(R.id.editTextOPassword);
        txtCPassword= (EditText) findViewById(R.id.editTextCPassword);
        txtNPassword= (EditText) findViewById(R.id.editTextNPassword);
        btnSave= (Button) findViewById(R.id.buttonSaveSetting);
        /*
        set all on click listener
         */
        btnSave.setOnClickListener(this);

        try {
            txtUserID.setText(session.getUserdata().getString("userID"));
            txtFullname.setText(session.getUserdata().getString("username"));
            txtUserID.setEnabled(false);
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    /*
    all on click event is here
     */
    @Override
    public void onClick(View v) {
        /*
        choose action by event
         */
        switch (v.getId()){
            case R.id.buttonSaveSetting :
                String username=txtUserID.getText().toString();
                final String fullname=txtFullname.getText().toString();
                String opassword=txtOPassword.getText().toString();
                String cpassword=txtCPassword.getText().toString();
                String npassword=txtNPassword.getText().toString();
                if(fullname.equals("")){
                    errorDialog("Fullname is required");
                    return;
                }
                if(opassword.equals("")){
                    errorDialog("Old Password is required");
                    return;
                }
                if(!npassword.equals(cpassword)){
                    errorDialog("Password not match");
                    return;
                }
                /*
                show loading
                 */
                loading=new ProgressDialog(this);
                loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                loading.setCancelable(false);
                loading.setMessage("Saving changes ...");
                loading.show();
                /*
                set Params
                 */
                RequestParams params=new RequestParams();
                JSONObject data=new JSONObject();
                params.put("token", session.getToken());
                try {
                    data.put("opassword",opassword);
                    data.put("npassword",npassword);
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                params.put("data", AES.generateText(data.toString(),0));
                httpClient.addHeader("API-KEY", config.APP_ID);
                httpClient.post(session.getUrl()+config.CPASSWORD_ENDPOINT, params, new AsyncHttpResponseHandler() {
                    /*
                     handle success save change
                      */
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        loading.hide();
                        try {
                            String data=new String(responseBody, StandardCharsets.UTF_8);
                            JSONObject response=new JSONObject(AES.realText(data));
                            if(response.getBoolean("status")){
                                JSONObject userData=session.getUserdata();
                                userData.put("isFirstLogin","0");
                                session.setUserData(userData);
                                session.setUserData(session.getUserdata().put("Fullname",fullname));
                                if(!txtNPassword.getText().toString().equals("") && txtNPassword.getText()==null) {
                                    session.setLastPassword(txtNPassword.getText().toString());
                                }
                                Intent m=new Intent(UserSettings.this,Main.class);
                                startActivity(m);
                                finish();
                            }else{
                                errorDialog(response.getString("message"));
                            }
                        } catch (JSONException e) {
                            Log.e(getClass().getSimpleName(),e.getMessage());
                        }
                    }
                    /*
                    handle error save
                     */
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        loading.hide();
                        errorDialog("Server Error ");
                    }
                });
                break;
        }
    }
}
