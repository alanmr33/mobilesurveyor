package com.indocyber.dipostarsurvey.Activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.indocyber.dipostarsurvey.Abstract.UserLoginChildActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.indocyber.dipostarsurvey.db.order_entriesDao;

import org.greenrobot.greendao.query.Query;
import java.io.File;

import static android.view.View.GONE;

public class DetailSPK extends UserLoginChildActivity {
    private PDFView pdfView;
    private PhotoView photoView;
    public order_entries orderEntries;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String OrderID=(getIntent().getStringExtra("ORDER_ID")==null)? "" : getIntent().getStringExtra("ORDER_ID");
        DaoSession daoSession=((App)getApplication()).getDbSession();
        order_entriesDao orderEntriesDao=daoSession.getOrder_entriesDao();
        Query<order_entries> orderEntriesQuery=orderEntriesDao.queryBuilder()
                .where(order_entriesDao.Properties.Order_id.eq(OrderID))
                .build();
        orderEntries=orderEntriesQuery.list().get(0);
        setContentView(R.layout.activity_detail_spk);
        pdfView= (PDFView) findViewById(R.id.pdfViewSPK);
        photoView= (PhotoView) findViewById(R.id.photoSPK);
        File f = getExternalFilesDir("spk-files");
        File att=new File(f.getAbsolutePath()+"/"+orderEntries.getAttachment());
        if(orderEntries.getAttachment().contains(".jpg")){
            pdfView.setVisibility(GONE);
            photoView.setVisibility(View.VISIBLE);
            Bitmap bitmap = BitmapFactory.decodeFile(att.getAbsolutePath());
            photoView.setImageBitmap(bitmap);
        }else{
            pdfView.fromFile(att)
                    .defaultPage(0)
                    .enableSwipe(true)
                    .enableDoubletap(true)
                    .onError(new OnErrorListener() {
                        @Override
                        public void onError(Throwable t) {
                            Toast.makeText(getBaseContext(),"Gagal membuka file lampiran !",Toast.LENGTH_SHORT).show();
                        }
                    })
                    .load();
            pdfView.setVisibility(View.VISIBLE);
            photoView.setVisibility(View.GONE);
        }
    }

}
