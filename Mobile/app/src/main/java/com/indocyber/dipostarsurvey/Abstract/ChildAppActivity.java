package com.indocyber.dipostarsurvey.Abstract;

import android.os.Bundle;

/**
 * Created by Indocyber on 14/07/2017.
 */

public class ChildAppActivity extends AppActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        set home back button
         */
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
