package com.indocyber.dipostarsurvey.Activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.UserLoginChildActivity;
import com.indocyber.dipostarsurvey.Adapter.OrderEntryNewAdapter;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.indocyber.dipostarsurvey.db.order_entriesDao;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONException;

import java.util.List;

public class NewOrder extends UserLoginChildActivity {

    private RecyclerView recyclerViewNewOrder;
    private OrderEntryNewAdapter adapter;
    private List<order_entries> result;
    private Spinner filter;
    private order_entriesDao orderEntriesDao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        DaoSession daoSession = ((App) getApplication()).getDbSession();
        orderEntriesDao = daoSession.getOrder_entriesDao();

        Query<order_entries> query = null;
        try {
            query = orderEntriesDao.queryBuilder()
                    .where(order_entriesDao.Properties.Enabled.eq(true))
                    .where(order_entriesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                    .build();
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        result = query.list();
        recyclerViewNewOrder = (RecyclerView) findViewById(R.id.recylerViewOrder);
        filter = (Spinner) findViewById(R.id.spinnerFilter);

        recyclerViewNewOrder.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewNewOrder.setItemAnimator(new DefaultItemAnimator());
        initAdapter();
        initFilter();
    }

    private void initAdapter() {
        adapter = new OrderEntryNewAdapter(result, R.layout.custom_neworder, getBaseContext(), this);
        recyclerViewNewOrder.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initFilter() {
        ArrayAdapter<CharSequence> adapterF = ArrayAdapter.createFromResource(this, R.array.filter_array, android.R.layout.simple_spinner_item);
        adapterF.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filter.setAdapter(adapterF);
        filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String filterText = filter.getSelectedItem().toString();
                QueryBuilder<order_entries> qb = null;
                try {
                    qb = orderEntriesDao.queryBuilder()
                            .where(order_entriesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                            .where(order_entriesDao.Properties.Enabled.eq(true));
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                if (filterText.equals("No FPP")) {
                    result = qb.orderAsc(order_entriesDao.Properties.Fpp_no).list();
                } else if (filterText.equals("Nama")) {
                    result = qb.orderAsc(order_entriesDao.Properties.Nama_pemesan).list();
                } else  if (filterText.equals("Alamat")){
                    result = qb.orderAsc(order_entriesDao.Properties.Alamat).list();
                } else if (filterText.equals("Tanggal Rencana Kunjungan")) {
                    result = qb.orderAsc(order_entriesDao.Properties.Plan_date).list();
                }else {
                    result = qb.orderAsc(order_entriesDao.Properties.Order_id).list();
                }
                initAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        initFilter();
    }
}
