package com.indocyber.dipostarsurvey.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Bundle;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Library.Utils;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.activities;
import com.indocyber.dipostarsurvey.db.activitiesDao;
import com.indocyber.dipostarsurvey.db.login_logs;
import com.indocyber.dipostarsurvey.db.login_logsDao;
import com.indocyber.dipostarsurvey.db.order_entries;

import org.greenrobot.greendao.query.Query;
import org.json.JSONException;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Splash extends AppActivity implements OnRequestPermissionsResultCallback {
    private TextView textLoading;
    private int currentStep = 1;
    private boolean permissionRequest = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        textLoading = (TextView) findViewById(R.id.textViewLoading);
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        if(!dateFormat.format(Calendar.getInstance().getTime()).equals(session.getLastLogin())){
            if(session.isLogin()){
                try {
                    DaoSession daoSession=((App)getApplication()).getDbSession();
                    login_logsDao login_logsDao=daoSession.getLogin_logsDao();
                    Query<login_logs> login_logsQuery=login_logsDao.queryBuilder()
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Login_date.eq(Utils.dateYesterday()))
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Logout.isNull())
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                            .build();
                    List<login_logs> login_logsList=login_logsQuery.list();
                    if(login_logsList.size()>0){
                        login_logsList.get(0).setLogout("00:00:00");
                        login_logsDao.insertOrReplace(login_logsList.get(0));
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
            session.setLogin(false);
        }
        try {
            /*
            set hash for App
             */
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                session.setHash(Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
            setStep(1);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e(getClass().getSimpleName(), e.getMessage());

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setStep(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /*
    step splash
     */
    private void setStep(final int step) {
        currentStep = step;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (step) {
                    case 1:
                        textLoading.setText("Check Permission ...");
                        permitStep();
                        break;
                    case 2:
                        textLoading.setText("Check App Configuration ...");
                        configStep();
                        break;
                    case 3:
                        textLoading.setText("Load User Information ...");
                        loginStep();
                        break;
                    case 4:
                        textLoading.setText("Get Data From Server ...");
                        syncStep();
                        break;
                }
            }
        }, config.SPLASH);
    }

    /*
    step permission 1
     */
    private void permitStep() {
        if (checkPermission()) {
            setStep(2);
        } else {
            /*
            marshmallow above
             */
            if (Build.VERSION.SDK_INT >= 22) {
                requestPermissions();
            } else {
                permissionRequest = false;
                setStep(2);
            }
        }
    }

    /*
    step configuration 2
     */
    private void configStep() {
         /*
        get device ID
         */
        if (!session.isConfigured()) {
            Intent config = new Intent(this, Configuration.class);
            startActivity(config);
        } else {
            TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            session.setDevice(mngr.getDeviceId());
             setStep(3);
         }
    }
    /*
    step login checking 3
     */
    private void loginStep(){
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = stat.getFreeBytes();
        long megAvailable = bytesAvailable / 1048576;
        if(megAvailable<=100) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Penyimpanan tidak mencukupi untuk menjalankan MSS (< 100 MB)");
            dialog.setNegativeButton("Abaikan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (session.isLogin()) {
                        setStep(4);
                    } else {
                        Intent login = new Intent(Splash.this, Login.class);
                        startActivity(login);
                    }
                }
            });
            dialog.setPositiveButton("Hapus Data Terkirim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    DaoSession daoSession= ((App)getApplication()).getDbSession();
                    Calendar calendar=Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_MONTH,-5);
                    com.indocyber.dipostarsurvey.db.order_entriesDao entriesDao=daoSession.getOrder_entriesDao();
                    com.indocyber.dipostarsurvey.db.activitiesDao activitiesDao=daoSession.getActivitiesDao();
                    Query<activities> activitiesQuery= null;
                    activitiesQuery = activitiesDao.queryBuilder()
                            .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("SENT"))
                            .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Updated_date.le(calendar.getTime()))
                            .build();
                    List<activities> activitiesList=activitiesQuery.list();
                    for (int i=0;i<activitiesList.size();i++){
                        Query<order_entries> query = null;
                        query = entriesDao.queryBuilder()
                                .where(com.indocyber.dipostarsurvey.db.order_entriesDao.Properties.Order_id
                                        .eq(activitiesList.get(i).getOrder_id()))
                                .build();
                        List<order_entries> result = query.list();
                        if(result.get(0).getAttachment()!=null && result.get(0).getAttachment().equals("-")){
                            File spkF = getExternalFilesDir("spk-files");
                            File att=new File(spkF.getAbsolutePath()+"/"+result.get(0).getAttachment());
                            if(att.exists()){
                                att.delete();
                            }
                        }
                        entriesDao.delete(result.get(i));

                        File f = getExternalFilesDir("images/"+activitiesList.get(i).getOrder_id()
                                .replace("/","_"));
                        if(f.exists()){
                            f.delete();
                        }
                        activitiesDao.delete(activitiesList.get(i));
                    }
                    if (session.isLogin()) {
                        setStep(4);
                    } else {
                        Intent login = new Intent(Splash.this, Login.class);
                        startActivity(login);
                        finish();
                    }
                }
            });
            AlertDialog alert = dialog.create();
            alert.show();
        }else{
            if (session.isLogin()) {
                setStep(4);
            } else {
                Intent login = new Intent(Splash.this, Login.class);
                startActivity(login);
                finish();
            }
        }
    }
    /*
    step sync database 4
     */
    private void syncStep() {
            if (session.isSynced()) {
                ((App)getApplicationContext()).mService.getTask();
                Intent main = new Intent(this,Main.class);
                startActivity(main);
                finish();
            }else{
                sync();
            }
    }
    /*
    check permission in realtime
     */
    private Boolean checkPermission(){
        int phoneState= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int storagePermission= ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int storagePermissionW= ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraP= ActivityCompat.checkSelfPermission(this,Manifest.permission.CAMERA);
        int locationCoarse=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationFine=ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        if(phoneState==PackageManager.PERMISSION_GRANTED && storagePermission==PackageManager.PERMISSION_GRANTED && locationCoarse==PackageManager.PERMISSION_GRANTED
                && locationFine==PackageManager.PERMISSION_GRANTED && storagePermissionW==PackageManager.PERMISSION_GRANTED
                &&cameraP==PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }
    }
    //Requesting permission
    private void requestPermissions() {
        if (permissionRequest) {
            permissionRequest=false;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_PHONE_STATE}, config.CODE_PERMISSION);
        }
    }
    /*
    on get permission from intent
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        if(requestCode==config.CODE_PERMISSION){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED){
                setStep(2);
            }else{
                Toast.makeText(this,"Please Grant Our Permission !",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
    public void sync(){
        session.setSynced(true);
        setStep(4);
    }
}

