package com.indocyber.dipostarsurvey.Activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.UserLoginActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Library.Utils;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.Service.AppService;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.Tracking;
import com.indocyber.dipostarsurvey.db.TrackingDao;
import com.indocyber.dipostarsurvey.db.activities;
import com.indocyber.dipostarsurvey.db.activitiesDao;
import com.indocyber.dipostarsurvey.db.login_logs;
import com.indocyber.dipostarsurvey.db.login_logsDao;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.indocyber.dipostarsurvey.db.order_entriesDao;
import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
public class Main extends UserLoginActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener,View.OnClickListener {
    private LinearLayout coordinatorLayout;
    private TextView txtLoginAs;
    private RelativeLayout menuNewOrder;
    private RelativeLayout menuSavedOrder;
    private RelativeLayout menuSentOrder;
    private RelativeLayout menuPendingOrder;
    private RelativeLayout menuAccount;
    private RelativeLayout menuLogout;
    //location code is here
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public double latitude;
    public double longitude;
    private Activity activity;
    /*
    location configuration
     */
    private static final int UPDATE_INTERVAL = 3*60*1000;
    private static final int FATEST_INTERVAL = 1*60*1000;
    private static final int DISPLACEMENT = 200;
    private int resultSum,resultSum2,resultSum3,resultSum4;
    private TextView textNotifBubble,textNotifBubble2,textNotifBubble3,textNotifBubble4;
    private ImageView imgNotifBubble,imgNotifBubble2,imgNotifBubble3,imgNotifBubble4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        init component
         */
        txtLoginAs= (TextView) findViewById(R.id.textViewLoginAs);
        menuAccount= (RelativeLayout) findViewById(R.id.menuAccount);
        menuNewOrder= (RelativeLayout) findViewById(R.id.menuNewOrder);
        menuSavedOrder= (RelativeLayout) findViewById(R.id.menuSavedOrder);
        menuPendingOrder= (RelativeLayout) findViewById(R.id.menuPendingOrder);
        menuSentOrder= (RelativeLayout) findViewById(R.id.menuSentOrder);
        menuLogout= (RelativeLayout) findViewById(R.id.menuLogout);
        coordinatorLayout = (LinearLayout) findViewById(R.id.mainContainer);
        /*
        set listener click here
         */
        menuAccount.setOnClickListener(this);
        menuNewOrder.setOnClickListener(this);
        menuPendingOrder.setOnClickListener(this);
        menuSavedOrder.setOnClickListener(this);
        menuSentOrder.setOnClickListener(this);
        menuLogout.setOnClickListener(this);
        setTitle();
        //location
        activity=this;
        if (checkPlayServices()) {
            checkLocationOn();
            buildGoogleApiClient();
            createLocationRequest();
        }
        notifBubbleChange();
        ((App)getApplicationContext()).location=session.getLocation();
    }
    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
       if(mGoogleApiClient!=null){
           if(mGoogleApiClient.isConnected()) {
               mGoogleApiClient.disconnect();
               stopLocationUpdates();
           }
       }
    }
    @Override
    protected void onResume() {
        super.onResume();
        notifBubbleChange();
        setTitle();
        //check location
        if(checkPlayServices()){
            if(!mGoogleApiClient.isConnected() && mGoogleApiClient != null){
                mGoogleApiClient.connect();
            }
            if (mGoogleApiClient.isConnected() && mGoogleApiClient!=null) {
                startLocationUpdates();
            }
        }
        if(!((App)getApplicationContext()).mBound){
            ((App)getApplicationContext()).bindService();
        }
        autoLogout();
    }
    public void setTitle(){
         /*
        set Login Ass
         */
        try {
            txtLoginAs.setText("Anda berhasil login sebagai "+session.getUserdata().getString("username"));
            if(session.getUserdata().getString("isFirstLogin").equals("1")){
                Intent userConfig=new Intent(this,UserSettings.class);
                startActivity(userConfig);
            }
        } catch (JSONException e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    /*
    all action click here guys
     */
    @Override
    public void onClick(View v) {
        //choose action by element
        switch (v.getId()){
            case R.id.menuAccount :
                /*
                go to user settings
                 */
                Intent userConfig=new Intent(this,UserSettings.class);
                startActivity(userConfig);
                break;
            case R.id.menuLogout :
                /*
                logout User
                 */
                session.setLogin(false);
                session.setSynced(false);
                try {
                    DaoSession daoSession=((App)getApplication()).getDbSession();
                    login_logsDao login_logsDao=daoSession.getLogin_logsDao();
                    SimpleDateFormat dateFormatA=new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat dateFormatB=new SimpleDateFormat("HH:mm:ss");
                    Query<login_logs> login_logsQuery=login_logsDao.queryBuilder()
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Login_date.eq(dateFormatA.format(Calendar.getInstance().getTime())))
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Logout.isNull())
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                            .orderDesc(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Login_date)
                            .build();
                   List<login_logs> login_logsList=login_logsQuery.list();
                   if(login_logsList.size()>0){
                       login_logsList.get(0).setLogout(dateFormatB.format(Calendar.getInstance().getTime()));
                       login_logsDao.insertOrReplace(login_logsList.get(0));
                   }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                Intent logout=new Intent(this,Login.class);
                startActivity(logout);
                finish();
                break;
            case R.id.menuNewOrder :
                /*
                go to pending order page
                 */
                Intent newOrder=new Intent(this, NewOrder.class);
                startActivity(newOrder);
                break;
            case R.id.menuPendingOrder :
                /*
                go to pending order page
                 */
                Intent pendingOrder=new Intent(this, PendingOrder.class);
                startActivity(pendingOrder);
                break;
            case R.id.menuSentOrder :
                /*
                go to sent order page
                 */
                Intent sendtOrder=new Intent(this,SentOrder.class);
                startActivity(sendtOrder);
                break;
            case R.id.menuSavedOrder :
                /*
                go to save order page
                 */
                Intent saveOrder=new Intent(this,SavedOrder.class);
                startActivity(saveOrder);
                break;
        }
    }
    /**
     * Create Api Client
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }
    public void checkLocationOn(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setCancelable(false);
            dialog.setMessage("Hidupkan lokasi anda untuk mengakses survey app !");
            dialog.setPositiveButton("Hidupkan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton("Keluar", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    paramDialogInterface.dismiss();
                    finish();
                }
            });
            dialog.show();
        }
    }
    /**
     check location Service
     **/
    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                int locationRequestCode = 1000;
                Log.e("Error Loc",GoogleApiAvailability.getInstance().getErrorString(resultCode));
                GoogleApiAvailability.getInstance().getErrorDialog(this, locationRequestCode, resultCode).show();
            } else {
                Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG).show();
                finish();
            }
            Log.i("Location Service","Location Service Is Turned Off");
            AlertDialog.Builder msg=new AlertDialog.Builder(activity);
            msg.setCancelable(false);
            msg.setMessage("Please , turn on location to user survey app !");
            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent settings = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(settings);
                }
            });
            msg.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            AlertDialog dialog=msg.create();
            dialog.show();
            return false;
        }
        return true;
    }

    private void updateLocationData() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            ((App)getApplicationContext()).location=latitude + "," + longitude;
            if(mLastLocation.getSpeed()<14){
                if(!session.getLocation().equals(latitude+","+longitude)) {
                    session.setLocation(latitude + "," + longitude);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date now = Calendar.getInstance().getTime();
                    TrackingDao trackingDao = ((App) getApplication()).getDbSession().getTrackingDao();
                    Tracking tracking = new Tracking();
                    tracking.setPosition(latitude + "," + longitude);
                    tracking.setTracking_date(dateFormat.format(now));
                    try {
                        tracking.setUser_id(session.getUserdata().getString("userID"));
                    } catch (JSONException e) {
                        FirebaseCrash.log(e.getMessage());
                    }
                    trackingDao.insertOrReplace(tracking);

                    Log.i("Location Service", "Received location " + latitude + "," + longitude);
                }
            }
        }else{
            Log.i("Location Service","Received location nulled");
            if(mGoogleApiClient.isConnected()){
                startLocationUpdates();
            }else{
                mGoogleApiClient.connect();
                startLocationUpdates();
            }
        }
    }
    protected void createLocationRequest() {
        Log.i("Location Service","Create Location Request");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }
    //receive location
    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        Log.i("Location Service","Starting Update Location");
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        Log.i("Location Service","Stoping Update Location");
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }
    //Requesting permission location
    private void requestPermissions(){
        Boolean locationCoarse=ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        Boolean locationFine=ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if(locationCoarse || locationFine){
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, 110);
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        updateLocationData();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // connect on susspend
        mGoogleApiClient.connect();
        startLocationUpdates();
        updateLocationData();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Location Request", "Connection failed: Error Code = "+ connectionResult.getErrorCode());
        Log.e("Location Request", "Connection failed: Error Message = "+ connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            updateLocationData();
        }
    }

    public void notifBubbleChange(){
        //new order
        textNotifBubble = (TextView)findViewById(R.id.text_notif_bubble);
        imgNotifBubble = (ImageView) findViewById(R.id.img_notif_bubble);
        textNotifBubble2 = (TextView)findViewById(R.id.text_notif_bubble2);
        imgNotifBubble2 = (ImageView) findViewById(R.id.img_notif_bubble2);
        textNotifBubble3 = (TextView)findViewById(R.id.text_notif_bubble3);
        imgNotifBubble3 = (ImageView) findViewById(R.id.img_notif_bubble3);
        textNotifBubble4 = (TextView)findViewById(R.id.text_notif_bubble4);
        imgNotifBubble4 = (ImageView) findViewById(R.id.img_notif_bubble4);
        DaoSession daoSession=((App)getApplication()).getDbSession();
        order_entriesDao entriesDao=daoSession.getOrder_entriesDao();
        Query<order_entries> orderEntriesQuery= null;
        try {
            orderEntriesQuery = entriesDao
                    .queryBuilder()
                    .where(order_entriesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                    .where(order_entriesDao.Properties.Enabled.eq(true))
                    .build();
            List<order_entries> results=orderEntriesQuery.list();
            resultSum=results.size();
            if (resultSum==0){
                textNotifBubble.setVisibility(View.INVISIBLE);
                imgNotifBubble.setVisibility(View.INVISIBLE);
            }
            else if (resultSum>99) {
                textNotifBubble.setText("99+");
                textNotifBubble.setVisibility(View.VISIBLE);
                imgNotifBubble.setVisibility(View.VISIBLE);
            }
            else {
                textNotifBubble.setText(String.valueOf(resultSum));
                textNotifBubble.setVisibility(View.VISIBLE);
                imgNotifBubble.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }

        //sent order
        activitiesDao activitiesDao=daoSession.getActivitiesDao();
        Query<activities> actSent= null;
        try {
            actSent = activitiesDao
                    .queryBuilder()
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("SENT"))
                    .build();
            List<activities> resultSent=actSent.list();
            resultSum2=resultSent.size();
            if (resultSum2==0){
                textNotifBubble2.setVisibility(View.INVISIBLE);
                imgNotifBubble2.setVisibility(View.INVISIBLE);
            }
            else if (resultSum2>99) {
                textNotifBubble2.setText("99+");
                textNotifBubble2.setVisibility(View.VISIBLE);
                imgNotifBubble2.setVisibility(View.VISIBLE);
            }
            else {
                textNotifBubble2.setText(String.valueOf(resultSum2));
                textNotifBubble2.setVisibility(View.VISIBLE);
                imgNotifBubble2.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        //draft order
        Query<activities> actDraft= null;
        try {
            actDraft = activitiesDao
                    .queryBuilder()
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("DRAFT"))
                    .build();
            List<activities> resultDraft=actDraft.list();
            resultSum3=resultDraft.size();
            if (resultSum3==0){
                textNotifBubble3.setVisibility(View.INVISIBLE);
                imgNotifBubble3.setVisibility(View.INVISIBLE);
            }
            else if (resultSum3>99) {
                textNotifBubble3.setText("99+");
                textNotifBubble3.setVisibility(View.VISIBLE);
                imgNotifBubble3.setVisibility(View.VISIBLE);
            }
            else {
                textNotifBubble3.setText(String.valueOf(resultSum3));
                textNotifBubble3.setVisibility(View.VISIBLE);
                imgNotifBubble3.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        //pending order
        Query<activities> actPending= null;
        try {
            actPending = activitiesDao
                    .queryBuilder()
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                    .where(com.indocyber.dipostarsurvey.db.activitiesDao.Properties.Mobile_status.eq("PENDING"))
                    .build();
            List<activities> resultPending=actPending.list();
            resultSum4=resultPending.size();
            if (resultSum4==0){
                textNotifBubble4.setVisibility(View.INVISIBLE);
                imgNotifBubble4.setVisibility(View.INVISIBLE);
            }
            else if (resultSum4>99) {
                textNotifBubble4.setText("99+");
                textNotifBubble4.setVisibility(View.VISIBLE);
                imgNotifBubble4.setVisibility(View.VISIBLE);
            }
            else {
                textNotifBubble4.setText(String.valueOf(resultSum4));
                textNotifBubble4.setVisibility(View.VISIBLE);
                imgNotifBubble4.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifBubbleChange();
            }
        },5000);
    }
    @Override
    public void onBackPressed() {
//       Intent main=new Intent(this,Splash.class);
//       startActivity(main);
       finish();
    }
    public void autoLogout(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
        if(!dateFormat.format(Calendar.getInstance().getTime()).equals(session.getLastLogin())){
            if(session.isLogin()){
                try {
                    DaoSession daoSession=((App)getApplication()).getDbSession();
                    login_logsDao login_logsDao=daoSession.getLogin_logsDao();
                    Query<login_logs> login_logsQuery=login_logsDao.queryBuilder()
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Login_date.eq(Utils.dateYesterday()))
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.Logout.isNull())
                            .where(com.indocyber.dipostarsurvey.db.login_logsDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                            .build();
                    List<login_logs> login_logsList=login_logsQuery.list();
                    if(login_logsList.size()>0){
                        login_logsList.get(0).setLogout("00:00:00");
                        login_logsDao.insertOrReplace(login_logsList.get(0));
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
            session.setLogin(false);
            Intent login=new Intent(this,Login.class);
            startActivity(login);
            finish();
        }
    }
}
