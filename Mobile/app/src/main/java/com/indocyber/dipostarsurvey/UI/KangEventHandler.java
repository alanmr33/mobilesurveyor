package com.indocyber.dipostarsurvey.UI;

import android.view.View;

/**
 * Created by Indocyber on 31/10/2017.
 */

public interface KangEventHandler {
    void onUIEvent(View view,String event, String tagUI, String values);
}
