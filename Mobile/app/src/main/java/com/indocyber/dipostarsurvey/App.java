package com.indocyber.dipostarsurvey;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.indocyber.dipostarsurvey.Service.AppService;
import com.indocyber.dipostarsurvey.db.DaoMaster;
import com.indocyber.dipostarsurvey.db.DaoSession;
import org.greenrobot.greendao.database.Database;

/**
 * Created by Indocyber on 19/07/2017.
 */

public class App extends Application {
    private DaoSession daoSession;
    public AppService mService;
    public boolean mBound = false;
    public String location="";
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            AppService.AppServiceBinder binder = (AppService.AppServiceBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, this.getExternalFilesDir("db")
                .getPath() + "/dsf_surveyor");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        if(!mBound){
            bindService();
        }
    }
    public DaoSession getDbSession() {
        return daoSession;
    }
    public  void bindService(){
        Intent intent = new Intent(getApplicationContext(), AppService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        mBound=true;
    }

}
