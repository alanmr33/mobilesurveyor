package com.indocyber.dipostarsurvey.UI;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.db.fields;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class SelectUI {
    public static final View create(Activity activity, final android.view.View view, final fields props,boolean enable, KangEventHandler eventHandler, Fragment fragment) {
        final LinearLayout component=new LinearLayout(activity);
        if(props.getField_visibility().equalsIgnoreCase("visible")){
            component.setVisibility(View.VISIBLE);
        }else{
            component.setVisibility(View.GONE);
        }
        component.setTag(props.getField_name()+"-layout");
        float weight=1;
        if(!props.getField_weight().replace(",",".").equals("")){
            weight=Float.parseFloat(props.getField_weight().replace(",","."));
        }
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,weight);
        component.setLayoutParams(layoutParams);
        component.setOrientation(LinearLayout.VERTICAL);
        if(!props.getField_label().equals("")) {
            component.addView(LabelUI.create(activity, component, props));
        }
        component.addView(ComboBoxUI.create(activity,component,props,enable,eventHandler,fragment));
        component.setBackgroundResource(0);
        return component;
    }
}
