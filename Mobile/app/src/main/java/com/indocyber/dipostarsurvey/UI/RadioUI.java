package com.indocyber.dipostarsurvey.UI;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Form.FormGenerator;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.fields;
import com.indocyber.dipostarsurvey.db.params;
import com.indocyber.dipostarsurvey.db.paramsDao;
import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class RadioUI {
    public static final View create(final Activity activity, final android.view.View view, final fields props, final boolean enable, final KangEventHandler eventHandler, Fragment fragment) {
        final RadioGroup component=new RadioGroup(activity);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        component.setLayoutParams(layoutParams);
        component.setTag(props.getField_name());
        try {
            JSONObject extras=new JSONObject(props.getField_extra());
            if(extras.has("orientation")){
                if(extras.getString("orientation").equals("vertical")){
                    component.setOrientation(LinearLayout.VERTICAL);
                }else{
                    component.setOrientation(LinearLayout.HORIZONTAL);
                }
            }
            if(extras.has("validation")){
                ((FormGenerator)fragment).validationList.put(props.getField_store(),extras.getJSONObject("validation"));
            }
            if(extras.has("readonly")){
                if (extras.getString("readonly").equals("true")){
                    component.setEnabled(false);
                    component.setClickable(false);
                }
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        DaoSession daoSession=((App)((AppActivity) activity).getApplication()).getDbSession();
        paramsDao paramsDao=daoSession.getParamsDao();
        Query<params> paramsQuery=paramsDao.queryBuilder()
                .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Param_condition.eq(props.getField_global_value()))
                .orderAsc(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Order)
                .build();
        final List<params> paramsList=paramsQuery.list();
        String tagSelected="";
        for (int i=0;i<paramsList.size();i++){
            final params paramsRadio=paramsList.get(i);
            RadioButton radioButton=new RadioButton(activity);
            try {
                JSONObject extras=new JSONObject(props.getField_extra());
                if(extras.has("readonly")){
                    if (extras.getString("readonly").equals("true")){
                        radioButton.setEnabled(false);
                        radioButton.setClickable(false);
                    }
                }
                if(extras.has("validation")){
                    ((FormGenerator)fragment).validationList.put(props.getField_store(),extras.getJSONObject("validation"));
                }
            } catch (JSONException e) {
                FirebaseCrash.log(e.getMessage());
            }
            LinearLayout.LayoutParams radioLayout=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
            radioButton.setLayoutParams(radioLayout);
            radioButton.setText(paramsList.get(i).getParam_description());
            radioButton.setTag(component.getTag().toString()+"-"+paramsList.get(i).getParam_id());
            if(((AppActivity) activity).tempDataStore.containsKey(props.getField_store())){
                if(((AppActivity) activity).tempDataStore.get(props.getField_store()).equals(String.valueOf(paramsRadio.getParam_id()))){
                    tagSelected=radioButton.getTag().toString();
                }
            }
            if(!enable){
                radioButton.setEnabled(false);
            }
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((AppActivity) activity).tempDataStore.put(props.getField_store(),String.valueOf(paramsRadio.getParam_id()));
                    ((AppActivity) activity).tempDataStore.put(props.getField_store()+"-readable",paramsRadio.getParam_description());
                    eventHandler.onUIEvent(component,"change",component.getTag().toString(),String.valueOf(paramsRadio.getParam_id()));
                }
            });
            component.addView(radioButton);
        }

        final String finalTagSelected = tagSelected;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(enable) {
                    eventHandler.onUIEvent(component, "init", props.getField_name(), "");
                }
                if(!finalTagSelected.equals("")){
                    ((RadioButton)component.findViewWithTag(finalTagSelected)).setChecked(true);
                }
            }
        },1000);
        return component;
    }
}