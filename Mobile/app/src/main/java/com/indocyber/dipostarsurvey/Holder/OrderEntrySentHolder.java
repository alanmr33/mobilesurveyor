package com.indocyber.dipostarsurvey.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indocyber.dipostarsurvey.R;

/**
 * Created by Indocyber on 26/07/2017.
 */

public class OrderEntrySentHolder extends RecyclerView.ViewHolder {
    public TextView txtOrderID, txtFppNo,txtNama,txtAlamat,txtNoHP,txtPlanDate;
    public LinearLayout mainLayout;
    public OrderEntrySentHolder(View itemView) {
        super(itemView);
        txtPlanDate= (TextView) itemView.findViewById(R.id.txtPlanDate);
        txtOrderID= (TextView) itemView.findViewById(R.id.txtNewOrderID);
        txtFppNo= (TextView) itemView.findViewById(R.id.txtFppNo);
        txtNama= (TextView) itemView.findViewById(R.id.txtNewOrderNama);
        txtNoHP= (TextView) itemView.findViewById(R.id.txtNewOrderNoHP);
        txtAlamat= (TextView) itemView.findViewById(R.id.txtNewOrderAlamat);
        mainLayout= (LinearLayout) itemView.findViewById(R.id.itemNewOrder);
    }
}
