package com.indocyber.dipostarsurvey.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Activity.FormInput;
import com.indocyber.dipostarsurvey.Activity.LogActivity;
import com.indocyber.dipostarsurvey.Holder.OrderEntryPendingHolder;
import com.indocyber.dipostarsurvey.db.activities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by user on 10/19/2017.
 */

public class OrderEntryPendingAdapter extends RecyclerView.Adapter<OrderEntryPendingHolder> {

    private List<activities> activities;
    private int layout;
    private Context context;
    private Activity activity;

    public OrderEntryPendingAdapter(List<activities> activities, int layout, Context context, Activity activity) {
        this.activities = activities;
        this.layout = layout;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public OrderEntryPendingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new OrderEntryPendingHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderEntryPendingHolder holder, int position) {

        try {
            final activities activityData = activities.get(position);
            final JSONObject jsonObject=new JSONObject(activityData.getBody());
            SimpleDateFormat formated=new SimpleDateFormat("dd MMM yyyy HH:mm");
            if(activityData.getPlan_date()!=null) {
                holder.txtPlanDate.setText(formated.format(activityData.getPlan_date()));
            }else{
                holder.txtPlanDate.setText("-");
            }
            holder.txtOrderID.setText(jsonObject.getString("survey-orderId"));
            holder.txtFppNo.setText(jsonObject.getString("fpp_no"));
            holder.txtNama.setText(jsonObject.getString("nama_pemesan"));
            holder.txtAlamat.setText(jsonObject.getString("alamat"));
            holder.txtNoHP.setText(jsonObject.getString("nohp"));
            holder.txtStatus.setText(activityData.getSend_status());
            holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent editForm=new Intent(context, FormInput.class);
                    editForm.putExtra("ORDER_ID",activityData.getOrder_id());
                    editForm.putExtra("readonly",true);
                    editForm.putExtra("lastdata",jsonObject.toString());
                    editForm.putExtra("mobile-status",activityData.getMobile_status());
                    editForm.putExtra("activity-id",activityData.getActivity_id());
                    activity.startActivity(editForm);
                }
            });
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return this.activities.size();
    }
}
