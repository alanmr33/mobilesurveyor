package com.indocyber.dipostarsurvey.UI;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.db.fields;
import com.indocyber.dipostarsurvey.db.rows;

import java.util.List;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class RowsUI {
    public static final View create(Activity activity, android.view.View view, final rows props, boolean enable, KangEventHandler eventHandler, Fragment fragment) {
        List<fields> fieldsList=props.getFieldsList();
        if(fieldsList.size()>0) {
            final LinearLayout component = new LinearLayout(activity);
            component.setTag("rows-" + props.getRow_id());
            if (props.getRow_visibility().equalsIgnoreCase("visible")) {
                component.setVisibility(View.VISIBLE);
            } else {
                component.setVisibility(View.GONE);
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            layoutParams.leftMargin = 10;
            layoutParams.rightMargin = 10;
            layoutParams.topMargin = 3;
            layoutParams.bottomMargin = 3;
            component.setLayoutParams(layoutParams);
            component.setOrientation(LinearLayout.HORIZONTAL);
            for (int i = 0; i < fieldsList.size(); i++) {
                fields f = fieldsList.get(i);
                switch (f.getField_type()) {
                    case "button":
                        component.addView(ButtonUI.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                    case "read_mode_button":
                        if(!enable){
                            component.addView(ButtonUI.create(activity, component, f, true, eventHandler, fragment));
                        }
                        break;
                    case "text":
                        component.addView(TextBoxUI.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                    case "radio":
                        component.addView(RadioButtonUI.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                    case "select":
                        component.addView(SelectUI.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                    case "title":
                        component.addView(TitleUI.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                    case "checkbox":
                        component.addView(CheckUI.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                    case "photo":
                        component.addView(PhotoPicker.create(activity, component, f, enable, eventHandler, fragment));
                        break;
                }
            }
            return component;
        }
        return  null;
    }
}
