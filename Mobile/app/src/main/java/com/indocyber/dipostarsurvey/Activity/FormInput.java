package com.indocyber.dipostarsurvey.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.UserLoginActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Form.Form1;
import com.indocyber.dipostarsurvey.Form.FormGenerator;
import com.indocyber.dipostarsurvey.Library.DateTimePicker;
import com.indocyber.dipostarsurvey.Library.OnSetValue;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.UI.MD5;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.activities;
import com.indocyber.dipostarsurvey.db.activitiesDao;
import com.indocyber.dipostarsurvey.db.forms;
import com.indocyber.dipostarsurvey.db.formsDao;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.indocyber.dipostarsurvey.db.order_entriesDao;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;
import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class FormInput extends UserLoginActivity implements StepperLayout.StepperListener {
    private ImageView imgSpinner;
    private FrameLayout containerFirstForm;
    private Toolbar toolbarForm;
    private StepperLayout formWizard;
    private TextView title;
    private FragmentTransaction transaction;
    public boolean isCompany=false;
    private List<Step> companies=new ArrayList<>();
    private List<String> companiesTitle=new ArrayList<>();
    private List<String> individualTitle=new ArrayList<>();
    private List<Step> individual=new ArrayList<>();
    private LinearLayout toolbarTitle;
    public order_entries orderEntries;
    public boolean readonly,init=false;
    public ImageView prev,next;
    private String mobileStatus,activityID;
    private order_entriesDao orderEntriesDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        readonly=getIntent().getBooleanExtra("readonly",false);
        mobileStatus=(getIntent().getStringExtra("mobile-status")!=null)? getIntent().getStringExtra("mobile-status") : "NEW";
        activityID=(getIntent().getStringExtra("activity-id")==null)? "" : getIntent().getStringExtra("activity-id");
        setContentView(R.layout.activity_form);
        /*
        INIT ORDER ID
         */
        DaoSession daoSession=((App)getApplication()).getDbSession();
        orderEntriesDao=daoSession.getOrder_entriesDao();
        Query<order_entries> orderEntriesQuery=orderEntriesDao.queryBuilder()
                .where(order_entriesDao.Properties.Order_id.eq(getIntent().getStringExtra("ORDER_ID")))
                .build();
        orderEntries=orderEntriesQuery.list().get(0);
        /*
        INIT UI
         */
        toolbarForm= (Toolbar) findViewById(R.id.toolbarNewOrder);
        toolbarTitle= (LinearLayout) toolbarForm.findViewById(R.id.linearLayoutTitle);
        imgSpinner= (ImageView) toolbarForm.findViewById(R.id.imgSpinner);
        title= (TextView) toolbarForm.findViewById(R.id.title_NewOrder);
        prev= (ImageView) toolbarForm.findViewById(R.id.imageViewPrev);
        prev.setVisibility(View.GONE);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevForm();
            }
        });
        next= (ImageView) toolbarForm.findViewById(R.id.imageViewNext);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextFormGo();
            }
        });
        next.setVisibility(View.GONE);
        containerFirstForm= (FrameLayout) findViewById(R.id.firstForm);
        formWizard= (StepperLayout) findViewById(R.id.stepperLayoutForm);
        setSupportActionBar(toolbarForm);
        formsDao formsDao=daoSession.getFormsDao();
        Query<forms> fomsQuery=formsDao.queryBuilder()
                .build();

        List<forms> formsList=fomsQuery.list();
        Log.i("FORM","jumlah "+formsList.size());
        for (int i=0;i<formsList.size();i++){
           if(formsList.get(i).getForm_id().contains("IND")){
               individualTitle.add(formsList.get(i).getForm_label());
               individual.add(FormGenerator.newInstance(formsList.get(i).getForm_id()));
           }else{
               companiesTitle.add(formsList.get(i).getForm_label());
               companies.add(FormGenerator.newInstance(formsList.get(i).getForm_id()));
           }
        }
        try {
            JSONObject lastDataInput=new JSONObject(getIntent().getStringExtra("lastdata"));
            Iterator<String> keyIterator=lastDataInput.keys();
            while (keyIterator.hasNext()){
                String key=keyIterator.next();
                if(!key.equals("cancelStatus")) {
                    tempDataStore.put(key, lastDataInput.getString(key));
                }
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        tempDataStore.put("survey-orderId",orderEntries.getOrder_id());
        tempDataStore.put("nama_pemesan",orderEntries.getNama_pemesan());
        tempDataStore.put("nohp",orderEntries.getNohp());
        tempDataStore.put("notes",orderEntries.getNotes());
        tempDataStore.put("alamat",orderEntries.getAlamat());
        tempDataStore.put("created_date",orderEntries.getCreated_date());
        tempDataStore.put("fpp_no",orderEntries.getFpp_no());
        if(tempDataStore.containsKey("isCompany")){
            if(tempDataStore.get("isCompany").equals("true")){
                isCompany=true;
            }else{
                isCompany=false;
            }
        }
        initFirstForm();
        if(!readonly){
            autoSave();
        }
    }
    public void nextFormGo(){
        if(isCompany){
            if((formWizard.getCurrentStepPosition()+1)>=companies.size()) {
                save();
            }else {
                if (formWizard.getCurrentStepPosition() < companies.size()) {
                    formWizard.setCurrentStepPosition(formWizard.getCurrentStepPosition() + 1);
                }
            }
        }else{
            if((formWizard.getCurrentStepPosition()+1)>=individual.size()) {
                save();
            }else {
                if (formWizard.getCurrentStepPosition() < individual.size()) {
                    formWizard.setCurrentStepPosition(formWizard.getCurrentStepPosition() + 1);
                }
            }
        }
        prev.setVisibility(View.VISIBLE);
    }
    public void prevForm(){
        if(isCompany){
            if(formWizard.getCurrentStepPosition()>0){
                if(formWizard.getCurrentStepPosition()-1==0){
                    prev.setVisibility(View.INVISIBLE);
                }
                formWizard.setCurrentStepPosition(formWizard.getCurrentStepPosition()-1);
            }
        }else{
            if(formWizard.getCurrentStepPosition()>0){
                if(formWizard.getCurrentStepPosition()-1==0){
                    prev.setVisibility(View.INVISIBLE);
                }
                formWizard.setCurrentStepPosition(formWizard.getCurrentStepPosition()-1);
            }
        }
    }
    public void initFirstForm(){
        transaction = getSupportFragmentManager().beginTransaction();
        containerFirstForm.setVisibility(View.VISIBLE);
        formWizard.setVisibility(View.GONE);
        title.setText("INISIASI");
        Form1 firstForm=new Form1();
        transaction.replace(R.id.firstForm,firstForm);
        transaction.commit();
    }
    public void tundaKunjungan(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_reason);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //set action button
        final CheckBox checkBoxIsMet= (CheckBox) dialog.findViewById(R.id.checkBoxIsMeet);
        final CheckBox checkBoxIsCalled= (CheckBox) dialog.findViewById(R.id.checkBoxIsCall);
        final EditText edtDateE= (EditText) dialog.findViewById(R.id.edt_dateE);
        final ImageView imgE= (ImageView) dialog.findViewById(R.id.imageViewDPE);
        final EditText edtNoteE= (EditText) dialog.findViewById(R.id.edt_noteE);
        SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        edtDateE.setText(df.format(Calendar.getInstance().getTime()));
        edtDateE.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DateTimePicker dp=new DateTimePicker();
                    dp.setOnSetvalue(new OnSetValue() {
                        @Override
                        public void OnSet(String data) {
                            edtDateE.setText(data);
                        }
                    });
                    dp.show(getFragmentManager(),null);
                }
            }
        });
        imgE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTimePicker dp=new DateTimePicker();
                dp.setOnSetvalue(new OnSetValue() {
                    @Override
                    public void OnSet(String data) {
                        edtDateE.setText(data);
                    }
                });
                dp.show(getFragmentManager(),null);
            }
        });
        Button buttonNoCancel= (Button) dialog.findViewById(R.id.buttonReasonNo);
        Button buttonYesCancel= (Button) dialog.findViewById(R.id.buttonReasonYes);
        buttonNoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        buttonYesCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SimpleDateFormat oldFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm");
                SimpleDateFormat newFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String isCalled=(checkBoxIsCalled.isChecked())? "0" : "1";
                String isMet=(checkBoxIsMet.isChecked())? "0" : "1";
                String nextDate=edtDateE.getText().toString();
                String remarkCancel=edtNoteE.getText().toString();
                try {
                    String finalDate=newFormat.format(oldFormat.parse(nextDate));
                    tempDataStore.put("expectedDate",finalDate);
                } catch (ParseException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                tempDataStore.put("cancelStatus","DELAY");
                tempDataStore.put("isCalled",isCalled);
                tempDataStore.put("isMet",isMet);
                tempDataStore.put("SurveyResult",remarkCancel);
                init=false;
                saveActivity("PENDING");
                finish();
            }
        });
        dialog.show();
    }
    public void setupPickUp(){
        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_pickup);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //set action button
        final ImageView imgP= (ImageView) dialog.findViewById(R.id.imageViewDPP);
        final EditText edtDateP= (EditText) dialog.findViewById(R.id.edt_dateP);
        SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        edtDateP.setText(df.format(Calendar.getInstance().getTime()));
        edtDateP.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    DateTimePicker dp=new DateTimePicker();
                    dp.setOnSetvalue(new OnSetValue() {
                        @Override
                        public void OnSet(String data) {
                            edtDateP.setText(data);
                        }
                    });
                    dp.show(getFragmentManager(),null);
                }
            }
        });
        imgP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTimePicker dp=new DateTimePicker();
                dp.setOnSetvalue(new OnSetValue() {
                    @Override
                    public void OnSet(String data) {
                        edtDateP.setText(data);
                    }
                });
                dp.show(getFragmentManager(),null);
            }
        });
        Button buttonNoCancel= (Button) dialog.findViewById(R.id.buttonReasonNo);
        Button buttonYesCancel= (Button) dialog.findViewById(R.id.buttonReasonYes);
        final EditText editTextRemarkP= (EditText) dialog.findViewById(R.id.edt_noteP);
        buttonNoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        buttonYesCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SimpleDateFormat oldFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm");
                SimpleDateFormat newFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String nextDate=edtDateP.getText().toString();
                try {
                    String finalDate=newFormat.format(oldFormat.parse(nextDate));
                    tempDataStore.put("expectedDate",finalDate);
                } catch (ParseException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                tempDataStore.put("cancelStatus","PICKUP");
                init=false;
                tempDataStore.put("SurveyResult",editTextRemarkP.getText().toString());
                saveActivity("PENDING");
                finish();
            }
        });
        dialog.show();
    }
    public void backDialog(){
        String addTxt=(!readonly && init)? ("\n (Data otomatis disimpan 'SIMPAN ORDER LIST'.)") : "";
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Kembali Ke Halaman Sebelumnya ? " +
                addTxt);
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                autoSave();
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    public void setCompanyForm(boolean status){
        isCompany=status;
        if(isCompany){
            Log.d("FORM","Form Perusahaan");
        }else{
            Log.d("FORM","Form Individu");
        }
    }
    public void nextForm(){
        init=true;
        formWizard= (StepperLayout) findViewById(R.id.stepperLayoutForm);
        containerFirstForm.setVisibility(View.GONE);
        formWizard.setVisibility(View.VISIBLE);
        formWizard.setListener(this);
        if(isCompany) {
            formWizard.setAdapter(new StepperdAdapter(getSupportFragmentManager(), this,companies,companiesTitle.toArray(new String[companiesTitle.size()])));
            if(tempDataStore.containsKey("SurveyStatus")){
                if(!tempDataStore.get("SurveyStatus").equals("65")){
                    formWizard.setCurrentStepPosition(companies.size()-1);
                    next.setVisibility(View.INVISIBLE);
                    prev.setVisibility(View.INVISIBLE);
                    title.setText("Rekomendasi & Submit Survey");
                }else{
                    toolbarTitle.setBackgroundResource(R.drawable.bg_spinner);
                    toolbarTitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder=new AlertDialog.Builder(FormInput.this);
                            String[] titles=companiesTitle.toArray(new String[companiesTitle.size()]);
                            builder.setItems(titles, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(which==0){
                                        prev.setVisibility(View.INVISIBLE);
                                    }else{
                                        prev.setVisibility(View.VISIBLE);
                                    }
                                    formWizard.setCurrentStepPosition(which);
                                    if(which==individual.size()-1){
                                        next.setVisibility(View.INVISIBLE);
                                    }else {
                                        next.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            AlertDialog alertDialog=builder.create();
                            alertDialog.show();
                        }
                    });
                    imgSpinner.setVisibility(View.VISIBLE);
                }
            }
        }else{
            formWizard.setAdapter(new StepperdAdapter(getSupportFragmentManager(), this,individual,individualTitle.toArray(new String[individualTitle.size()])));
            if(tempDataStore.containsKey("SurveyStatus")){
                if(!tempDataStore.get("SurveyStatus").equals("65")){
                    formWizard.setCurrentStepPosition(individual.size()-1);
                    next.setVisibility(View.INVISIBLE);
                    prev.setVisibility(View.INVISIBLE);
                    title.setText("Rekomendasi & Submit Survey");
                }else{
                    toolbarTitle.setBackgroundResource(R.drawable.bg_spinner);
                    toolbarTitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder=new AlertDialog.Builder(FormInput.this);
                            String[] titles=individualTitle.toArray(new String[individualTitle.size()]);
                            builder.setItems(titles, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(which==0){
                                        prev.setVisibility(View.INVISIBLE);
                                    }else{
                                        prev.setVisibility(View.VISIBLE);
                                    }
                                    formWizard.setCurrentStepPosition(which);
                                    if(which==individual.size()-1){
                                        next.setVisibility(View.INVISIBLE);
                                    }else {
                                        next.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            AlertDialog alertDialog=builder.create();
                            alertDialog.show();
                        }
                    });
                    imgSpinner.setVisibility(View.VISIBLE);
                }
            }
        }
        next.setVisibility(View.VISIBLE);
    }
    public void save(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Submit data hasil survey ?");
        builder.setCancelable(false);
        builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(tempDataStore.get("SurveyStatus").equals("65")){
                    if(!tempDataStore.containsKey("cancelStatus")){
                        if(tempDataStore.containsKey("Photo1-file") &&
                                tempDataStore.containsKey("Photo1Loc")){
                            if(!tempDataStore.get("Photo1-file").equals("") &&
                                    !tempDataStore.get("Photo1Loc").equals("")){
                                ((App) getApplication()).mService.doUpload();
                                saveActivity("PENDING");
                                finish();
                            }else{
                                Toast.makeText(getBaseContext(), "Foto 1 & Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(getBaseContext(), "Foto 1 & Lokasinya Harus Diisi", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        ((App) getApplication()).mService.doUpload();
                        saveActivity("PENDING");
                        finish();
                    }
                }else {
                    ((App) getApplication()).mService.doUpload();
                    saveActivity("PENDING");
                    finish();
                }
            }
        });
        builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    @Override
    public void onCompleted(View completeButton) {
       save();
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {
        autoSave();
        if(newStepPosition==0){
            prev.setVisibility(View.INVISIBLE);
        }
        if(isCompany){
            if(newStepPosition==companies.size()-1){
                next.setVisibility(View.INVISIBLE);
            }else {
                next.setVisibility(View.VISIBLE);
            }
        }else{
            if(newStepPosition==individual.size()-1){
                next.setVisibility(View.INVISIBLE);
            }else {
                next.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onReturn() {

    }

    private class StepperdAdapter extends AbstractFragmentStepAdapter {
        private List<Step> steps;
        private String[] titles;
        StepperdAdapter(FragmentManager fm, Context context,List<Step> steps,String[] titles) {
            super(fm, context);
            this.steps=steps;
            this.titles=titles;
        }

        @Override
        public Step createStep(int position) {
           return steps.get(position);
        }

        @Override
        public int getCount() {
            return steps.size();
        }

        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {

            if(tempDataStore.get("SurveyStatus").equals("65")){
                title.setText(titles[position]);
            }
            StepViewModel.Builder builder = new StepViewModel.Builder(context)
                    .setTitle("");
            return builder.create();
        }
    }
    @Override
    public void onBackPressed() {
        if(init && !readonly){
            saveActivity("DRAFT");
        }
        backDialog();
    }
    public void saveActivity(String mobileStatus){
        JSONObject jsonObject = new JSONObject();
        DaoSession daoSession = ((App) getApplication()).getDbSession();
        activitiesDao activitiesDao = daoSession.getActivitiesDao();
        activities activities = new activities();
        if(!activityID.equals("")){
            activities.setActivity_id(activityID);
        }else {
            activities.setActivity_id(MD5.enc(tempDataStore.get("survey-orderId") + Calendar.getInstance().getTimeInMillis()));
        }
        activities.setCreated_date(Calendar.getInstance().getTime());

        try {
            if(orderEntries.getPlan_date()!=null) {
                activities.setPlan_date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(orderEntries.getPlan_date()));
            }else{
                activities.setPlan_date(null);
            }
        } catch (ParseException e) {
            FirebaseCrash.log(e.getMessage());
        }
        activities.setUpdated_date(Calendar.getInstance().getTime());
        activities.setOrder_id(tempDataStore.get("survey-orderId"));
        activities.setSend_status("0");
        Set<String> validKeys = tempDataStore.keySet();
        try {
            for (String s : validKeys) {
                jsonObject.put(s, tempDataStore.get(s));
            }
            jsonObject.put("isCompany", String.valueOf(isCompany));
            jsonObject.put("FPPNO",orderEntries.getFpp_no());
            jsonObject.put("NamaPemesan",orderEntries.getNama_pemesan());
            jsonObject.put("Alamat",orderEntries.getAlamat());
            jsonObject.put("NoHP",orderEntries.getNohp());
            jsonObject.put("Notes",orderEntries.getNotes());
            jsonObject.put("SubmitLocation",session.getLocation());
            jsonObject.put("SubmitDate",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        activities.setBody(jsonObject.toString());
        activities.setMobile_status(mobileStatus);
        if(mobileStatus.equals("PENDING")){
            activities.setSend_status("ANTRIAN");
        }
        activities.setFpp_no(orderEntries.getFpp_no());
        activities.setNama_pemesan(orderEntries.getNama_pemesan());
        activities.setAlamat(orderEntries.getAlamat());
        activities.setNohp(orderEntries.getNohp());
        activities.setNotes(orderEntries.getNotes());
        try {
            activities.setUser_id(session.getUserdata().getString("userID"));
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        if(!activityID.equals("")){
            activityID=activities.getActivity_id();
            activitiesDao.update(activities);
        }else {
            activityID=activities.getActivity_id();
            activitiesDao.insert(activities);
        }
        orderEntries.setEnabled(false);
        orderEntriesDao.update(orderEntries);
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    public void autoSave(){
        if(init && !readonly){
            saveActivity("DRAFT");
        }
    }
}
