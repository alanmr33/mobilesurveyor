package com.indocyber.dipostarsurvey.Abstract;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.indocyber.dipostarsurvey.Activity.Configuration;
import com.indocyber.dipostarsurvey.Config.Global;
import com.indocyber.dipostarsurvey.Library.Resizer;
import com.indocyber.dipostarsurvey.Library.Session;
import com.indocyber.dipostarsurvey.UI.KangEventHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.yan.netmanager.NetStatus;
import com.yan.netmanager.NetStatusManager;

import java.io.File;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;


/**
 * Created by Indocyber on 14/07/2017.
 */

public class AppActivity extends AppCompatActivity{
    public Boolean resultConnection = false;
    public Global config;
    public Session session;
    public AsyncHttpClient httpClient;
    public ProgressDialog loading;
    public HashMap<String,String> tempDataStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(getClass().getSimpleName(), "====onCreate===");
        //set configuration
        config = new Global();
        //create session
        session = new Session(this);
        //create http client
        httpClient = new AsyncHttpClient();
        httpClient.setConnectTimeout(300000);
        //create DB
        tempDataStore=new HashMap<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(getClass().getSimpleName(), "====onStart===");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(getClass().getSimpleName(), "====onPause===");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(getClass().getSimpleName(), "====onResume===");
//        refreshNetworkStatus();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(getClass().getSimpleName(), "====onStop===");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(getClass().getSimpleName(), "====onDestroy===");
    }

    /*
    public test connection
     */
    public void testConnection(String siteURL) {
         /*
        set loading
         */
        loading = new ProgressDialog(this);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setCancelable(false);
        loading.setMessage("Connecting to server ...");
        loading.show();
        httpClient.get(siteURL, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                loading.hide();
                resultConnection = true;
                Toast.makeText(getBaseContext(), "Connected to server.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                loading.hide();
                Toast.makeText(getBaseContext(), "Can't connect to server!", Toast.LENGTH_SHORT).show();
                resultConnection = true;
            }
        });
    }

    /*
    error dialog
     */
    public void errorDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*
    setting dialog
     */
    public void settingDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent setting = new Intent(getBaseContext(), Configuration.class);
                startActivity(setting);
            }
        });
        builder.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*
    get network status
     */
    public NetStatus getNetworkStatus() {
        return NetStatusManager.getInstance().getNetStatus();
    }
    /*
    is network connected
     */
    public boolean isConnected(){
        if(getNetworkStatus().equals(NetStatus.UNKNOW)){
            return false;
        }else{
            return true;
        }
    }
    public void refreshNetworkStatus(){
        // refresh network status
        NetStatusManager.getInstance().refreshStatus();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String refreshKey=tempDataStore.get("active-photo-picker")+"-refresh";
        if (requestCode == 1150 && resultCode == RESULT_OK) {
            Resizer.resizeBitmapFile(tempDataStore.get(tempDataStore.get("active-photo-picker")+"-file"),300);
            tempDataStore.put(refreshKey,"true");
        }else{
            tempDataStore.put(refreshKey,"false");
            File check=new File(tempDataStore.get(tempDataStore.get("active-photo-picker")+"-file"));
            if(check.exists()){
                if(check.length()==0){
                    check.delete();
                    tempDataStore.remove(tempDataStore.get("active-photo-picker")+"-file");
                }
            }
        }
    }
}