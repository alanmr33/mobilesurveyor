package com.indocyber.dipostarsurvey.Library;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import com.indocyber.dipostarsurvey.Form.FormGenerator;

/**
 * Created by Indocyber on 17/11/2017.
 */

public abstract class Dialog {
    public static void alert(Activity activity, final FormGenerator generator, String message, final String tag){
        AlertDialog.Builder builder=new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                generator.onUIEvent(null,"click",tag,"OK");
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
    public static void confirm(Activity activity, final FormGenerator generator, String message, final String yes, final String no, final String tag){
        AlertDialog.Builder builder=new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                generator.onUIEvent(null,"click",tag,yes);
            }
        });
        builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                generator.onUIEvent(null,"click",tag,no);
            }
        });
        AlertDialog dialog=builder.create();
        dialog.show();
    }
}
