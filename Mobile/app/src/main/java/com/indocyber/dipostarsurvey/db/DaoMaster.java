package com.indocyber.dipostarsurvey.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

import org.greenrobot.greendao.AbstractDaoMaster;
import org.greenrobot.greendao.database.StandardDatabase;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseOpenHelper;
import org.greenrobot.greendao.identityscope.IdentityScopeType;


// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/**
 * Master of DAO (schema version 1): knows all DAOs.
 */
public class DaoMaster extends AbstractDaoMaster {
    public static final int SCHEMA_VERSION = 1;

    /** Creates underlying database table using DAOs. */
    public static void createAllTables(Database db, boolean ifNotExists) {
        TrackingDao.createTable(db, ifNotExists);
        login_logsDao.createTable(db, ifNotExists);
        order_entriesDao.createTable(db, ifNotExists);
        activitiesDao.createTable(db, ifNotExists);
        activity_logsDao.createTable(db, ifNotExists);
        formsDao.createTable(db, ifNotExists);
        rowsDao.createTable(db, ifNotExists);
        fieldsDao.createTable(db, ifNotExists);
        eventsDao.createTable(db, ifNotExists);
        paramsDao.createTable(db, ifNotExists);
        postcodeDao.createTable(db, ifNotExists);
    }

    /** Drops underlying database table using DAOs. */
    public static void dropAllTables(Database db, boolean ifExists) {
        TrackingDao.dropTable(db, ifExists);
        login_logsDao.dropTable(db, ifExists);
        order_entriesDao.dropTable(db, ifExists);
        activitiesDao.dropTable(db, ifExists);
        activity_logsDao.dropTable(db, ifExists);
        formsDao.dropTable(db, ifExists);
        rowsDao.dropTable(db, ifExists);
        fieldsDao.dropTable(db, ifExists);
        eventsDao.dropTable(db, ifExists);
        paramsDao.dropTable(db, ifExists);
        postcodeDao.dropTable(db, ifExists);
    }

    /**
     * WARNING: Drops all table on Upgrade! Use only during development.
     * Convenience method using a {@link DevOpenHelper}.
     */
    public static DaoSession newDevSession(Context context, String name) {
        Database db = new DevOpenHelper(context, name).getWritableDb();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }

    public DaoMaster(SQLiteDatabase db) {
        this(new StandardDatabase(db));
    }

    public DaoMaster(Database db) {
        super(db, SCHEMA_VERSION);
        registerDaoClass(TrackingDao.class);
        registerDaoClass(login_logsDao.class);
        registerDaoClass(order_entriesDao.class);
        registerDaoClass(activitiesDao.class);
        registerDaoClass(activity_logsDao.class);
        registerDaoClass(formsDao.class);
        registerDaoClass(rowsDao.class);
        registerDaoClass(fieldsDao.class);
        registerDaoClass(eventsDao.class);
        registerDaoClass(paramsDao.class);
        registerDaoClass(postcodeDao.class);
    }

    public DaoSession newSession() {
        return new DaoSession(db, IdentityScopeType.Session, daoConfigMap);
    }

    public DaoSession newSession(IdentityScopeType type) {
        return new DaoSession(db, type, daoConfigMap);
    }

    /**
     * Calls {@link #createAllTables(Database, boolean)} in {@link #onCreate(Database)} -
     */
    public static abstract class OpenHelper extends DatabaseOpenHelper {
        public OpenHelper(Context context, String name) {
            super(context, name, SCHEMA_VERSION);
        }

        public OpenHelper(Context context, String name, CursorFactory factory) {
            super(context, name, factory, SCHEMA_VERSION);
        }

        @Override
        public void onCreate(Database db) {
            Log.i("greenDAO", "Creating tables for schema version " + SCHEMA_VERSION);
            createAllTables(db, false);
        }
    }

    /** WARNING: Drops all table on Upgrade! Use only during development. */
    public static class DevOpenHelper extends OpenHelper {
        public DevOpenHelper(Context context, String name) {
            super(context, name);
        }

        public DevOpenHelper(Context context, String name, CursorFactory factory) {
            super(context, name, factory);
        }

        @Override
        public void onUpgrade(Database db, int oldVersion, int newVersion) {
            Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
            dropAllTables(db, true);
            onCreate(db);
        }
    }

}
