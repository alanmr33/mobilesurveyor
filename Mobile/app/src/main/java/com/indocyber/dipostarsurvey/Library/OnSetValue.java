package com.indocyber.dipostarsurvey.Library;

/**
 * Created by Indocyber on 14/12/2017.
 */

public interface OnSetValue {
    public void OnSet(String data);
}
