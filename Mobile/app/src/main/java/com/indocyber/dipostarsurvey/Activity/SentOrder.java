package com.indocyber.dipostarsurvey.Activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.UserLoginChildActivity;
import com.indocyber.dipostarsurvey.Adapter.OrderEntryPendingAdapter;
import com.indocyber.dipostarsurvey.Adapter.OrderEntrySentAdapter;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.activities;
import com.indocyber.dipostarsurvey.db.activitiesDao;
import com.indocyber.dipostarsurvey.db.order_entries;
import com.indocyber.dipostarsurvey.db.order_entriesDao;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.json.JSONException;

import java.util.List;

public class SentOrder extends UserLoginChildActivity {
    private RecyclerView recyclerViewOrder;
    private OrderEntrySentAdapter adapter;
    private List<activities> result;
    private Spinner filter;
    private activitiesDao orderEntriesDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_order);
        //set database
        DaoSession daoSession = ((App) getApplication()).getDbSession();
        orderEntriesDao = daoSession.getActivitiesDao();
        Query<activities> query = null;
        try {
            query = orderEntriesDao.queryBuilder()
                    .where(activitiesDao.Properties.Mobile_status.eq("SENT"))
                    .where(activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                    .build();
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        result = query.list();
        //init component
        recyclerViewOrder = (RecyclerView) findViewById(R.id.recylerViewOrder);
        filter = (Spinner) findViewById(R.id.spinnerFilter);
        //configure recycle view
        recyclerViewOrder.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewOrder.setItemAnimator(new DefaultItemAnimator());
        initAdapter();
        initFilter();
    }

    private void initAdapter() {
        adapter = new OrderEntrySentAdapter(result, R.layout.custom_sentorder, getBaseContext(), this);
        recyclerViewOrder.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initFilter() {
        ArrayAdapter<CharSequence> adapterF = ArrayAdapter.createFromResource(this, R.array.filter_array, android.R.layout.simple_spinner_item);
        adapterF.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filter.setAdapter(adapterF);
        filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String filterText = filter.getSelectedItem().toString();
                QueryBuilder<activities> query = null;
                try {
                    query = orderEntriesDao.queryBuilder()
                            .where(activitiesDao.Properties.User_id.eq(session.getUserdata().getString("userID")))
                            .where(activitiesDao.Properties.Mobile_status.eq("SENT"));
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                if (filterText.equals("No FPP")) {
                    result = query.orderAsc(activitiesDao.Properties.Fpp_no).list();
                }else if(filterText.equals("Nama")){
                    result = query.orderAsc(activitiesDao.Properties.Nama_pemesan).list();
                }else if(filterText.equals("Alamat")){
                    result = query.orderAsc(activitiesDao.Properties.Alamat).list();
                }else if(filterText.equals("Tanggal Rencana Kunjungan")){
                    result = query.orderAsc(activitiesDao.Properties.Plan_date).list();
                }else{
                    result = query.orderAsc(activitiesDao.Properties.Order_id).list();
                }
                initAdapter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        initFilter();
    }
}
