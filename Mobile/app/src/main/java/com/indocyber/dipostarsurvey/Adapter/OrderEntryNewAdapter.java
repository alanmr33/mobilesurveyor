package com.indocyber.dipostarsurvey.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Activity.FormInput;
import com.indocyber.dipostarsurvey.Activity.NewOrder;
import com.indocyber.dipostarsurvey.Holder.OrderEntryNewHolder;
import com.indocyber.dipostarsurvey.db.order_entries;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 10/17/2017.
 */

public class OrderEntryNewAdapter extends RecyclerView.Adapter<OrderEntryNewHolder> {

    private int layout;
    private List <order_entries> orderEntries;
    private Context context;
    private Activity activity;

    public OrderEntryNewAdapter (List<order_entries> orderEntries, int layout, Context context, Activity activity) {
        this.orderEntries = orderEntries;
        this.layout = layout;
        this.context = context;
        this.activity = activity;
    }


    @Override
    public OrderEntryNewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
        return new OrderEntryNewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderEntryNewHolder holder, int position) {

        final order_entries order_entries = orderEntries.get(position);
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formated=new SimpleDateFormat("dd MMM yyyy HH:mm");
        if(order_entries.getPlan_date()!=null) {
            try {
                Date plan = dateFormat.parse(order_entries.getPlan_date());
                holder.txtPlanDate.setText(formated.format(plan));
            } catch (ParseException e) {
                FirebaseCrash.log(e.getMessage());
            }
        }else{
            holder.txtPlanDate.setText("-");
        }
        holder.txtOrderID.setText(order_entries.getOrder_id());
        holder.txtfppNo.setText(order_entries.getFpp_no());
        holder.txtNama.setText(order_entries.getNama_pemesan());
        holder.txtAlamat.setText(order_entries.getAlamat());
        holder.txtNoHP.setText(order_entries.getNohp());

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editForm=new Intent(activity, FormInput.class);
                editForm.putExtra("ORDER_ID",order_entries.getOrder_id());
                editForm.putExtra("readonly",false);
                String lastdata=(order_entries.getLast_data()!=null)? order_entries.getLast_data() : "{}";
                JSONObject data= null;
                try {
                    data = new JSONObject(lastdata);
                    if(data.has("survey-orderId")){
                        data.remove("survey-orderId");
                    }
                    if(data.has("SurveyStatus")){
                        data.remove("SurveyStatus");
                    }
                    if(data.has("SurveyStatus-readable")){
                        data.remove("SurveyStatus-readable");
                    }
                    if(data.has("SurveyResult")){
                        data.remove("SurveyResult");
                    }
                    if(data.has("Recommendation")){
                        data.remove("Recommendation");
                    }
                    if(data.has("cancelStatus")){
                        data.remove("cancelStatus");
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
                editForm.putExtra("lastdata",lastdata);
                activity.startActivity(editForm);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.orderEntries.size();
    }
}
