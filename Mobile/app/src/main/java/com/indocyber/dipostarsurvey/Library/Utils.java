package com.indocyber.dipostarsurvey.Library;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Indocyber on 16/03/2018.
 */

public abstract class Utils {
    public static String dateYesterday(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cl=Calendar.getInstance();
        cl.add(Calendar.DAY_OF_MONTH,-1);
        Date date=cl.getTime();
        return dateFormat.format(date);
    }
}
