package com.indocyber.dipostarsurvey.Library;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;

import com.google.firebase.crash.FirebaseCrash;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Indocyber on 28/07/2017.
 */

public abstract class Resizer {
    /*
    resize image here
     */
    public static Bitmap resizeBitmap(Bitmap bitmap, int targetW, int targetH) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
        byte[] array = byteArrayOutputStream .toByteArray();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(array,0,array.length,bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        return BitmapFactory.decodeByteArray(array,0,array.length,bmOptions);
    }
    /*
    resize image here
     */
    public static void resizeBitmapFile(String filename, int targetW) {
        try {
            ExifInterface Exif;
            Exif = new ExifInterface(filename);
            String exifOrientation = Exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filename,bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            int targetH=photoH/ (photoW/targetW);
            int scaleFactor = 1;
            if ((targetW > 0)) {
                scaleFactor = Math.min(photoW/targetW, photoH/targetH);
            }
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            Bitmap b2 = BitmapFactory.decodeFile(filename,bmOptions);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            b2.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
            File f = new File(filename);
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(f);
                fo.write(outStream.toByteArray());
                fo.close();
            } catch (IOException e) {
                FirebaseCrash.log(e.getMessage());
            }
            if (exifOrientation != null) {
                ExifInterface newExif = new ExifInterface(filename);
                newExif.setAttribute(ExifInterface.TAG_ORIENTATION, exifOrientation);
                newExif.saveAttributes();
            }
        } catch (IOException e) {
            FirebaseCrash.log(e.getMessage());
        }
    }
}
