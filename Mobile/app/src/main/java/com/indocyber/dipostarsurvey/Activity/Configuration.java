package com.indocyber.dipostarsurvey.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.ChildAppActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Config.Global;
import com.indocyber.dipostarsurvey.Library.AES;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.events;
import com.indocyber.dipostarsurvey.db.eventsDao;
import com.indocyber.dipostarsurvey.db.fields;
import com.indocyber.dipostarsurvey.db.fieldsDao;
import com.indocyber.dipostarsurvey.db.forms;
import com.indocyber.dipostarsurvey.db.formsDao;
import com.indocyber.dipostarsurvey.db.params;
import com.indocyber.dipostarsurvey.db.paramsDao;
import com.indocyber.dipostarsurvey.db.postcode;
import com.indocyber.dipostarsurvey.db.postcodeDao;
import com.indocyber.dipostarsurvey.db.rows;
import com.indocyber.dipostarsurvey.db.rowsDao;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class Configuration extends ChildAppActivity implements View.OnClickListener {
    public TextView txtDevID, txtAppID,txtLogicID;
    public EditText serverURL;
    public Button buttonSave, buttonTest;
    public ImageButton buttonConnection;
    public DaoSession daoSession;
    private Activity activity;
    private int activeObject=0;
    private JSONArray objectList;
    private paramsDao paramsDao;
    private formsDao formsDao;
    private eventsDao eventsDao;
    private rowsDao rowsDao;
    private fieldsDao fieldsDao;
    private postcodeDao postcodeDao;
    private int totalSize;
    private int currentSize;
    private boolean finished=false;
    private String newVersion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(R.layout.activity_configuration);
        daoSession = ((App) getApplication()).getDbSession();
        //init component
        serverURL = (EditText) findViewById(R.id.editTextURL);
        buttonSave = (Button) findViewById(R.id.buttonSaveConfiguration);
        buttonTest = (Button) findViewById(R.id.buttonConnectionTest);
        buttonConnection = (ImageButton) findViewById(R.id.imageButtonTestConnection);
        txtDevID = (TextView) findViewById(R.id.textViewDevID);
        txtAppID = (TextView) findViewById(R.id.textViewAppID);
        txtLogicID = (TextView) findViewById(R.id.textViewLogicID);
        //set on click button
        buttonSave.setOnClickListener(this);
        buttonTest.setOnClickListener(this);
        buttonConnection.setOnClickListener(this);
        /*
        set app information
         */
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            txtAppID.setText("V.1.0.5");
            String mode=(config.API.contains("qa"))? "-DEVQA" : "";
            txtAppID.setText("V."+pInfo.versionName+mode);
//            txtLogicID.setText("V.1.0.1");
            txtLogicID.setText(session.getVersion());
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            txtDevID.setText(mngr.getDeviceId());

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
        serverURL.setText(session.getUrl());
        eventsDao=daoSession.getEventsDao();
        paramsDao=daoSession.getParamsDao();
        fieldsDao=daoSession.getFieldsDao();
        rowsDao=daoSession.getRowsDao();
        formsDao=daoSession.getFormsDao();
        postcodeDao=daoSession.getPostcodeDao();
    }
    /*
    on click all component
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSaveConfiguration :
                if(URLUtil.isValidUrl(serverURL.getText().toString())){
                    //save config & back
                    session.setURL(serverURL.getText().toString());
                    session.setConfigured(true);
                    finish();
                }else{
                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.buttonConnectionTest :
                if(URLUtil.isValidUrl(serverURL.getText().toString())) {
                    testConnection(serverURL.getText().toString());
                }else{
                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.imageButtonTestConnection :
                if(URLUtil.isValidUrl(serverURL.getText().toString())) {
                    testConnection(serverURL.getText().toString());
                }else{
                    Toast.makeText(this,"Invalid URL",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    public void testConnection(final String siteURL) {
         /*
        set loading
         */
        loading = new ProgressDialog(this);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loading.setCancelable(false);
        loading.setMessage("Connecting to server ...");
        loading.show();
        httpClient.addHeader("API-KEY",config.APP_ID);
        totalSize=0;
        currentSize=0;
        httpClient.get(siteURL, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                resultConnection = true;
                Toast.makeText(getBaseContext(), "Connected to server.", Toast.LENGTH_SHORT).show();
                session.setURL(siteURL);
                sync();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Can't connect to server!", Toast.LENGTH_SHORT).show();
                resultConnection = true;
            }
        });
    }
    public void sync() {
          /*
        set loading
         */
        loading.setMessage("Sync Questioner Lists ...");
        httpClient.addHeader("API-KEY",config.APP_ID);
        activeObject=0;
        RequestParams params=new RequestParams();
        params.put("version",txtLogicID.getText().toString());
        httpClient.get(session.getUrl()+new Global().SYNC_ENDPOINT, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                resultConnection = true;
                String data=new String(responseBody, StandardCharsets.UTF_8);
                try {
                    JSONObject response=new JSONObject(AES.realText(data));
                    objectList=response.getJSONArray("data");
                    newVersion=response.getString("version");
                    loading.setMessage("Sync Get Logics ...");
                    if(newVersion.equals(session.getVersion())){
                        finished=true;
                    }else{
                        finished=false;
                        if(objectList.length()>0){
                            getObject(0);
                        }
                    }
                    loadingUpdater();
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                } catch (InterruptedException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Can't connect to server!", Toast.LENGTH_SHORT).show();
                resultConnection = true;
            }
        });
    }
    public void getObject(final int page) throws JSONException, InterruptedException {
        final String object_name=objectList.getString(activeObject);
        httpClient.addHeader("API-KEY",config.APP_ID);
        httpClient.get(session.getUrl()+new Global().SYNC_OBJ_ENDPOINT+"/"+object_name+"/"+page, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                resultConnection = true;
                String data=new String(responseBody, StandardCharsets.UTF_8);
                new syncTask(object_name,page).execute(data);
                try {
                    JSONObject response=new JSONObject(AES.realText(data));
                    JSONArray lists=response.getJSONArray("data");
                    totalSize+=lists.length();
                    if(lists.length() == 0){
                        if(activeObject<objectList.length()-1){
                            activeObject++;
                            getObject(0);
                        }else{
                            if(activeObject==objectList.length()-1){
                                activeObject++;
                            }
                        }
                    }else{
                        getObject(page+1);
                    }
                    if(response.getBoolean("finished")){
                        finished=true;
                    }
                    Log.i("APP","FINISHED "+response.getBoolean("finished"));
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                } catch (InterruptedException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                loading.dismiss();
                Toast.makeText(getBaseContext(), "Can't connect to server!", Toast.LENGTH_SHORT).show();
                Log.i("ERROR",statusCode+" response");
                resultConnection = true;
            }
        });
    }
    public void loadingUpdater(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loading.setMessage("Synchronizing  "+(activeObject+1)+" of "+(objectList.length()));
                Log.i("Finished","Is Finished "+finished);
                if(finished && currentSize>=totalSize){
                    Log.i("Finished","Is Finished "+finished);
                    loading.hide();
                    AlertDialog.Builder builder= new AlertDialog.Builder(activity);
                    txtLogicID.setText(newVersion);
                    builder.setMessage("Versi logic terbaru sudah terinstall");
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            session.setVersion(txtLogicID.getText().toString());
                        }
                    });
                    builder.show();
                    buttonSave.setVisibility(View.VISIBLE);
                    buttonTest.setVisibility(View.GONE);
                }else {
                    loadingUpdater();
                }
            }
        },1000);
    }
    private class syncTask extends AsyncTask<String,Integer,Long>{
        private String object_name;
        private int page;
        private syncTask(String object_name,int page){
            this.object_name=object_name;
            this.page=page;
        }
        @Override
        protected Long doInBackground(String... strings) {
            for(int a=0;a<strings.length;a++){
                try {
                    JSONObject response=new JSONObject(AES.realText(strings[a]));
                    JSONArray lists=response.getJSONArray("data");
                    if(Objects.equals(object_name, "dsf_mobile_forms")){
                        forms[] forms=new forms[lists.length()];
                        for (int i=0;i<lists.length();i++){
                            forms[i]=new forms();
                            forms[i].setForm_id(lists.getJSONObject(i).getString("form_id"));
                            forms[i].setForm_label(lists.getJSONObject(i).getString("form_label"));
                            forms[i].setForm_name(lists.getJSONObject(i).getString("form_name"));
                            forms[i].setForm_order(lists.getJSONObject(i).getInt("form_order"));
                            currentSize+=1;
                        }
                        formsDao.insertOrReplaceInTx(forms);
                    }
                    if(Objects.equals(object_name, "dsf_mobile_form_rows")){
                        rows[] rows=new rows[lists.length()];
                        for (int i=0;i<lists.length();i++){
                            rows[i]=new rows();
                            rows[i].setForm_id(lists.getJSONObject(i).getString("form_id"));
                            rows[i].setRow_id(lists.getJSONObject(i).getString("row_id"));
                            rows[i].setRow_visibility(lists.getJSONObject(i).getString("row_visibility"));
                            rows[i].setRow_order(lists.getJSONObject(i).getInt("row_order"));
                            currentSize+=1;
                        }
                        rowsDao.insertOrReplaceInTx(rows);
                    }
                    if(Objects.equals(object_name, "dsf_mobile_row_fields")){
                        fields[] fields=new fields[lists.length()];
                        for (int i=0;i<lists.length();i++){
                            fields[i]=new fields();
                            fields[i].setRow_id(lists.getJSONObject(i).getString("row_id"));
                            fields[i].setField_id(lists.getJSONObject(i).getString("field_id"));
                            if(lists.getJSONObject(i).has("field_extra")) {
                                fields[i].setField_extra(lists.getJSONObject(i).getString("field_extra"));
                            }else{
                                fields[i].setField_extra("{}");
                            }
                            if(lists.getJSONObject(i).has("field_global_value")) {
                                fields[i].setField_global_value(lists.getJSONObject(i).getString("field_global_value"));
                            }
                            fields[i].setField_name(lists.getJSONObject(i).getString("field_name"));
                            fields[i].setField_type(lists.getJSONObject(i).getString("field_type"));
                            if(lists.getJSONObject(i).has("field_store")) {
                                fields[i].setField_store(lists.getJSONObject(i).getString("field_store"));
                            }
                            fields[i].setField_weight(lists.getJSONObject(i).getString("field_weight"));
                            fields[i].setField_visibility(lists.getJSONObject(i).getString("field_visibility"));
                            if(lists.getJSONObject(i).has("field_label")) {
                                fields[i].setField_label(lists.getJSONObject(i).getString("field_label"));
                            }
                            currentSize+=1;
                        }
                        fieldsDao.insertOrReplaceInTx(fields);
                    }
                    if(Objects.equals(object_name, "dsf_master_parameter")){
                        params[] params=new params[lists.length()];
                        for (int i=0;i<lists.length();i++){
                            params[i]=new params();
                            params[i].setParam_id(lists.getJSONObject(i).getString("param_ID"));
                            if(lists.getJSONObject(i).has("parent_ID")){
                                params[i].setParent_id(lists.getJSONObject(i).getString("parent_ID"));
                            }
                            params[i].setParam_condition(lists.getJSONObject(i).getString("condition"));
                            params[i].setParam_description(lists.getJSONObject(i).getString("level_param"));
                            if(lists.getJSONObject(i).has("param_order")) {
                                params[i].setOrder(lists.getJSONObject(i).getLong("param_order"));
                            }
                            currentSize+=1;
                        }
                        paramsDao.insertOrReplaceInTx(params);
                    }
                    if(Objects.equals(object_name, "dsf_mobile_ui_events")){
                        events[] events=new events[lists.length()];
                        for (int i=0;i<lists.length();i++){
                            events[i]=new events();
                            events[i].setEvent_id(lists.getJSONObject(i).getString("event_id"));
                            events[i].setEvent_component(lists.getJSONObject(i).getString("event_component"));
                            events[i].setEvent_component_target(lists.getJSONObject(i).getString("event_component_target"));
                            events[i].setEvent_type(lists.getJSONObject(i).getString("event_type"));
                            events[i].setEvent_data(lists.getJSONObject(i).getString("event_data"));
                            events[i].setForm_id(lists.getJSONObject(i).getString("form_id"));
                            currentSize+=1;
                        }
                        eventsDao.insertOrReplaceInTx(events);
                    }
                    if(Objects.equals(object_name, "dsf_mst_postcode")){
                        postcode[] postcodes=new postcode[lists.length()];
                        for (int i=0;i<lists.length();i++){
                            postcodes[i]=new postcode();
                            postcodes[i].setProv_param(lists.getJSONObject(i).getString("ProvParam"));
                            postcodes[i].setKab_param(lists.getJSONObject(i).getString("KotaParam"));
                            postcodes[i].setKec_param(lists.getJSONObject(i).getString("KecParam"));
                            postcodes[i].setKel_param(lists.getJSONObject(i).getString("KelParam"));
                            postcodes[i].setPost_code(lists.getJSONObject(i).getString("PostCode"));
                            currentSize+=1;
                        }
                        postcodeDao.insertOrReplaceInTx(postcodes);
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
            return null;
        }
    }
}
