package com.indocyber.dipostarsurvey.UI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.Form.FormGenerator;
import com.indocyber.dipostarsurvey.Library.Formatting;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.fields;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class EditTextUI {
    public static final View create(final Activity activity, final View view, final fields props, boolean enable, final KangEventHandler eventHandler, Fragment fragment) {
        final EditText component=new EditText(activity);
        component.setTag(props.getField_name());
        if(!enable){
            component.setEnabled(false);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            component.setBackground(activity.getResources().getDrawable(R.drawable.input_bg,null));
        }else{
            component.setBackground(activity.getResources().getDrawable(R.drawable.input_bg));
        }
        component.setSingleLine(true);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        component.setPadding(3,8,3,8);
        try {
            JSONObject extras=new JSONObject(props.getField_extra());
            if(extras.has("placeholder")){
                component.setHint(extras.getString("placeholder"));
            }
            if(extras.has("subtype")){
                switch (extras.getString("subtype")){
                    default:
                        component.setInputType(InputType.TYPE_CLASS_TEXT);
                        layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                        break;
                    case "multiline" :
                        component.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                        component.setSingleLine(false);
                        if(extras.has("lines")){
                            int count=Integer.valueOf(extras.getString("lines"))*20;
                            layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                            component.setMinHeight(count);
                        }else{
                            layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                        }
                        break;
                    case  "phone"  :
                        component.setInputType(InputType.TYPE_CLASS_PHONE);
                        layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                        break;
                    case  "email"   :
                        component.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                        break;
                    case  "money"   :
                        component.setInputType(InputType.TYPE_CLASS_NUMBER);
                        component.setFilters(new InputFilter[] {new InputFilter.LengthFilter(22)});
                        layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                        component.setGravity(Gravity.RIGHT);
                    case  "number"   :
                        component.setInputType(InputType.TYPE_CLASS_NUMBER);
                        layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
                        component.setGravity(Gravity.RIGHT);
                        break;
                }
            }
            if(extras.has("readonly")){
                if (extras.getString("readonly").equals("true")){
                    component.setEnabled(false);
                }
            }
            if(extras.has("max_length")){
                component.setFilters(new InputFilter[] {new InputFilter.LengthFilter(extras.getInt("max_length"))});
            }
            if(extras.has("validation")){
                ((FormGenerator)fragment).validationList.put(props.getField_store(),extras.getJSONObject("validation"));
            }
            if(extras.has("default")){
                component.setText(extras.getString("default"));
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        component.setLayoutParams(layoutParams);
        if(((AppActivity) activity).tempDataStore.containsKey(props.getField_store())){
            component.setText(((AppActivity) activity).tempDataStore.get(props.getField_store()));
        }
        component.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("DefaultLocale")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        JSONObject extras = new JSONObject(props.getField_extra());
                        if (extras.has("autocap")) {
                            if (extras.getString("autocap").equals("true")) {
                                if(!s.toString().equals(s.toString().toUpperCase())) {
                                    int last=component.getSelectionStart();
                                    String txt = s.toString().toUpperCase();
                                    component.setText(txt);
                                    component.setSelection(last);
                                    eventHandler.onUIEvent(component,"change",component.getTag().toString(),component.getText().toString());
                                }
                            }
                        }
                        if (extras.has("subtype")) {
                            if (extras.getString("subtype").equals("money")) {
                                if(!s.toString().equals(Formatting.currency(Formatting.getCleanResultLong(s.toString())))) {
                                    if (!s.equals("")) {
                                        int last=component.getSelectionStart();
                                        String txt = Formatting.currency(Formatting.getCleanResultLong(s.toString()));
                                        component.setText(txt);
                                        if(last<txt.length()) {
                                            component.setSelection(last);
                                        }else{
                                            component.setSelection(txt.length());
                                        }
                                        eventHandler.onUIEvent(component,"change",component.getTag().toString(),txt);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        FirebaseCrash.log(e.getMessage());
                    }
            }

            @Override
            public void afterTextChanged(Editable s) {
                JSONObject extras = null;
                try {
                    extras = new JSONObject(props.getField_extra());
                    if (extras.has("autocap")) {
                        ((AppActivity) activity).tempDataStore.put(props.getField_store(),s.toString().toUpperCase());
                        eventHandler.onUIEvent(component,"change",component.getTag().toString(),s.toString().toUpperCase());
                    }else{
                        if (extras.has("subtype")) {
                            if (extras.getString("subtype").equals("money")) {
                                String txt = Formatting.currency(Formatting.getCleanResultLong(s.toString()));
                                ((AppActivity) activity).tempDataStore.put(props.getField_store(),txt);
                                eventHandler.onUIEvent(component,"change",component.getTag().toString(),txt);
                            }else{
                                ((AppActivity) activity).tempDataStore.put(props.getField_store(),s.toString());
                                eventHandler.onUIEvent(component,"change",component.getTag().toString(),s.toString());
                            }
                        }else {
                            ((AppActivity) activity).tempDataStore.put(props.getField_store(),s.toString());
                            eventHandler.onUIEvent(component,"change",component.getTag().toString(),s.toString());
                        }
                    }
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
        });
        if(enable){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    eventHandler.onUIEvent(component,"init",props.getField_name(),"");
                }
            },1000);
        }
        return component;
    }
}
