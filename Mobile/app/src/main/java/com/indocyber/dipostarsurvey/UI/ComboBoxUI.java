package com.indocyber.dipostarsurvey.UI;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.firebase.crash.FirebaseCrash;
import com.indocyber.dipostarsurvey.Abstract.AppActivity;
import com.indocyber.dipostarsurvey.App;
import com.indocyber.dipostarsurvey.Form.FormGenerator;
import com.indocyber.dipostarsurvey.R;
import com.indocyber.dipostarsurvey.db.DaoSession;
import com.indocyber.dipostarsurvey.db.fields;
import com.indocyber.dipostarsurvey.db.params;
import com.indocyber.dipostarsurvey.db.paramsDao;
import org.greenrobot.greendao.query.Query;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Indocyber on 31/10/2017.
 */

public abstract class
ComboBoxUI {
    public static final View create(final Activity activity, final android.view.View view, final fields props, final boolean enable, final KangEventHandler eventHandler, Fragment fragment) {
        DaoSession daoSession=((App)((AppActivity) activity).getApplication()).getDbSession();
        final paramsDao paramsDao=daoSession.getParamsDao();
        JSONObject parent=null;
        final  LinearLayout component=new LinearLayout(activity);
        LinearLayout.LayoutParams comLayoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        component.setTag(props.getField_name()+"-main");
        component.setLayoutParams(comLayoutParams);
        component.setPadding(0,2,0,2);
        final Spinner componentSpinner=new Spinner(activity);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,1);
        componentSpinner.setLayoutParams(layoutParams);
        componentSpinner.setPadding(3,10,3,10);
        if(enable){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                component.setBackground(activity.getResources().getDrawable(R.drawable.ic_rounded_white_focus,null));
            }else{
                component.setBackground(activity.getResources().getDrawable(R.drawable.ic_rounded_white_focus));
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                component.setBackground(activity.getResources().getDrawable(R.drawable.ic_rounded_white,null));
            }else{
                component.setBackground(activity.getResources().getDrawable(R.drawable.ic_rounded_white));
            }
        }
        try {
            JSONObject extras = new JSONObject(props.getField_extra());
            if(extras.has("validation")){
                ((FormGenerator)fragment).validationList.put(props.getField_store(),extras.getJSONObject("validation"));
            }
            if(extras.has("parent")){
                parent=extras.getJSONObject("parent");
                ArrayList<CharSequence> listValue=new ArrayList<>();
                listValue.add("---Pilih---");
                if(((AppActivity) activity).tempDataStore.containsKey(parent.getString("parent_id"))){
                    Query<params> paramsQuery=paramsDao.queryBuilder()
                            .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Param_condition.eq(props.getField_global_value()))
                            .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Parent_id.eq(((AppActivity) activity).tempDataStore.get(parent.getString("parent_id"))))
                            .orderAsc(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Order)
                            .build();
                    final List<params> paramsList=paramsQuery.list();
                    listValue=new ArrayList<>();
                    listValue.add("---Pilih---");
                    int active=0;
                    for (int i=0;i<paramsList.size();i++){
                        params paramsCombo=paramsList.get(i);
                        listValue.add(paramsCombo.getParam_description());
                        if(((AppActivity) activity).tempDataStore.containsKey(props.getField_store())){
                            if(((AppActivity) activity).tempDataStore.get(props.getField_store())
                                    .equals(String.valueOf(paramsCombo.getParam_id()))){
                                active=i+1;
                            }
                        }
                    }
                    ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(activity.getBaseContext(), android.R.layout.simple_spinner_item,listValue);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    componentSpinner.setAdapter(adapter);
                    componentSpinner.setSelection(active);
                }else{
                    ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(activity.getBaseContext(), android.R.layout.simple_spinner_item,listValue);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    componentSpinner.setAdapter(adapter);
                }
            }else{
                Query<params> paramsQuery=paramsDao.queryBuilder()
                        .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Param_condition.eq(props.getField_global_value()))
                        .orderAsc(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Order)
                        .build();
                final List<params> paramsList=paramsQuery.list();
                ArrayList<CharSequence> listValue=new ArrayList<>();
                listValue.add("---Pilih---");
                int active=0;
                for (int i=0;i<paramsList.size();i++){
                    params paramsCombo=paramsList.get(i);
                    listValue.add(paramsCombo.getParam_description());
                    if(((AppActivity) activity).tempDataStore.containsKey(props.getField_store())){
                        if(((AppActivity) activity).tempDataStore.get(props.getField_store())
                                .equals(String.valueOf(paramsCombo.getParam_id()))){
                            active=i+1;
                        }
                    }
                }
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(activity.getBaseContext(), android.R.layout.simple_spinner_item,listValue);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                componentSpinner.setAdapter(adapter);
                componentSpinner.setSelection(active);
            }
            if(!enable){
                componentSpinner.setEnabled(false);
            }
            componentSpinner.setTag(props.getField_name());
            if(extras.has("readonly")){
                if (extras.getString("readonly").equals("true")){
                    componentSpinner.setEnabled(false);
                    componentSpinner.setClickable(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        component.setBackground(activity.getResources().getDrawable(R.drawable.ic_rounded_white,null));
                    }else{
                        component.setBackground(activity.getResources().getDrawable(R.drawable.ic_rounded_white));
                    }
                }
            }
            componentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                    if(id!=0){
                        Query<params> paramsQuery=paramsDao.queryBuilder()
                                .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Param_condition.eq(props.getField_global_value()))
                                .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Param_description.eq(componentSpinner.getSelectedItem().toString()))
                                .build();
                        ((AppActivity) activity).tempDataStore.put(props.getField_store(),String.valueOf(paramsQuery.list().get(0).getParam_id()));
                        ((AppActivity) activity).tempDataStore.put(props.getField_store()+"-readable",paramsQuery.list().get(0).getParam_description());
                        ((AppActivity) activity).tempDataStore.put(props.getField_store()+"-reinit","true");
                        eventHandler.onUIEvent(componentSpinner,"change",componentSpinner.getTag().toString(),String.valueOf(paramsQuery.list().get(0).getParam_id()));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            if(parent!=null){
                ReInit(componentSpinner,((AppActivity) activity),paramsDao,props,parent);
            }
        } catch (JSONException e) {
            FirebaseCrash.log(e.getMessage());
        }
        component.addView(componentSpinner);
        if(enable){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    eventHandler.onUIEvent(component,"init",props.getField_name(),"");
                }
            },1000);
        }
        return component;
    }
    public static void ReInit(final Spinner spinner, final AppActivity appActivity, final paramsDao paramsDao, final fields props, final JSONObject parent) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if(appActivity.tempDataStore.containsKey(parent.getString("parent_id")+"-reinit")){
                        if(appActivity.tempDataStore.get(parent.getString("parent_id")+"-reinit").equals("true")){
                            appActivity.tempDataStore.put(parent.getString("parent_id")+"-reinit","false");
                            Query<params> paramsQuery;
                            try {
                                paramsQuery = paramsDao.queryBuilder()
                                        .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Param_condition.eq(props.getField_global_value()))
                                        .where(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Parent_id.eq(appActivity.tempDataStore.get(parent.getString("parent_id"))))
                                        .orderAsc(com.indocyber.dipostarsurvey.db.paramsDao.Properties.Order)
                                        .build();
                                final List<params> paramsList=paramsQuery.list();
                                ArrayList<CharSequence> listValue=new ArrayList<>();
                                listValue.add("---Pilih---");
                                int active=0;
                                for (int i=0;i<paramsList.size();i++){
                                    listValue.add(paramsList.get(i).getParam_description());
                                    if(String.valueOf(paramsList.get(i).getParam_id()).equals(appActivity.tempDataStore.get(props.getField_store()))){
                                        active=i+1;
                                    }
                                }
                                ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(appActivity.getBaseContext(), android.R.layout.simple_spinner_item,listValue);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                adapter.notifyDataSetChanged();
                                spinner.setAdapter(adapter);
                                spinner.setSelection(active);
                                appActivity.tempDataStore.put(props.getField_name()+"-reinit","false");
                            } catch (JSONException e) {
                                FirebaseCrash.log(e.getMessage());
                            }
                        }
                    }
                    ReInit(spinner,appActivity,paramsDao,props,parent);
                } catch (JSONException e) {
                    FirebaseCrash.log(e.getMessage());
                }
            }
        }, 1000);
    }
}
