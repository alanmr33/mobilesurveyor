package com.indocyber.dipostarsurvey.db;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "ACTIVITY_LOGS".
 */
@Entity
public class activity_logs {

    @Id
    @NotNull
    private String log_id;

    @NotNull
    private String activity_id;
    private java.util.Date log_date;
    private String status;
    private String user_id;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public activity_logs() {
    }

    public activity_logs(String log_id) {
        this.log_id = log_id;
    }

    @Generated
    public activity_logs(String log_id, String activity_id, java.util.Date log_date, String status, String user_id) {
        this.log_id = log_id;
        this.activity_id = activity_id;
        this.log_date = log_date;
        this.status = status;
        this.user_id = user_id;
    }

    @NotNull
    public String getLog_id() {
        return log_id;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setLog_id(@NotNull String log_id) {
        this.log_id = log_id;
    }

    @NotNull
    public String getActivity_id() {
        return activity_id;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setActivity_id(@NotNull String activity_id) {
        this.activity_id = activity_id;
    }

    public java.util.Date getLog_date() {
        return log_date;
    }

    public void setLog_date(java.util.Date log_date) {
        this.log_date = log_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
