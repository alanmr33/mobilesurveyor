package com.indocyber.dipostarsurvey.Library;

import android.app.*;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.indocyber.dipostarsurvey.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Indocyber on 14/12/2017.
 */

public class DatePicker extends DialogFragment {
    private  String date;
    private OnSetValue setValue;
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        final android.app.Dialog dialog=new Dialog(getActivity());
        dialog.setContentView(R.layout.datepicker);
        final android.widget.DatePicker datePicker= (android.widget.DatePicker) dialog.findViewById(R.id.datePicker);
        Button btnOK= (Button) dialog.findViewById(R.id.buttonDPOK);
        Button btnCancel= (Button) dialog.findViewById(R.id.buttonDPCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String   day  = (datePicker.getDayOfMonth()>=10)? String.valueOf(datePicker.getDayOfMonth()) : "0"+String.valueOf(datePicker.getDayOfMonth());
                String   month= (datePicker.getMonth()+1>=10)? String.valueOf(datePicker.getMonth()+1) : "0"+String.valueOf(datePicker.getMonth()+1);
                String   year = String.valueOf(datePicker.getYear());
                date=day+"/"+month+"/"+year;
                setValue.OnSet(date);
                dialog.dismiss();
            }
        });
        return dialog;
    }
    public void setOnSetvalue(OnSetValue setValue){
        this.setValue=setValue;
    }
    public String getDate(){
        return date;
    }
}
