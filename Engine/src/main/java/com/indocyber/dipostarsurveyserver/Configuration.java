package com.indocyber.dipostarsurveyserver;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import spark.utils.IOUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Indocyber on 11/09/2017.
 */
public class Configuration {
    public static final boolean databaseLog=false;
    public static final boolean debug=true;
    public final String baseUrl="/APIV10/";
    public final String username=getUsername();
    public final String password=getPassword();
    public final String imageLocation=getImageLocation();
    public final String apkLocation=apkLocation();
    public final String apiKey=getAPIKey();
    public final String mapKey=getMapKey();
    public final String url=getUrlDB();
    public final String className="com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public String getUrlDB(){
//            return "jdbc:sqlserver://" + getHostname() + ":" + getPort() + ";DatabaseName=" + getDatabase() +
//                    ";failoverPartner=" + getPartner();
            return "jdbc:sqlserver://" + getHostnameQA() + ":" + getPort() + ";DatabaseName=" + getDatabase();
    }
    public JSONObject getDBConfig(){
        InputStream Config=getClass().getResourceAsStream("/config.json");
        JSONParser parser=new JSONParser();
        Object obj= null;
        try {
            obj = parser.parse(IOUtils.toString(Config));
            JSONObject jsonObject = (JSONObject) obj;
            return  jsonObject;
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }
    public String getHostname(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("hostname");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getHostnameQA(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("qa");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getPartner(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("partner");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getDatabase(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("database");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getUsername(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("username");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getPassword(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("password");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public Long getPort(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (Long) object.get("port");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getImageLocation(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("image_location");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String apkLocation(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("apk_location");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getAPIKey(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("api_key");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getMapKey(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("gmap_api");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getLDAPServer(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("ldap_server");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public String getCRON(){
        JSONObject object=getDBConfig();
        if(object!=null){
            try {
                return (String) object.get("cron");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
