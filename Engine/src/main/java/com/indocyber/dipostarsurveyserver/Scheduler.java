package com.indocyber.dipostarsurveyserver;

import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by Indocyber on 13/03/2018.
 */
@WebListener
public class Scheduler extends QuartzInitializerListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        super.contextInitialized(sce);
        Configuration configuration=new Configuration();
        String key = "org.quartz.impl.StdSchedulerFactory.KEY";
        ServletContext servletContext = sce.getServletContext();
        StdSchedulerFactory factory = (StdSchedulerFactory) servletContext.getAttribute(key);
        try {
            org.quartz.Scheduler quartzScheduler = factory.getScheduler("MyQuartzScheduler");
            JobDetail job = newJob(JobResize.class)
                    .withIdentity("resize", "mss")
                    .build();
            Trigger trigger = newTrigger()
                    .withIdentity("resize-trigger", "mss")
                    .withSchedule(cronSchedule(configuration.getCRON()))
                    .forJob("resize", "mss")
                    .startNow()
                    .build();
            quartzScheduler.scheduleJob(job,trigger);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        // TODO use quartzScheduler here.
    }
}
