package com.indocyber.dipostarsurveyserver;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Indocyber on 23/10/2017.
 */
public class AES256 {
    public static final byte[] encBytes(byte[] srcBytes, byte[] key,
                                        byte[] newIv) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec iv = new IvParameterSpec(newIv);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(srcBytes);
        return encrypted;
    }

    public static final String encText(String sSrc, byte[] key, byte[] newIv)
            throws Exception {
        byte[] srcBytes = sSrc.getBytes("utf-8");
        byte[] encrypted = encBytes(srcBytes, key, newIv);
        return Base64.encode(encrypted);
    }

    public static final byte[] decBytes(byte[] srcBytes, byte[] key,
                                        byte[] newIv) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        IvParameterSpec iv = new IvParameterSpec(newIv);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(srcBytes);
        return encrypted;
    }

    public static final String decText(String sSrc, byte[] key, byte[] newIv)
            throws Exception {
        byte[] srcBytes = Base64.decode(sSrc);
        byte[] decrypted = decBytes(srcBytes, key, newIv);
        return new String(decrypted, "utf-8");
    }
    /*
    key 16 digit
     */
    public static String encryptText(String s,String key){
        byte[] ivk = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        try {
            return encText(s,key.getBytes("UTF-8"),ivk);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public static String decryptText(String s,String key){
        byte[] ivk = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        try {
            return decText(s,key.getBytes("UTF-8"),ivk);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public static String generateText(String text,int counter){
        counter=999999;
        Random random=new Random();
        counter=random.nextInt(counter);
        String counTXT;
        if(counter<=9){
            counTXT="00000"+String.valueOf(counter);
        }else if(counter>=10 && counter<=99){
            counTXT="0000"+String.valueOf(counter);
        }else if(counter>=100 && counter <=999){
            counTXT="000"+String.valueOf(counter);
        }else if(counter>=1000 && counter<=9999){
            counTXT="00"+String.valueOf(counter);
        }else if(counter>=10000 &&counter <=99999){
            counTXT="0"+String.valueOf(counter);
        }else{
            counTXT=String.valueOf(counter);
        }
        String key = "alanrahman"+counTXT;
        String string = encryptText(text,key);
        return string+"!"+key;
    }
    public static String realText(String text){
        if(text!=null) {
            String[] txt = text.split("!");
            if (Configuration.debug) {
                System.out.println("TEXT " + txt[0]);
                System.out.println("KEY " + txt[1]);
            }
            return decryptText(txt[0], txt[1]);
        }
        return null;
    }
}
