package com.indocyber.dipostarsurveyserver;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ldaptive.*;
import org.ldaptive.auth.*;
import org.ldaptive.auth.ext.ActiveDirectoryAuthenticationResponseHandler;
import org.symphonyoss.symphony.jcurl.JCurl;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.security.cert.CertificateParsingException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by Indocyber on 23/10/2017.
 */
public class Utils {
    public static String dateNow(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd");
        Date date=new Date();
        return dateFormat.format(date);
    }
    public static String date3Day(){
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd");
        Calendar cl=Calendar.getInstance();
        cl.add(Calendar.DAY_OF_MONTH,-3);
        Date date=cl.getTime();
        return dateFormat.format(date);
    }
    public static String dateFull()
    {
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Date date=new Date();
        return dateFormat.format(date);
    }
    public static String dateNowStart(){
        return Utils.dateNow()+" 00:00:00";
    }
    public static String date3DayStart(){
        return Utils.date3Day()+" 00:00:00";
    }
    public static String dateNowEnd(){
        return Utils.dateNow()+" 23:55:00";
    }
    public static String MD5(java.lang.String text){
        try {
            return DigestUtils.md5Hex(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String SHA1(java.lang.String text){
        try {
            return DigestUtils.sha1Hex(text.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
    public static String generatedToken(){
        Random random = new Random();
        int userLogin=random.nextInt(1)+1000000;
        return MD5(dateFull()+userLogin);
    }
    public static String getToAddress(String latlong){
        Configuration configuration=new Configuration();
        JCurl jcurl = JCurl.builder()
                .method(JCurl.HttpMethod.GET)               //HTTP GET is the default; this line can be skipped
                .build();

        String url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+latlong+"&key="+configuration.mapKey;
        try {
            HttpURLConnection connection=jcurl.connect(url);
            JCurl.Response response=jcurl.processResponse(connection);
            JSONObject result=new JSONObject(response.getOutput());
            JSONArray lists=result.getJSONArray("results");
            if(lists.length()>0) {
                return lists.getJSONObject(0).getString("formatted_address");
            }else{
                return "";
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateParsingException e) {
            e.printStackTrace();
        }
        return "";
    }
    public static boolean getLDAPConnection(String Host, String username, String password){
        ConnectionConfig connConfig = new ConnectionConfig(Host);
        connConfig.setUseStartTLS(false);
        connConfig.setConnectionInitializer(
                new BindConnectionInitializer("CN=Surveyor System1,OU=IT Dept,OU=User-JKTHO,OU=Dipostar-User,DC=corp,DC=dipostar,DC=com"
                        , new Credential("1Ndocyber")));
        SearchDnResolver dnResolver = new SearchDnResolver(new DefaultConnectionFactory(connConfig));
        dnResolver.setBaseDn("DC=corp,DC=dipostar,DC=com");
        dnResolver.setUserFilter("(|(uid={user})(displayName={user})(sn={user})(cn={user}))");
        dnResolver.setSubtreeSearch(true);
        BindAuthenticationHandler authHandler = new BindAuthenticationHandler(new DefaultConnectionFactory(connConfig));
        Authenticator auth = new Authenticator(dnResolver, authHandler);
        auth.setAuthenticationResponseHandlers(new ActiveDirectoryAuthenticationResponseHandler());
        auth.setReturnAttributes(ActiveDirectoryAuthenticationResponseHandler.ATTRIBUTES);
        AuthenticationResponse response = null;
        try {
            response = auth.authenticate(new AuthenticationRequest(username,
                    new Credential(password)));
            if (response.getResult()) {
                return true;
            } else {
                System.err.println(response.getMessage());
                return false;
            }
        } catch (LdapException e) {
            System.err.println("======LDAP ERROR=====");
            e.printStackTrace();
            return false;
        }
    }
    public static byte[] covertSignedToUnsigned(byte[] arr) {

        if(arr == null || arr.length == 0){
            return null;
        }
        byte[] converted=new byte[arr.length];
        for(int i=0;i<arr.length;i++)   {
            int x = arr[i];
            if(x>127){
                x-=256;
                converted[i]=(byte)x;
            }else{
                converted[i]=(byte)x;
            }
        }
        return converted;
    }
}
