package com.indocyber.dipostarsurveyserver;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ldaptive.*;
import org.ldaptive.auth.*;
import org.ldaptive.auth.ext.ActiveDirectoryAuthenticationResponseHandler;
import spark.ModelAndView;
import spark.servlet.SparkApplication;
import spark.template.velocity.VelocityTemplateEngine;
import java.io.*;
import java.sql.*;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import static spark.Spark.*;
/**
 * Created by Indocyber on 23/10/2017.
 */
public class Main implements SparkApplication {
    public static void main(String[] args) {
       new Main().init();
    }

    @Override
    public void init() {
        staticFiles.location("public");
        Configuration configuration=new Configuration();
        VelocityTemplateEngine templateEngine=new VelocityTemplateEngine();
        /*
        API MIDDLEWARE LEVEL 1
        KEY & IMEI VALIDATION
         */
        before("/login",(req,res)->{
            String apiKey=req.headers("API-KEY");
            if(!apiKey.equals(configuration.apiKey)){
                res.redirect("/APIV10/denied-access");
            }
        });
        before("/sync",(req,res)->{
            String apiKey=req.headers("API-KEY");
            if(!apiKey.equals(configuration.apiKey)){
                res.redirect("/APIV10/denied-access");
            }
        });
        get("/denied-access",(req,res)->{
            res.status(403);
            res.type("application/json");
            return Errors.error403();
        });
        //====TEST====
        get("/",(request, response)->{
//            Map map=new HashMap();
//            map.put("url",configuration.baseUrl);
//            map.put("error","");
//            return templateEngine.render(new ModelAndView(map,"login.vm"));
            response.status(200);
            response.type("application/json");
            return Errors.error403();
        });
        post("/adminlogin",(request, response)->{
            String username=request.queryParams("username");
            String password=request.queryParams("password");
            String error="";
            if(username!=null && password!=null){
                if(username.equals(configuration.username) && password.equals(configuration.password)){
                    request.session(true);
                    response.redirect("/APIV10/admin/");
                }else {
                    error="Invalid Username & Password";
                }
            }
            Map<String,String> map=new HashMap<>();
            map.put("url",configuration.baseUrl);
            map.put("error",error);
            return templateEngine.render(new ModelAndView(map,"login.vm"));
//            response.status(200);
//            response.type("application/json");
//            return Errors.error403();
        });
        path("/admin",()->{
            before("/*",(q,a)->{
                if(q.session().isNew()){
                    a.redirect("/APIV10");
                }
            });
            get("/",((request, response) -> {
                Map<String,String> map=new HashMap<>();
                map.put("url",configuration.baseUrl);
                return templateEngine.render(new ModelAndView(map,"main.vm"));
            }));
            get("/form_design",((request, response) -> {
                Dao dao1=new Dao();
                ArrayList<Map<String,Object>> formList=new ArrayList<>();
                JSONObject listForm=dao1.getDataByQuery("SELECT * FROM dsf_mobile_forms");
                for(int i=0;i<listForm.getJSONArray("data").length();i++){
                    Map<String,Object> item=new HashMap<>();
                    item.put("form_id",listForm.getJSONArray("data").getJSONObject(i).getString("form_id"));
                    item.put("form_name",listForm.getJSONArray("data").getJSONObject(i).getString("form_name"));
                    item.put("form_label",listForm.getJSONArray("data").getJSONObject(i).getString("form_label"));
                    item.put("form_order",listForm.getJSONArray("data").getJSONObject(i).getInt("form_order"));
                    formList.add(i,item);
                }
                ArrayList<String> extraList=new ArrayList<>();
                JSONObject listExtra=dao1.getDataByQuery("SELECT DISTINCT condition FROM dsf_master_parameter");
                for(int i=0;i<listExtra.getJSONArray("data").length();i++){
                    Map<String,Object> item=new HashMap<>();
                    extraList.add(i,listExtra.getJSONArray("data").getJSONObject(i).getString("condition"));
                }
                Map<String,Object> map=new HashMap<>();
                map.put("url",configuration.baseUrl);
                map.put("form",formList);
                map.put("param",extraList);
                dao1.close();
                return templateEngine.render(new ModelAndView(map,"form_row.vm"));
            }));
            get("/events",((request, response) -> {
                Dao dao1=new Dao();
                ArrayList<Map<String,Object>> formList=new ArrayList<>();
                JSONObject listForm=dao1.getDataByQuery("SELECT * FROM dsf_mobile_forms");
                for(int i=0;i<listForm.getJSONArray("data").length();i++){
                    Map<String,Object> item=new HashMap<>();
                    item.put("form_id",listForm.getJSONArray("data").getJSONObject(i).getString("form_id"));
                    item.put("form_name",listForm.getJSONArray("data").getJSONObject(i).getString("form_name"));
                    item.put("form_label",listForm.getJSONArray("data").getJSONObject(i).getString("form_label"));
                    item.put("form_order",listForm.getJSONArray("data").getJSONObject(i).getInt("form_order"));
                    formList.add(i,item);
                }
                Map<String,Object> map=new HashMap<>();
                map.put("url",configuration.baseUrl);
                map.put("form",formList);
                dao1.close();
                return templateEngine.render(new ModelAndView(map,"events.vm"));
            }));
            get("/getRows/:form_id",((request, response) -> {
                Dao dao1=new Dao();
                JSONObject result=new JSONObject();
                JSONObject listForm=dao1.getDataByQuery("SELECT * FROM dsf_mobile_form_rows WHERE form_id='"+request.params("form_id")+"' order by row_order asc ");
                JSONArray parsedTable=new JSONArray();
                for (int i=0;i<listForm.getJSONArray("data").length();i++){
                    JSONObject listFields=dao1.getDataByQuery("SELECT * FROM dsf_mobile_row_fields WHERE row_id='"+listForm.getJSONArray("data").getJSONObject(i).getString("row_id")+"'");
                    JSONArray data=new JSONArray();
                    data.put(0,listForm.getJSONArray("data").getJSONObject(i).getString("row_id"));
                    String items="<div class='row' style='max-width:  400px'>";
                    for (int a=0;a<listFields.getJSONArray("data").length();a++) {
                        String label=(listFields.getJSONArray("data").getJSONObject(a).has("field_label"))? listFields.getJSONArray("data").getJSONObject(a).getString("field_label").replaceAll("['\"\\\\]", "\\\\$0") : "";
                        items+="<div class='col-12'><a class='item' data-field='"+listFields.getJSONArray("data").getJSONObject(a)+"'>";
                        items+=UILib.addField(listFields.getJSONArray("data").getJSONObject(a).getString("field_weight"),
                                listFields.getJSONArray("data").getJSONObject(a).getString("field_name"),
                                label);
                        items+="</a></div>";
                    }
                    items+="</div>";
                    data.put(1, items);
                    data.put(2,"<span style='text-align: center'>"+listForm.getJSONArray("data").getJSONObject(i).getString("row_visibility")+"</span>");
                    String button="";
                    data.put(3,listForm.getJSONArray("data").getJSONObject(i).getString("row_order"));
                    button+="<button class='btn btn-primary up' data-row='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-arrow-up-b'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-primary down' data-row='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-arrow-down-b'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-warning vis' data-row='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-eye'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-success add' data-row='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-ios-plus-empty'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-danger delete' data-row='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-ios-close-outline'></span>" +
                            "</button> &nbsp;";
                    data.put(4,button);
                    parsedTable.put(i,data);
                }
                result.put("data",parsedTable);
                dao1.close();
               return result.toString();
            }));
            get("/getForms",((request, response) -> {
                Dao dao1=new Dao();
                JSONObject result=new JSONObject();
                JSONObject listForm=dao1.getDataByQuery("SELECT * FROM dsf_mobile_forms order by form_order asc");
                JSONArray parsedTable=new JSONArray();
                for (int i=0;i<listForm.getJSONArray("data").length();i++){
                    JSONArray data=new JSONArray();
                    data.put(0,listForm.getJSONArray("data").getJSONObject(i).getString("form_id"));
                    data.put(1,listForm.getJSONArray("data").getJSONObject(i).getString("form_name"));
                    data.put(2,listForm.getJSONArray("data").getJSONObject(i).getString("form_label"));
                    data.put(3,listForm.getJSONArray("data").getJSONObject(i).getInt("form_order"));
                    String button="";
                    button+="<button class='btn btn-primary up' data-form='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-arrow-up-b'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-primary down' data-form='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-arrow-down-b'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-success detail' data-form='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-ios-grid-view'></span>" +
                            "</button> &nbsp;";
                    data.put(4,button);
                    parsedTable.put(i,data);
                }
                result.put("data",parsedTable);
                dao1.close();
                return result.toString();
            }));
            get("/getEvents/:form_id",((request, response) -> {
                Dao dao1=new Dao();
                JSONObject result=new JSONObject();
                JSONObject listForm=dao1.getDataByQuery("SELECT * FROM dsf_mobile_ui_events " +
                        "where form_id='"+request.params("form_id")+"'");
                JSONArray parsedTable=new JSONArray();
                for (int i=0;i<listForm.getJSONArray("data").length();i++){
                    JSONArray data=new JSONArray();
                    data.put(0,listForm.getJSONArray("data").getJSONObject(i).getString("event_id"));
                    data.put(1,listForm.getJSONArray("data").getJSONObject(i).getString("event_type"));
                    data.put(2,listForm.getJSONArray("data").getJSONObject(i).getString("event_component"));
                    data.put(3,listForm.getJSONArray("data").getJSONObject(i).getString("event_component_target"));
                    data.put(4,listForm.getJSONArray("data").getJSONObject(i).getString("event_data"));
                    String button="";
                    button+="<button class='btn btn-primary edit' data-event='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-edit'></span>" +
                            "</button> &nbsp;";
                    button+="<button class='btn btn-danger delete' data-event='"+listForm.getJSONArray("data").getJSONObject(i)+"'>" +
                            "<span class='icon ion-close-circled'></span>" +
                            "</button> &nbsp;";
                    data.put(5,button);
                    parsedTable.put(i,data);
                }
                result.put("data",parsedTable);
                dao1.close();
                return result.toString();
            }));
            get("/getFields/:form_id",((request, response) -> {
                Dao dao1=new Dao();
                JSONObject result=new JSONObject();
                JSONObject listForm=dao1.getDataByQuery("SELECT\n" +
                        "dbo.dsf_mobile_row_fields.field_id,\n" +
                        "dbo.dsf_mobile_row_fields.field_name,\n" +
                        "dbo.dsf_mobile_row_fields.field_type,\n" +
                        "dbo.dsf_mobile_row_fields.field_visibility,\n" +
                        "dbo.dsf_mobile_row_fields.field_label,\n" +
                        "dbo.dsf_mobile_row_fields.field_global_value,\n" +
                        "dbo.dsf_mobile_row_fields.field_weight,\n" +
                        "dbo.dsf_mobile_row_fields.field_store,\n" +
                        "dbo.dsf_mobile_row_fields.field_extra,\n" +
                        "dbo.dsf_mobile_row_fields.row_id,\n" +
                        "dbo.dsf_mobile_form_rows.row_order,\n" +
                        "dbo.dsf_mobile_form_rows.row_visibility,\n" +
                        "dbo.dsf_mobile_form_rows.form_id\n" +
                        "\n" +
                        "FROM\n" +
                        "dbo.dsf_mobile_row_fields\n" +
                        "INNER JOIN dbo.dsf_mobile_form_rows ON dbo.dsf_mobile_row_fields.row_id = dbo.dsf_mobile_form_rows.row_id\n" +
                        "where dbo.dsf_mobile_form_rows.form_id='"+request.params("form_id")+"'");
                JSONArray parsedTable=new JSONArray();
                for (int i=0;i<listForm.getJSONArray("data").length();i++){
                   parsedTable.put(i,listForm.getJSONArray("data").getJSONObject(i).getString("field_name"));
                }
                result.put("data",parsedTable);
                dao1.close();
                return result.toString();
            }));
            post("/row/:type",((request, response) -> {
                Dao dao1=new Dao();
                int currentPos= Integer.valueOf(request.queryParams("row_order"));
                String rowID=request.queryParams("row_id");
                int nextPost;
                if(request.params("type").equals("up")){
                    nextPost=currentPos-1;
                }else{
                    nextPost=currentPos+1;
                }
                JSONObject result= dao1.crudQuery("update dsf_mobile_form_rows set row_order='"+nextPost+"' " +
                        "where row_id='"+rowID+"'");
                dao1.close();
                return result;
            }));
            post("/form/:type",((request, response) -> {
                Dao dao1=new Dao();
                int currentPos= Integer.valueOf(request.queryParams("form_order"));
                String form_id=request.queryParams("form_id");
                int nextPost;
                if(request.params("type").equals("up")){
                    nextPost=currentPos-1;
                }else{
                    nextPost=currentPos+1;
                }
                JSONObject result=dao1.crudQuery("update dsf_mobile_forms set form_order='"+nextPost+"' " +
                        "where form_id='"+form_id+"'");
                dao1.close();
                return result;
            }));
            post("/setVisibilityRow",((request, response) -> {
                Dao dao1=new Dao();
                int currentPos= Integer.valueOf(request.queryParams("row_order"));
                String rowID=request.queryParams("row_id");
                String visibility=request.queryParams("row_visibility");
                if(visibility.equals("visible")){
                    visibility="invisible";
                }else {
                    visibility="visible";
                }
                JSONObject result=dao1.crudQuery("update dsf_mobile_form_rows set row_visibility='"+visibility+"' " +
                        "where row_id='"+rowID+"'");
                dao1.close();
                return result;
            }));
            post("/deleteRow",((request, response) -> {
                Dao dao=new Dao();
                String rowID=request.queryParams("row_id");
                JSONObject result=dao.crudQuery("delete from dsf_mobile_form_rows where row_id='"+rowID+"'");
                dao.close();
                return result;
            }));
            post("/deleteFormField",((request, response) -> {
                Dao dao=new Dao();
                String fieldID=request.queryParams("field_id");
                JSONObject result= dao.crudQuery("delete from dsf_mobile_row_fields where field_id='"+fieldID+"'");
                dao.close();
                return result;
            }));
            post("/deleteEvent",((request, response) -> {
                Dao dao=new Dao();
                String eventID=request.queryParams("event_id");
                JSONObject result=dao.crudQuery("delete from dsf_mobile_ui_events where event_id='"+eventID+"'");
                dao.close();
                return result;
            }));
            post("/addRow",((request, response) -> {
                Dao dao=new Dao();
                String rowID=request.queryParams("row_id");
                String formID=request.queryParams("form_id");
                JSONObject result=dao.crudQuery("insert into dsf_mobile_form_rows VALUES('"+rowID+"'," +
                        "1+(Select max(row_order) from dsf_mobile_form_rows where form_id='"+formID+"'),'visible','"+formID+"')");
                dao.close();
                return result;
            }));
            post("/saveForm",((request, response) -> {
                Dao dao=new Dao();
                JSONObject result=new JSONObject();
                String formID=request.queryParams("form_id");
                String formName=request.queryParams("form_name");
                String formLabel=request.queryParams("form_label");
                String formOrder=request.queryParams("form_order");
                JSONObject checkForm=dao.getDataByQuery("SELECT * FROM dsf_mobile_forms WHERE form_id='"+formID+"'");
                if(checkForm.getInt("count")>0){
                    result= dao.crudQuery("UPDATE dsf_mobile_forms SET " +
                            "form_id='"+formID+"',form_name='"+formName+"',form_label='"+formLabel+"', form_order='"+formOrder+"'" +
                            "WHERE form_id='"+formID+"'");
                }else{
                    result= dao.crudQuery("INSERT INTO dsf_mobile_forms VALUES('"+formID+"'," +
                            "'"+formName+"','"+formLabel+"','"+formOrder+"')");
                }
                dao.close();
                return result.toString();
            }));
            post("/saveEvent/:form_id",((request, response) -> {
                Dao dao=new Dao();
                JSONObject result=new JSONObject();
                String formID=request.params("form_id");
                String eventID=request.queryParams("event_id");
                String eventType=request.queryParams("event_type");
                String eventComponent=request.queryParams("event_component");
                String eventComponentTarget=request.queryParams("event_component_target");
                String eventData=request.queryParams("event_data");
                JSONObject checkEvent=dao.getDataByQuery("SELECT * FROM dsf_mobile_ui_events WHERE event_id='"+eventID+"'");
                if(checkEvent.getInt("count")>0){
                    result= dao.crudQuery("UPDATE dsf_mobile_ui_events SET " +
                            "event_id='"+eventID+"'," +
                            "event_type='"+eventType+"'," +
                            "event_component='"+eventComponent+"'," +
                            "event_component_target='"+eventComponentTarget+"'," +
                            "event_data='"+eventData+"'," +
                            "form_id='"+formID+"'" +
                            "WHERE event_id='"+eventID+"'");
                }else{
                    result= dao.crudQuery("INSERT INTO dsf_mobile_ui_events " +
                            "VALUES" +
                            "('"+eventID+"','"+eventType+"','"+eventComponent+"','"+eventComponentTarget+"','"+eventData+"','"+formID+"')");
                }
                dao.close();
                return result.toString();
            }));
            post("/saveFormField",((request, response) -> {
                Dao dao=new Dao();
                JSONObject result=new JSONObject();
                String fieldID=request.queryParams("field_id");
                String fieldName=request.queryParams("field_name");
                String fieldType=request.queryParams("field_type");
                String fieldLabel=request.queryParams("field_label");
                String fieldExtra=request.queryParams("field_extra");
                String fieldVisibility=request.queryParams("field_visibility");
                String fieldGlobalValue=request.queryParams("field_global_value");
                String fieldStore=request.queryParams("field_store");
                String fieldWeight=request.queryParams("field_weight");
                String rowID=request.queryParams("row_id");
                JSONObject checkForm=dao.getDataByQuery("SELECT * FROM dsf_mobile_row_fields " +
                        "WHERE field_id='"+fieldID+"'");
                if(checkForm.getInt("count")>0){
                    result= dao.crudQuery("UPDATE dsf_mobile_row_fields SET " +
                            "field_id='"+fieldID+"'," +
                            "field_name='"+fieldName+"'," +
                            "field_type='"+fieldType+"'," +
                            "field_label='"+fieldLabel+"'," +
                            "field_extra='"+fieldExtra+"'," +
                            "field_visibility='"+fieldVisibility+"'," +
                            "field_weight='"+fieldWeight+"'," +
                            "field_global_value='"+fieldGlobalValue+"'," +
                            "field_store='"+fieldStore+"'" +
                            "WHERE field_id='"+fieldID+"'");
                }else{
                    result= dao.crudQuery("INSERT INTO dsf_mobile_row_fields (" +
                            "field_id," +
                            "field_name," +
                            "field_type," +
                            "field_label," +
                            "field_extra," +
                            "field_visibility," +
                            "field_weight," +
                            "field_global_value," +
                            "field_store," +
                            "row_id)" +
                            "VALUES" +
                            "(" +
                            "'"+fieldID+"'," +
                            "'"+fieldName+"'," +
                            "'"+fieldType+"'," +
                            "'"+fieldLabel+"'," +
                            "'"+fieldExtra+"'," +
                            "'"+fieldVisibility+"'," +
                            "'"+fieldWeight+"'," +
                            "'"+fieldGlobalValue+"'," +
                            "'"+fieldStore+"'," +
                            "'"+rowID+"')");
                }
                dao.close();
                return result.toString();
            }));
            post("/deleteForm",((request, response) -> {
                Dao dao=new Dao();
                JSONObject result=new JSONObject();
                String formID=request.queryParams("form_id");
                dao.crudQuery("delete from dsf_mobile_forms where form_id='"+formID+"'");
                result.put("status",true);
                result.put("message","form deleted");
                dao.close();
                return result.toString();
            }));
        });
        get("/logout",((request, response) -> {
            request.session().invalidate();
            response.redirect("/APIV10");
            return null;
        }));
        post("/geo",(request, response)->{
            return Utils.getToAddress(request.queryParams("latlong"));
        });
        //END TEST
        get("/sync/get_object/:object_name/:page",((req,res)->{
            Dao dao=new Dao();
            int rpp=5000;
            HashMap<String,String> objList=new HashMap<>();
            objList.put("dsf_mobile_forms","form_id");
            objList.put("dsf_mobile_form_rows","row_id");
            objList.put("dsf_master_parameter","param_ID");
            objList.put("dsf_mobile_row_fields","field_id");
            objList.put("dsf_mobile_ui_events","event_id");
            objList.put("dsf_mst_postcode","PostCode");
            String tableName=req.params("object_name");
            int page=Integer.parseInt(req.params("page"));
            int start=(page<=1)? 0 : (page-1)*rpp;
            int end=start+rpp;
            JSONObject result=dao.getDataByQuery("SELECT * FROM ( SELECT *, ROW_NUMBER() OVER " +
                    "(ORDER BY "+objList.get(tableName)+") as row FROM "+tableName+")a " +
                    " WHERE row>="+start+" AND row < "+end);
            dao.close();
            result.put("finished",false);
            if(tableName.equals("dsf_mst_postcode") && result.getInt("count")<1){
                result.put("finished",true);
            }
            return AES256.generateText(result.toString(),0);
        }));
        get("/sync",(req,res)-> {
            String version=req.queryParams("version");
            Dao dao=new Dao();
            JSONObject lastVersion=dao.getDataByQuery("" +
                    "SELECT level_param FROM dsf_master_parameter WHERE " +
                    "condition='MobileAppVersion' OR condition='PackageVersion' ");
            String AppVersion=lastVersion.getJSONArray("data").getJSONObject(0)
                    .getString("level_param");
            String PackVersion=lastVersion.getJSONArray("data").getJSONObject(1)
                    .getString("level_param");
            JSONArray objects = new JSONArray();
            JSONObject listObject = new JSONObject();
            if(!version.equals(AppVersion)) {
                objects.put(0, "dsf_mobile_forms");
                objects.put(1, "dsf_mobile_form_rows");
                objects.put(2, "dsf_master_parameter");
                objects.put(3, "dsf_mobile_row_fields");
                objects.put(4, "dsf_mobile_ui_events");
                objects.put(5, "dsf_mst_postcode");
                listObject.put("data", objects);
                listObject.put("version", AppVersion);
                listObject.put("pack_version", PackVersion);
            }else{
                listObject.put("data", objects);
                listObject.put("version", AppVersion);
                listObject.put("pack_version", PackVersion);
            }
            return AES256.generateText(listObject.toString(),0);
        });
        get("/downloadAPK",(req,res)->{
            res.header("Content-Type","application/vnd.android.package-archive");
            String version=req.queryParams("version");

            File file = new File(configuration.apkLocation+"MSS-"+version+".apk");
            if(file.exists()) {
                byte[] files = new byte[(int) file.length()];
                InputStream is = new FileInputStream(file);
                is.read(files);
                return files;
            }
            return null;
        });
        post("/login",(req,res)->{
            boolean ldap=false;
            Dao dao=new Dao();
            res.type("application/json");
            JSONObject response=new JSONObject();
            JSONObject data=new JSONObject(AES256.realText(req.queryParams("data")));
            try {
                ldap= Utils.getLDAPConnection(configuration.getLDAPServer(),
                        data.getString("username"),data.getString("password"));
            }catch (Exception e){
                ldap=false;
            }
            if(!ldap){
                String username=data.getString("username").replace("'","").toUpperCase();
                String password=Utils.SHA1(username+data.getString("password"));
                String device_id=data.getString("device_id");
                /*
                login using username and password
                 */
                JSONObject loginResult=dao.getDataByQuery("select * from dsf_mst_user2 where " +
                        " (userID = '"+username+"') and (password='"+password+"' and isActive='1') ");
                if(loginResult.getBoolean("status")){
                    if(loginResult.getInt("count")>0){
                        if(loginResult.getJSONArray("data")
                                .getJSONObject(0).has("imei")){
                            if(loginResult.getJSONArray("data")
                                    .getJSONObject(0).getString("imei").equals(device_id)) {
                                if(loginResult.getJSONArray("data")
                                        .getJSONObject(0).getInt("isAccessMobile")==1) {
                                    /*
                                    generate token
                                     */
                                    String token = Utils.generatedToken();
                                    /*
                                    insert token to db
                                     */
                                    dao.crudQuery("insert into dsf_mobile_tokens values('" + loginResult.getJSONArray("data")
                                            .getJSONObject(0).getString("userID") + "'," +
                                            "'" + Utils.dateFull() + "','" + token + "')");

                                    response.put("status", true);
                                    response.put("message", "Login Success");
                                    response.put("token", token);
                                    response.put("data", loginResult.getJSONArray("data"));
                                }else{
                                    response.put("status",false);
                                    response.put("message","Akes Mobile Diblokir");
                                }
                            }else{
                                response.put("status",false);
                                response.put("message","Mobile Device Tidak Sesuai");
                            }
                        }else{
                            response.put("status",false);
                            response.put("message","Mobile Device Tidak Terdaftar");
                        }
                    }else{
                        response.put("status",false);
                        response.put("message","User ID dan Password Tidak Sesuai");
                    }
                }else{
                    response.put("status",false);
                    response.put("message","Internal Server Error");
                }
                dao.close();
            }else{
                String username=data.getString("username").toUpperCase();
                String device_id=data.getString("device_id");
                /*
                login using username and password
                 */
                JSONObject loginResult=dao.getDataByQuery("select * from dsf_mst_user2 where " +
                        "ldap = '"+username+"' and isActive='1'");
                if(loginResult.getBoolean("status")){
                    if(loginResult.getInt("count")>0){
                        if(loginResult.getJSONArray("data")
                                .getJSONObject(0).has("imei")){
                            if(loginResult.getJSONArray("data")
                                    .getJSONObject(0).getString("imei").equals(device_id)) {
                                if(loginResult.getJSONArray("data")
                                        .getJSONObject(0).getInt("isAccessMobile")==1) {
                                /*
                                generate token
                                 */
                                    String token = Utils.generatedToken();
                                /*
                                insert token to db
                                 */
                                    dao.crudQuery("insert into dsf_mobile_tokens values('" + loginResult.getJSONArray("data")
                                            .getJSONObject(0).getString("userID") + "'," +
                                            "'" + Utils.dateFull() + "','" + token + "')");

                                    response.put("status", true);
                                    response.put("message", "Login Success");
                                    response.put("token", token);
                                    response.put("data", loginResult.getJSONArray("data"));
                                }else{
                                    response.put("status",false);
                                    response.put("message","Akes Mobile Diblokir");
                                }
                            }else{
                                response.put("status",false);
                                response.put("message","Mobile Device Tidak Sesuai");
                            }
                        }else{
                            response.put("status",false);
                            response.put("message","Mobile Device Tidak Terdaftar");
                        }
                    }else{
                        response.put("status",false);
                        response.put("message","User ID dan Password Tidak Sesuai");
                    }
                }else{
                    response.put("status",false);
                    response.put("message","Internal Server Error");
                }
                dao.close();
            }
            return AES256.generateText(response.toString(),0);
        });

        path("/user",()->{
            /*
            API MIDDLEWARE LEVEL 2
            TOKEN VALIDATION
             */
            before("/*",(req,res)->{
                String apiKey=req.headers("API-KEY");
                if(!apiKey.equals(configuration.apiKey)){
                    res.redirect("/APIV10/denied-access");
                }else{
                    Dao dao=new Dao();
                    String token=req.queryParams("token");
                    JSONObject validate=dao.getDataByQuery("select userID from dbo.dsf_mobile_tokens where " +
                            "token='"+token+"'");
                    dao.close();
                    if(validate.getBoolean("status")){
                        if(validate.getInt("count")<1){
                            res.body("{status : false , message : 'denied'}");
                        }
                    }else{
                        res.body("{status : false , message : 'denied'}");
                    }
                }
            });
            /*
            get task query
             */
            get("/task",(req,res)->{
                Dao dao=new Dao();
                String token=req.queryParams("token");
                String date_start=Utils.date3DayStart();
                String date_end=Utils.dateNowEnd();
                JSONObject currentUser=dao.getDataByQuery("select userID from dbo.dsf_mobile_tokens where " +
                        "token='"+token+"'");
                String UserID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("userID");
                JSONObject tasks=dao.getDataByQuery("SELECT " +
                        "dbo.dsf_mobile_orders.orderID," +
                        "dbo.dsf_mobile_orders.fppNo," +
                        "dbo.dsf_mobile_orders.namaPemesan," +
                        "dbo.dsf_mobile_orders.alamatPemesan," +
                        "dbo.dsf_mobile_orders.noHP," +
                        "dbo.dsf_mobile_orders.status," +
                        "dbo.dsf_mobile_orders.createdDate," +
                        "dbo.dsf_mobile_orders.updatedDate," +
                        "CAST(\n" +
                        "\tCASE WHEN dbo.dsf_mobile_orders.newPlanDate IS NULL \n" +
                        "\t\tTHEN dbo.dsf_mobile_orders.planDate\n" +
                        "\t\tELSE dbo.dsf_mobile_orders.newPlanDate\n" +
                        "\tEND\n" +
                        "as DATETIME) as planDate," +
                        "dbo.dsf_mobile_orders.surveyorNotes as notes," +
                        "dbo.dsf_mobile_orders.spkFilename," +
                        "dbo.dsf_mobile_orders.branchID," +
                        "dbo.getLastActivityData(dbo.dsf_mobile_orders.fppNo) as lastData, " +
                        "dbo.dsf_mobile_orders.surveyorID as userID," +
                        "dbo.dsf_mobile_orders.oldSurveyorID " +
                        "FROM dbo.dsf_mobile_orders WHERE " +
                        "(" +
                        "dbo.dsf_mobile_orders.surveyorID ='"+UserID+"'  OR " +
                        "dbo.dsf_mobile_orders.oldSurveyorID ='"+UserID+"'" +
                        ")" +
                        "AND " +
                        "(" +
                        "(" +
                        "(" +
                        "(dbo.dsf_mobile_orders.createdDate BETWEEN '"+date_start+"' AND '"+date_end+"')" +
                        "OR" +
                        "(dbo.dsf_mobile_orders.updatedDate BETWEEN '"+date_start+"' AND '"+date_end+"')" +
                        ")" +
                        " AND " +
                        "(" +
                        "dbo.dsf_mobile_orders.status='94'" +
                        "OR " +
                        "dbo.dsf_mobile_orders.status='96'" +
                        ")" +
                        ")" +
                        " OR " +
                        "dbo.dsf_mobile_orders.status='87' " +
                        " OR " +
                        "dbo.dsf_mobile_orders.status='90' " +
                        " OR " +
                        "dbo.dsf_mobile_orders.status='88' " +
                        " OR " +
                        "dbo.dsf_mobile_orders.status='91' " +
                        ")");
                if(tasks.getBoolean("status")){
                        /*
                        update downloaded order flag
                         */
                    JSONArray taskList=tasks.getJSONArray("data");
                    for (int i=0;i<taskList.length();i++){
                            /*
                            update downloaded flag
                             */
                        if(taskList.getJSONObject(i)
                                .getString("status").equals("87")) {
                            dao.crudQuery("update dbo.dsf_mobile_orders set status='88' ," +
                                    " updatedDate='" + Utils.dateFull() + "' , deliveredDate='" + Utils.dateFull() + "'" +
                                    "where orderID='" + taskList.getJSONObject(i)
                                    .getString("orderID") + "'");
                        }
                    }
                    dao.close();
                    return AES256.generateText(tasks.getJSONArray("data").toString(),0);
                }else{
                    dao.close();
                    return AES256.generateText(new JSONArray().toString(),0);
                }
            });
            /*
            get download spk
             */
            post("/downloadSPK",(req,res)->{
                JSONObject data=new JSONObject(AES256.realText(req.queryParams("data")));
                String orderID=data.getString("orderID");
                Dao dao=new Dao();
                Connection connection=dao.getConnection();
                String query="SELECT spkFile ,spkFilename FROM dsf_mobile_orders WHERE orderID='"+orderID+"'";
                Statement statement=connection.createStatement();
                ResultSet rs = statement.executeQuery(query);
                if (rs.next()) {
                    byte[] files=rs.getBytes(1);
                    String filename=rs.getString(2);
                    if(filename.contains(".pdf")){
                        res.header("Content-Type","application/pdf");
                    }else{
                        res.header("Content-Type","image/jpeg");
                    }
                    dao.close();
                    return files;
                }
                dao.close();
                return null;
            });
            /*
            get last data
             */
            get("/downloadLastPhoto",(req,res)->{
                res.header("Content-Type","image/jpeg");
                JSONObject dataRequest=new JSONObject(AES256.realText(req.queryParams("data")));
                String orderID=dataRequest.getString("orderID");
                String photoType=dataRequest.getString("photoType");
                Dao dao=new Dao();
                JSONObject data=dao.getDataByQuery("SELECT "+photoType+" " +
                        " FROM dsf_mst_upload_documentation " +
                        " WHERE SurveyOrderID='"+orderID+"' ");
                if(data.getInt("count")>0){
                    File file = new File(configuration.imageLocation+data.getJSONArray("data")
                    .getJSONObject(0).getString(photoType));
                    byte[] files=new byte[(int) file.length()];
                    InputStream is=new FileInputStream(file);
                    is.read(files);
                    return files;
                }
                dao.close();
                return null;
            });
            post("/task",(req,res)->{
                Dao dao=new Dao();
                JSONObject result;
                ArrayList<String> queries=new ArrayList<>();
                String token=req.queryParams("token");
                JSONObject data=new JSONObject(AES256.realText(req.queryParams("data")));
                String orderID=data.getString("order_id");
                String activityID=data.getString("activity_id");
                String createdDate=data.getString("created_date");
                JSONObject currentUser=dao.getDataByQuery("select userID from dbo.dsf_mobile_tokens where " +
                        "token='"+token+"'");
                String UserID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("userID");
                JSONObject body=new JSONObject(data.getString("body"));
                /*
                variable LIST
                 */
                String isCompany=(body.has("isCompany"))? body.getString("isCompany").replaceAll("['\"\\\\]", "\\\\$0") : "";
                String cancelStatus=(body.has("cancelStatus"))? body.getString("cancelStatus").replaceAll("['\"\\\\]", "\\\\$0") : "";
                String surveyStatus=(body.has("SurveyStatus"))? body.getString("SurveyStatus").replaceAll("['\"\\\\]", "\\\\$0") : "";
                String submitLocation=(body.has("SubmitLocation"))? body.getString("SubmitLocation").replaceAll("['\"\\\\]", "\\\\$0") : "";
                String submitDate=(body.has("SubmitDate"))? body.getString("SubmitDate").replaceAll("['\"\\\\]", "\\\\$0") : "";

                if(!cancelStatus.equals("")){
                    if(cancelStatus.equals("PICKUP")){
                        //=====PICKUP DOCUMENT=====
                        queries=pickupDocument(dao,body);
                    }else{
                        //=====TUNDA KUNJUNGAN=====
                        queries=tundaKunjungan(dao,body);
                    }
                }else {
                    if(isCompany.equals("true") && surveyStatus.equals("65") && cancelStatus.equals("")){
                        //=====SURVEY PERUSAHAAN=====
                        queries=companySurvey(dao,body);
                    }else if(isCompany.equals("false") && surveyStatus.equals("65") && cancelStatus.equals("")){
                        //=====SURVEY INDIVIDU=====
                        queries=individualSurvey(dao,body);
                    }else if(surveyStatus.equals("66")){
                        //=====SURVEY CANCEL=====
                        queries=batalSurvey(dao,body);
                    }else if(surveyStatus.equals("75")){
                        //=====TTD KONTRAK=====
                        queries=ttdKontrak(dao,body);
                    }
                }
                JSONObject actCheck=dao.getDataByQuery("SELECT activity_id " +
                        "FROM dsf_mobile_activities where activity_id='"+activityID+"'");
                if(actCheck.getInt("count")<1){
                    queries.add("INSERT INTO dsf_mobile_activities(" +
                            "activity_id," +
                            "order_id," +
                            "body," +
                            "created_date," +
                            "userID" +
                            ")VALUES(" +
                            "'"+activityID+"'," +
                            "'"+orderID+"'," +
                            "'"+body+"'," +
                            "'"+createdDate+"'," +
                            "'"+UserID+"'" +
                            ")");
                }
                if(surveyStatus.equals("65")) {
                    if(body.has("Photo1Loc")) {
                        queries.add(queries.size(), "insert into dbo.dsf_mobile_trackings VALUES(" +
                                "'" + UserID + "'," +
                                "'" + submitDate + "'," +
                                "'" + body.getString("Photo1Loc") + "'," +
                                "'" + Utils.getToAddress(body.getString("Photo1Loc")) + "'," +
                                "'" + orderID + "')");
                    }else{
                        queries.add(queries.size(), "insert into dbo.dsf_mobile_trackings VALUES(" +
                                "'" + UserID + "'," +
                                "'" + submitDate + "'," +
                                "'" + submitLocation + "'," +
                                "'" + Utils.getToAddress(submitLocation) + "'," +
                                "'" + orderID + "')");
                    }
                }else{
                    queries.add(queries.size(), "insert into dbo.dsf_mobile_trackings VALUES(" +
                            "'" + UserID + "'," +
                            "'" + submitDate + "'," +
                            "'" + submitLocation + "'," +
                            "'" + Utils.getToAddress(submitLocation) + "'," +
                            "'" + orderID + "')");
                }
                queries.add(queries.size(),"update dsf_mobile_orders SET submitDate='"+submitDate+"',actualSubmitDate=GETDATE() \n" +
                        "where orderID='"+orderID+"'");
                result=dao.crudQueries(queries);
                dao.close();
                return AES256.generateText(result.toString(),0);
            });
            post("/task/upload",(req,res)->{
                Dao dao=new Dao();
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
                JSONObject result=new JSONObject();
                String token=req.queryParams("token");
                String photoCount=req.queryParams("photo_name");
                String orderID=req.queryParams("order_id");
                String image=req.queryParams("image");
                Calendar calendar = Calendar.getInstance();
                String month=(calendar.get(Calendar.MONTH)+1>9)? String.valueOf(calendar.get(Calendar.MONTH)+1) : '0'+String.valueOf(calendar.get(Calendar.MONTH)+1);
                String year=String.valueOf(calendar.get(Calendar.YEAR));
                JSONObject currentUser=dao.getDataByQuery("select userID from dbo.dsf_mobile_tokens where " +
                        "token='"+token+"'");
                String UserID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("userID");
                String name=year+"/"+month+"/"+orderID.replace("/","_")+"/"+photoCount+"_"+dateFormat.format(new Date())+".jpg";
                byte[] data = Base64.decode(image);
                try {
                    File imageFile=new File(configuration.imageLocation+UserID+"/"+name);
                    if(!imageFile.exists()){
                        imageFile.getParentFile().mkdirs();
                    }
                    OutputStream stream = new FileOutputStream(imageFile.getPath());
                    stream.write(data);
                    stream.close();
                    result=dao.crudQuery("UPDATE dsf_mst_upload_documentation SET " +
                            photoCount+"='"+UserID+"/"+name+"'" +
                            " WHERE SurveyOrderID='"+orderID+"'");
                    dao.crudQuery("UPDATE dsf_mobile_orders SET status='89' " +
                            " WHERE orderID='"+orderID+"'");
                }catch (Exception e){
                    if(Configuration.debug){
                        System.out.println(e.getMessage());
                    }
                    result.put("status",false);
                    result.put("message","failed");
                }
                dao.close();
                return AES256.generateText(result.toString(),0);
            });
                /*
                insert tracking
                 */
            post("/tracking",(req,res)->{
                Dao dao=new Dao();
                String token=req.queryParams("token");
                JSONArray location=new JSONArray(AES256.realText(req.queryParams("locs")));

                JSONObject currentUser=dao.getDataByQuery("select userID from dbo.dsf_mobile_tokens where " +
                        "token='"+token+"'");
                String UserID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("userID");

                for (int i=0;i<location.length();i++){
                    dao.crudQuery("insert into dbo.dsf_mobile_trackings VALUES(" +
                            "'"+UserID+"'," +
                            "'"+location.getJSONObject(0).getString("date")+"'," +
                            "'"+location.getJSONObject(0).getString("location")+"'," +
                            "'"+Utils.getToAddress(location.getJSONObject(0).getString("location"))+"'," +
                            "null)");
                }
                JSONObject result=new JSONObject();
                result.put("status",true);
                result.put("message","Success");
                dao.close();
                return AES256.generateText(result.toString(),0);
            });
            ///settings
            post("/settings",(req,res)->{
                Dao dao=new Dao();
                String token=req.queryParams("token");
                JSONObject data=new JSONObject(AES256.realText(req.queryParams("data")));
                JSONObject currentUser=dao.getDataByQuery("select userID from dbo.dsf_mobile_tokens where " +
                        "token='"+token+"'");
                String UserID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("userID");
                String opassword=Utils.SHA1(UserID+data.getString("opassword"));
                String npassword=Utils.SHA1(UserID+data.getString("npassword"));
                JSONObject check=dao.getDataByQuery("select userID from dbo.dsf_mst_user2 where " +
                        "userID='"+UserID+"' and password='"+opassword+"'");
                JSONObject result=new JSONObject();
                if(check.getInt("count")>0){
                    result=dao.crudQuery("update dbo.dsf_mst_user2 " +
                            "SET password='"+npassword+"'," +
                            "isFirstLogin='0' " +
                            "where " +
                            "userID='"+UserID+"' and password='"+opassword+"'");
                }else{
                    result.put("status",false);
                    result.put("message","Passsword Lama Tidak Cocok");
                }
                dao.close();
                return AES256.generateText(result.toString(),0);
            });
            /*
            insert user logs
             */
            post("/log",(req,res)->{
                Dao dao=new Dao();
                String token=req.queryParams("token");
                JSONArray histories=new JSONArray(AES256.realText(req.queryParams("histories")));

                JSONObject currentUser=dao.getDataByQuery("SELECT\n" +
                        "dbo.dsf_detail_access.userID,\n" +
                        "dbo.dsf_detail_access.branchID,\n" +
                        "dbo.dsf_mobile_tokens.token\n" +
                        "FROM\n" +
                        "dbo.dsf_mobile_tokens\n" +
                        "INNER JOIN dbo.dsf_detail_access ON dbo.dsf_mobile_tokens.userID = dbo.dsf_detail_access.userID\n" +
                        "WHERE\n" +
                        "dbo.dsf_detail_access.branchID IS NOT NULL\n" +
                        "AND \n" +
                        "dbo.dsf_mobile_tokens.token='"+token+"'");
                String UserID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("userID");
                String branchID=currentUser.getJSONArray("data").getJSONObject(0)
                        .getString("branchID");
                for (int i=0;i<histories.length();i++){
                    String logout=(histories.getJSONObject(i).has("logout"))? histories.getJSONObject(i).getString("logout") : "00:00:00";
                    dao.crudQuery("insert into dbo.dsf_mst_login" +
                            "(userID,date,login,logout,branchID) VALUES(" +
                            "'"+UserID+"'," +
                            "'"+histories.getJSONObject(i).getString("date")+"'," +
                            "'"+histories.getJSONObject(i).getString("login")+"'," +
                            "'"+logout+"'," +
                            "'"+branchID+"'" +
                            ")");
                }
                JSONObject result=new JSONObject();
                result.put("status",true);
                result.put("message","Success");
                dao.close();
                return AES256.generateText(result.toString(),0);
            });
                /*
                gzip minimalize output API
                 */
            after((request, response) -> {
                response.header("Content-Encoding", "gzip");
            });
        });

        /*
        API ERROR
         */
        internalServerError(Errors::error500);
        notFound(Errors::error404);
        exception(Exception.class, (exception, request, response) -> {
            System.out.println("ERROR "+request.url());
            if(Configuration.debug){
                exception.printStackTrace();
            }
            response.status(500);
            response.body("INTERNAL SERVER ERROR");
        });
        System.out.println("API STARTED with DB : "+configuration.getUrlDB());
    }
    public ArrayList<String> companySurvey(Dao dao,JSONObject data){
        ArrayList<String> queries=new ArrayList<>();
        String SurveyResult=(data.has("SurveyResult"))? data.getString("SurveyResult").replaceAll("['\"\\\\]", "\\\\$0").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Recomendation=(data.has("Recomendation"))? data.getString("Recomendation").replaceAll("['\"\\\\]", "\\\\$0").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String orderID=(data.has("survey-orderId"))? data.getString("survey-orderId").replaceAll("['\"\\\\]", "\\\\$0").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String fppNo=(data.has("fpp_no"))? data.getString("fpp_no").replaceAll("['\"\\\\]", "\\\\$0").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject dataOrder=dao.getDataByQuery("SELECT branchID,surveyorID FROM dsf_mobile_orders WHERE orderID='"+orderID+"'");
        String branchID=dataOrder.getJSONArray("data").getJSONObject(0).getString("branchID").replaceAll("['\"\\\\]", "\\\\$0");
        String surveyorID=dataOrder.getJSONArray("data").getJSONObject(0).getString("surveyorID").replaceAll("['\"\\\\]", "\\\\$0");
        int i=0;
        String CustomerType="PERUSAHAAN";
        String TitleofCustomer=(data.has("CompanyType-readable"))? data.getString("CompanyType-readable").replaceAll("['\"\\\\]", "\\\\$0").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String CustomerName=(data.has("CompanyName"))? data.getString("CompanyName").replaceAll("['\"\\\\]", "\\\\$0").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessSector=(data.has("CompanySector"))? data.getString("CompanySector").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String PICCompanyName=(data.has("PICName"))? data.getString("PICName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String PICJobTitle=(data.has("PICRole"))? data.getString("PICRole").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone1=(data.has("OfficePhone1"))? data.getString("OfficePhone1").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone2=(data.has("OfficePhone2"))? data.getString("OfficePhone2").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone3=(data.has("PICHandphone1"))? data.getString("PICHandphone1").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone4=(data.has("PICHandphone2"))? data.getString("PICHandphone2").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String MainIncome=(data.has("MainSalary"))? data.getString("MainSalary").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OtherIncome=(data.has("OtherSalary"))? data.getString("OtherSalary").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DailyCost=(data.has("DailySpend"))? data.getString("DailySpend").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OtherCost=(data.has("OtherSpend"))? data.getString("OtherSpend").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NettIncome=(data.has("NettSalary"))? data.getString("NettSalary").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessSpecification=(data.has("BusinessSpec"))? data.getString("BusinessSpec").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TypeofFinancingPayment=(data.has("TypeOfFinancingPayment-readable"))? data.getString("TypeOfFinancingPayment-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String InsuranceCompany=(data.has("InsuranceCompany-readable"))? data.getString("InsuranceCompany-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TypeOfInsurance=(data.has("TypeOfInsurance-readable"))? data.getString("TypeOfInsurance-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String PurchaseStatus=(data.has("PurchaseStatus-readable"))? data.getString("PurchaseStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String AssetsOwned=(data.has("OwnedAsset"))? data.getString("OwnedAsset").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OtherFinancingExperience=(data.has("OtherFinancingExperience"))? data.getString("OtherFinancingExperience").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordC=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_customer " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordC.getInt("count")>0){
            queries.add(i,"UPDATE  dsf_mst_customer SET " +
                    "CustomerType='"+CustomerType+"'," +
                    "TitleofCustomer='"+TitleofCustomer+"'," +
                    "CustomerName='"+CustomerName+"'," +
                    "BusinessSector='"+BusinessSector+"'," +
                    "PICCompanyName='"+PICCompanyName+"'," +
                    "PICJobTitle='"+PICJobTitle+"'," +
                    "Phone1='"+Phone1+"'," +
                    "Phone2='"+Phone2+"'," +
                    "Phone3='"+Phone3+"'," +
                    "Phone4='"+Phone4+"'," +
                    "MainIncome='"+MainIncome+"'," +
                    "OtherIncome='"+OtherIncome+"'," +
                    "DailyCost='"+DailyCost+"'," +
                    "OtherCost='"+OtherCost+"'," +
                    "NettIncome='"+NettIncome+"'," +
                    "BusinessSpecification='"+BusinessSpecification+"'," +
                    "TypeofFinancingPayment='"+TypeofFinancingPayment+"'," +
                    "InsuranceCompany='"+InsuranceCompany+"'," +
                    "TypeOfInsurance='"+TypeOfInsurance+"'," +
                    "PurchaseStatus='"+PurchaseStatus+"'," +
                    "AssetsOwned='"+AssetsOwned+"'," +
                    "OtherFinancingExperience='"+OtherFinancingExperience+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_customer(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "CustomerType," +
                    "TitleofCustomer," +
                    "CustomerName," +
                    "BusinessSector," +
                    "PICCompanyName," +
                    "PICJobTitle," +
                    "Phone1," +
                    "Phone2," +
                    "Phone3," +
                    "Phone4," +
                    "MainIncome," +
                    "OtherIncome," +
                    "DailyCost," +
                    "OtherCost," +
                    "NettIncome," +
                    "BusinessSpecification," +
                    "TypeofFinancingPayment," +
                    "InsuranceCompany," +
                    "TypeOfInsurance," +
                    "PurchaseStatus," +
                    "AssetsOwned," +
                    "OtherFinancingExperience" +
                    ")" +
                    "VALUES" +
                    "(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+CustomerType+"'," +
                    "'"+TitleofCustomer+"'," +
                    "'"+CustomerName+"'," +
                    "'"+BusinessSector+"'," +
                    "'"+PICCompanyName+"'," +
                    "'"+PICJobTitle+"'," +
                    "'"+Phone1+"'," +
                    "'"+Phone2+"'," +
                    "'"+Phone3+"'," +
                    "'"+Phone4+"'," +
                    "'"+MainIncome+"'," +
                    "'"+OtherIncome+"'," +
                    "'"+DailyCost+"'," +
                    "'"+OtherCost+"'," +
                    "'"+NettIncome+"'," +
                    "'"+BusinessSpecification+"'," +
                    "'"+TypeofFinancingPayment+"'," +
                    "'"+InsuranceCompany+"'," +
                    "'"+TypeOfInsurance+"'," +
                    "'"+PurchaseStatus+"'," +
                    "'"+AssetsOwned+"'," +
                    "'"+OtherFinancingExperience+"'" +
                    ")");
            i++;
        }
        String isKTPCustomers=(data.has("IsDirectorID"))? data.getString("IsDirectorID").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesKTPCustomer=(data.has("DirectorID"))? data.getString("DirectorID").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isNPWP=(data.has("IsNPWP"))? data.getString("IsNPWP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesNPWPCustomer=(data.has("NPWP"))? data.getString("NPWP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isTDP=(data.has("IsTDP"))? data.getString("IsTDP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesTDP=(data.has("TDP"))? data.getString("TDP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isSIUP=(data.has("IsSIUP"))? data.getString("IsSIUP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesSIUP=(data.has("SIUP"))? data.getString("SIUP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isSKDomicilie=(data.has("IsSKResi"))? data.getString("IsSKResi").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesSKDomicilie=(data.has("SKResi"))? data.getString("SKResi").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isAktaBudgetingUU=(data.has("IsAPA"))? data.getString("IsAPA").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesAktaBudgetingUU=(data.has("APA"))? data.getString("APA").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isAktaDirectors=(data.has("IsAP"))? data.getString("IsAP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesAktaDirectors=(data.has("AP"))? data.getString("AP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isAktaModal=(data.has("isAPMT"))? data.getString("IsAPMT").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesAktaModal=(data.has("APMT"))? data.getString("APMT").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isAktaSaham=(data.has("IsAPST"))? data.getString("IsAPST").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesAktaSaham=(data.has("APST"))? data.getString("APST").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isBankAccount=(data.has("IsLast3MAccount"))? data.getString("IsLast3MAccount").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesBankAccount=(data.has("Last3MAccount"))? data.getString("Last3MAccount").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isAdditionalDocumentBPKB=(data.has("IsOtherForBPKB"))? data.getString("IsOtherForBPKB").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesAdditionalDocumentBPKB=(data.has("OtherForBPKB"))? data.getString("OtherForBPKB").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isOtherSupportingDocument=(data.has("IsOtherSupport"))? data.getString("IsOtherSupport").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesOtherSupportingDocument=(data.has("OtherSupport"))? data.getString("OtherSupport").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordCD=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_customer_documentation " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordCD.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_customer_documentation SET " +
                    "isKTPCustomers='"+isKTPCustomers+"',"+
                    "NotesKTPCustomer='"+NotesKTPCustomer+"',"+
                    "isNPWP='"+isNPWP+"',"+
                    "NotesNPWPCustomer='"+NotesNPWPCustomer+"',"+
                    "isTDP='"+isTDP+"',"+
                    "NotesTDP='"+NotesTDP+"',"+
                    "isSIUP='"+isSIUP+"',"+
                    "NotesSIUP='"+NotesSIUP+"',"+
                    "isSKDomicilie='"+isSKDomicilie+"',"+
                    "NotesSKDomicilie='"+NotesSKDomicilie+"',"+
                    "isAktaBudgetingUU='"+isAktaBudgetingUU+"',"+
                    "NotesAktaBudgetingUU='"+NotesAktaBudgetingUU+"',"+
                    "isAktaDirectors='"+isAktaDirectors+"',"+
                    "NotesAktaDirectors='"+NotesAktaDirectors+"',"+
                    "isAktaModal='"+isAktaModal+"',"+
                    "NotesAktaModal='"+NotesAktaModal+"',"+
                    "isAktaSaham='"+isAktaSaham+"',"+
                    "NotesAktaSaham='"+NotesAktaSaham+"',"+
                    "isBankAccount='"+isBankAccount+"',"+
                    "NotesBankAccount='"+NotesBankAccount+"',"+
                    "isAdditionalDocumentBPKB='"+isAdditionalDocumentBPKB+"',"+
                    "NotesAdditionalDocumentBPKB='"+NotesAdditionalDocumentBPKB+"',"+
                    "isOtherSupportingDocument='"+isOtherSupportingDocument+"',"+
                    "NotesOtherSupportingDocument='"+NotesOtherSupportingDocument+"'"+
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_customer_documentation(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "isKTPCustomers," +
                    "NotesKTPCustomer," +
                    "isNPWP," +
                    "NotesNPWPCustomer," +
                    "isTDP," +
                    "NotesTDP," +
                    "isSIUP," +
                    "NotesSIUP," +
                    "isSKDomicilie," +
                    "NotesSKDomicilie," +
                    "isAktaBudgetingUU," +
                    "NotesAktaBudgetingUU," +
                    "isAktaDirectors," +
                    "NotesAktaDirectors," +
                    "isAktaModal," +
                    "NotesAktaModal," +
                    "isAktaSaham," +
                    "NotesAktaSaham," +
                    "isBankAccount," +
                    "NotesBankAccount," +
                    "isAdditionalDocumentBPKB," +
                    "NotesAdditionalDocumentBPKB," +
                    "isOtherSupportingDocument," +
                    "NotesOtherSupportingDocument" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+isKTPCustomers+"'," +
                    "'"+NotesKTPCustomer+"'," +
                    "'"+isNPWP+"'," +
                    "'"+NotesNPWPCustomer+"'," +
                    "'"+isTDP+"'," +
                    "'"+NotesTDP+"'," +
                    "'"+isSIUP+"'," +
                    "'"+NotesSIUP+"'," +
                    "'"+isSKDomicilie+"'," +
                    "'"+NotesSKDomicilie+"'," +
                    "'"+isAktaBudgetingUU+"'," +
                    "'"+NotesAktaBudgetingUU+"'," +
                    "'"+isAktaDirectors+"'," +
                    "'"+NotesAktaDirectors+"'," +
                    "'"+isAktaModal+"'," +
                    "'"+NotesAktaModal+"'," +
                    "'"+isAktaSaham+"'," +
                    "'"+NotesAktaSaham+"'," +
                    "'"+isBankAccount+"'," +
                    "'"+NotesBankAccount+"'," +
                    "'"+isAdditionalDocumentBPKB+"'," +
                    "'"+NotesAdditionalDocumentBPKB+"'," +
                    "'"+isOtherSupportingDocument+"'," +
                    "'"+NotesOtherSupportingDocument+"'" +
                    ")");
            i++;
        }
        String BPKBStatus="";
        String BPKBData=(data.has("BPKBData"))? data.getString("BPKBData").replaceAll("['\"\\\\]", "\\\\$0") : "";
        if(BPKBData.equals("BIS2")){
            BPKBStatus=(data.has("StatusBPKB-readable"))? data.getString("StatusBPKB-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        }else{
            BPKBStatus="FPP";
        }
        String BPKBCustomerName=(data.has("BPKBName"))? data.getString("BPKBName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        if(BPKBCustomerName.equals("")){
            BPKBCustomerName=(data.has("BPKBCompanyName"))? data.getString("BPKBCompanyName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        }
        String BPKBTitleofCustomer=(data.has("BPKBTitle"))? data.getString("BPKBTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        if(BPKBTitleofCustomer.equals("")){
            BPKBTitleofCustomer=(data.has("BPKBCompanyType-readable"))? data.getString("BPKBCompanyType-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        }
        String BPKBCustomerRelation=(data.has("BPKBRelation-readable"))? data.getString("BPKBRelation-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBCustomerRelationOther=(data.has("BPKBRelationOther"))? data.getString("BPKBRelationOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBMaritalStatus=(data.has("BPKBMaritalStatus-readable"))? data.getString("BPKBMaritalStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBSpouseName=(data.has("BPKBSpouseName"))? data.getString("BPKBSpouseName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBSpouseTitle=(data.has("BPKBSpouseTitle"))? data.getString("BPKBSpouseTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessStatus=(data.has("BPKBBusinessStatus-readable"))? data.getString("BPKBBusinessStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessStatusOther=(data.has("BPKBBusinessStatusOther"))? data.getString("BPKBBusinessStatusOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordBI=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_bpkb_information " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordBI.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_bpkb_information SET " +
                    "BPKBStatus='"+BPKBStatus+"'," +
                    "CustomerName='"+BPKBCustomerName+"'," +
                    "TitleofCustomer='"+BPKBTitleofCustomer+"'," +
                    "CustomerRelation='"+BPKBCustomerRelation+"'," +
                    "CustomerRelationOther='"+BPKBCustomerRelationOther+"'," +
                    "MaritalStatus='"+BPKBMaritalStatus+"'," +
                    "SpouseName='"+BPKBSpouseName+"'," +
                    "SpouseTitle='"+BPKBSpouseTitle+"'," +
                    "BusinessStatus='"+BusinessStatus+"'," +
                    "BusinessStatusOther='"+BusinessStatusOther+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_bpkb_information(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "BPKBStatus," +
                    "CustomerName," +
                    "TitleofCustomer," +
                    "CustomerRelation," +
                    "CustomerRelationOther," +
                    "MaritalStatus," +
                    "SpouseName," +
                    "SpouseTitle," +
                    "BusinessStatus," +
                    "BusinessStatusOther" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+BPKBStatus+"'," +
                    "'"+BPKBCustomerName+"'," +
                    "'"+BPKBTitleofCustomer+"'," +
                    "'"+BPKBCustomerRelation+"'," +
                    "'"+BPKBCustomerRelationOther+"'," +
                    "'"+BPKBMaritalStatus+"'," +
                    "'"+BPKBSpouseName+"'," +
                    "'"+BPKBSpouseTitle+"'," +
                    "'"+BusinessStatus+"'," +
                    "'"+BusinessStatusOther+"'" +
                    ")");
            i++;
        }
        String TradeCustomerName1=(data.has("FirstTradeName"))? data.getString("FirstTradeName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTitleofCustomer1=(data.has("FirstTradeTitle"))? data.getString("FirstTradeTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeCustomerRelation1=(data.has("FirstTradeRelation"))? data.getString("FirstTradeRelation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTelephone1=(data.has("FirstTradeTelp"))? data.getString("FirstTradeTelp").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeMobilePhone1=(data.has("FirstTradeHandphone"))? data.getString("FirstTradeHandphone").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeResult1=(data.has("FirstTradeResult"))? data.getString("FirstTradeResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeCustomerName2=(data.has("SecondTradeName"))? data.getString("SecondTradeName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTitleofCustomer2=(data.has("SecondTradeTitle"))? data.getString("SecondTradeTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeCustomerRelation2=(data.has("SecondTradeRelation"))? data.getString("SecondTradeRelation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTelephone2=(data.has("SecondTradeTelp"))? data.getString("SecondTradeTelp").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeMobilePhone2=(data.has("SecondTradeHandphone"))? data.getString("SecondTradeHandphone").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeResult2=(data.has("SecondTradeResult"))? data.getString("SecondTradeResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordTC=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_trade_checking " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordTC.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_trade_checking SET " +
                    "TradeCustomerName1='"+TradeCustomerName1+"'," +
                    "TradeTitleofCustomer1='"+TradeTitleofCustomer1+"'," +
                    "TradeCustomerRelation1='"+TradeCustomerRelation1+"'," +
                    "TradeTelephone1='"+TradeTelephone1+"'," +
                    "TradeMobilePhone1='"+TradeMobilePhone1+"'," +
                    "TradeResult1='"+TradeResult1+"'," +
                    "TradeCustomerName2='"+TradeCustomerName2+"'," +
                    "TradeTitleofCustomer2='"+TradeTitleofCustomer2+"'," +
                    "TradeCustomerRelation2='"+TradeCustomerRelation2+"'," +
                    "TradeTelephone2='"+TradeTelephone2+"'," +
                    "TradeMobilePhone2='"+TradeMobilePhone2+"'," +
                    "TradeResult2='"+TradeResult2+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_trade_checking(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "TradeCustomerName1," +
                    "TradeTitleofCustomer1," +
                    "TradeCustomerRelation1," +
                    "TradeTelephone1," +
                    "TradeMobilePhone1," +
                    "TradeResult1," +
                    "TradeCustomerName2," +
                    "TradeTitleofCustomer2," +
                    "TradeCustomerRelation2," +
                    "TradeTelephone2," +
                    "TradeMobilePhone2," +
                    "TradeResult2" +
                    ")VALUES(" +
                    "'"+branchID+"',"+
                    "'"+orderID+"',"+
                    "'"+fppNo+"',"+
                    "'"+TradeCustomerName1+"'," +
                    "'"+TradeTitleofCustomer1+"'," +
                    "'"+TradeCustomerRelation1+"'," +
                    "'"+TradeTelephone1+"'," +
                    "'"+TradeMobilePhone1+"'," +
                    "'"+TradeResult1+"'," +
                    "'"+TradeCustomerName2+"'," +
                    "'"+TradeTitleofCustomer2+"'," +
                    "'"+TradeCustomerRelation2+"'," +
                    "'"+TradeTelephone2+"'," +
                    "'"+TradeMobilePhone2+"'," +
                    "'"+TradeResult2+"'" +
                    ")");
            i++;
        }
        String Photo1Loc=(data.has("Photo1Loc"))? data.getString("Photo1Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo1Decription=(data.has("Photo1Desc"))? data.getString("Photo1Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo2Loc=(data.has("Photo2Loc"))? data.getString("Photo2Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo2Decription=(data.has("Photo2Desc"))? data.getString("Photo2Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo3Loc=(data.has("Photo3Loc"))? data.getString("Photo3Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo3Decription=(data.has("Photo3Desc"))? data.getString("Photo3Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo4Loc=(data.has("Photo4Loc"))? data.getString("Photo4Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo4Decription=(data.has("Photo4Desc"))? data.getString("Photo4Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo5Loc=(data.has("Photo5Loc"))? data.getString("Photo5Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo5Decription=(data.has("Photo5Desc"))? data.getString("Photo5Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo6Loc=(data.has("Photo6Loc"))? data.getString("Photo6Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo6Decription=(data.has("Photo6Desc"))? data.getString("Photo6Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo7Loc=(data.has("Photo7Loc"))? data.getString("Photo7Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo7Decription=(data.has("Photo7Decription"))? data.getString("Photo7Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo8Loc=(data.has("Photo8Loc"))? data.getString("Photo8Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo8Decription=(data.has("Photo8Desc"))? data.getString("Photo8Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo9Loc=(data.has("Photo9Loc"))? data.getString("Photo9Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo9Decription=(data.has("Photo9Desc"))? data.getString("Photo9Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo10Loc=(data.has("Photo10Loc"))? data.getString("Photo10Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo10Decription=(data.has("Photo10Desc"))? data.getString("Photo10Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordUP=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_upload_documentation " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordUP.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_upload_documentation SET " +
                    "Photo1Loc='"+Photo1Loc+"'," +
                    "Photo1Decription='FOTO KANTOR'," +
                    "Photo2Loc='"+Photo2Loc+"'," +
                    "Photo2Decription='"+Photo2Decription+"'," +
                    "Photo3Loc='"+Photo3Loc+"'," +
                    "Photo3Decription='"+Photo3Decription+"'," +
                    "Photo4Loc='"+Photo4Loc+"'," +
                    "Photo4Decription='"+Photo4Decription+"'," +
                    "Photo5Loc='"+Photo5Loc+"'," +
                    "Photo5Decription='"+Photo5Decription+"'," +
                    "Photo6Loc='"+Photo6Loc+"'," +
                    "Photo6Decription='"+Photo6Decription+"'," +
                    "Photo7Loc='"+Photo7Loc+"'," +
                    "Photo7Decription='"+Photo7Decription+"'," +
                    "Photo8Loc='"+Photo8Loc+"'," +
                    "Photo8Decription='"+Photo8Decription+"'," +
                    "Photo9Loc='"+Photo9Loc+"'," +
                    "Photo9Decription='"+Photo9Decription+"'," +
                    "Photo10Loc='"+Photo10Loc+"'," +
                    "Photo10Decription='"+Photo10Decription+"' " +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_upload_documentation (" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "Photo1Loc," +
                    "Photo1Decription," +
                    "Photo2Loc," +
                    "Photo2Decription," +
                    "Photo3Loc," +
                    "Photo3Decription," +
                    "Photo4Loc," +
                    "Photo4Decription," +
                    "Photo5Loc," +
                    "Photo5Decription," +
                    "Photo6Loc," +
                    "Photo6Decription," +
                    "Photo7Loc," +
                    "Photo7Decription," +
                    "Photo8Loc," +
                    "Photo8Decription," +
                    "Photo9Loc," +
                    "Photo9Decription," +
                    "Photo10Loc," +
                    "Photo10Decription" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+Photo1Loc+"'," +
                    "'FOTO KANTOR'," +
                    "'"+Photo2Loc+"'," +
                    "'"+Photo2Decription+"'," +
                    "'"+Photo3Loc+"'," +
                    "'"+Photo3Decription+"'," +
                    "'"+Photo4Loc+"'," +
                    "'"+Photo4Decription+"'," +
                    "'"+Photo5Loc+"'," +
                    "'"+Photo5Decription+"'," +
                    "'"+Photo6Loc+"'," +
                    "'"+Photo6Decription+"'," +
                    "'"+Photo7Loc+"'," +
                    "'"+Photo7Decription+"'," +
                    "'"+Photo8Loc+"'," +
                    "'"+Photo8Decription+"'," +
                    "'"+Photo9Loc+"'," +
                    "'"+Photo9Decription+"'," +
                    "'"+Photo10Loc+"'," +
                    "'"+Photo10Decription+"'" +
                    ")");
            i++;
        }
        String isSurveyOffice1=(data.has("IsOffice1"))? data.getString("IsOffice1").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeAddress1=(data.has("Office1Address"))? data.getString("Office1Address").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeRT1=(data.has("Office1RT"))? data.getString("Office1RT").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeRW1=(data.has("Office1RW"))? data.getString("Office1RW").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Officedistricts1=(data.has("Office1Region-readable"))? data.getString("Office1Region-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeCities1=(data.has("Office1City-readable"))? data.getString("Office1City-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeProvince1=(data.has("Office1Province-readable"))? data.getString("Office1Province-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeVillage1=(data.has("Office1Village-readable"))? data.getString("Office1Village-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficePostCode1=(data.has("Office1ZIP"))? data.getString("Office1ZIP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeStatusofBuilding1=(data.has("Office1BuildingOwnership-readable"))? data.getString("Office1BuildingOwnership-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeLocation1=(data.has("Office1Location-readable"))? data.getString("Office1Location-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeOtherLocation1=(data.has("Office1LocationOther"))? data.getString("Office1LocationOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeAccessLocation1=(data.has("Office1Access-readable"))? data.getString("Office1Access-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeTotalofEmployee1=(data.has("Office1NumberEmployer"))? data.getString("Office1NumberEmployer").replaceAll("['\"\\\\]", "\\\\$0") : "";

        String isSurveyOffice2=(data.has("IsOffice2"))? data.getString("IsOffice2").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeAddress2=(data.has("Office2Address"))? data.getString("Office2Address").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeRT2=(data.has("Office2RT"))? data.getString("Office2RT").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeRW2=(data.has("Office2RW"))? data.getString("Office2RW").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeVillage2=(data.has("Office2Village-readable"))? data.getString("Office2Village-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Officedistricts2=(data.has("Office2Region-readable"))? data.getString("Office2Region-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeCities2=(data.has("Office2City-readable"))? data.getString("Office2City-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeProvince2=(data.has("Office2Province"))? data.getString("Office2Province-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficePostCode2=(data.has("Office2ZIP"))? data.getString("Office2ZIP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeStatusofBuilding2=(data.has("Office2BuildingOwnership-readable"))? data.getString("Office2BuildingOwnership-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeLocation2=(data.has("Office2Location-readable"))? data.getString("Office2Location-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeOtherLocation2=(data.has("Office2LocationOther"))? data.getString("Office2LocationOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeAccessLocation2=(data.has("Office2Access-readable"))? data.getString("Office2Access-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeTotalofEmployee2=(data.has("Office2NumberEmployer"))? data.getString("Office2NumberEmployer").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordSL=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_survey_location " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordSL.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_survey_location SET " +
                    "isSurveyOffice1='"+isSurveyOffice1+"'," +
                    "OfficeAddress1='"+OfficeAddress1+"'," +
                    "OfficeRT1='"+OfficeRT1+"'," +
                    "OfficeRW1='"+OfficeRW1+"'," +
                    "OfficeVillage1='"+OfficeVillage1+"'," +
                    "Officedistricts1='"+Officedistricts1+"'," +
                    "OfficeCities1='"+OfficeCities1+"'," +
                    "OfficeProvince1='"+OfficeProvince1+"'," +
                    "OfficePostCode1='"+OfficePostCode1+"'," +
                    "OfficeStatusofBuilding1='"+OfficeStatusofBuilding1+"'," +
                    "OfficeLocation1='"+OfficeLocation1+"'," +
                    "OfficeOtherLocation1='"+OfficeOtherLocation1+"'," +
                    "OfficeAccessLocation1='"+OfficeAccessLocation1+"'," +
                    "OfficeTotalofEmployee1='"+OfficeTotalofEmployee1+"'," +
                    "isSurveyOffice2='"+isSurveyOffice2+"'," +
                    "OfficeAddress2='"+OfficeAddress2+"'," +
                    "OfficeRT2='"+OfficeRT2+"'," +
                    "OfficeRW2='"+OfficeRW2+"'," +
                    "OfficeVillage2='"+OfficeVillage2+"'," +
                    "Officedistricts2='"+Officedistricts2+"'," +
                    "OfficeCities2='"+OfficeCities2+"'," +
                    "OfficeProvince2='"+OfficeProvince2+"'," +
                    "OfficePostCode2='"+OfficePostCode2+"'," +
                    "OfficeStatusofBuilding2='"+OfficeStatusofBuilding2+"'," +
                    "OfficeLocation2='"+OfficeLocation2+"'," +
                    "OfficeOtherLocation2='"+OfficeOtherLocation2+"'," +
                    "OfficeAccessLocation2='"+OfficeAccessLocation2+"'," +
                    "OfficeTotalofEmployee2='"+OfficeTotalofEmployee2+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_survey_location (" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "isSurveyOffice1," +
                    "OfficeAddress1," +
                    "OfficeRT1," +
                    "OfficeRW1," +
                    "Officedistricts1," +
                    "OfficeCities1," +
                    "OfficeProvince1," +
                    "OfficeVillage1," +
                    "OfficePostCode1," +
                    "OfficeStatusofBuilding1," +
                    "OfficeLocation1," +
                    "OfficeOtherLocation1," +
                    "OfficeAccessLocation1," +
                    "OfficeTotalofEmployee1," +
                    "isSurveyOffice2," +
                    "OfficeAddress2," +
                    "OfficeRT2," +
                    "OfficeRW2," +
                    "Officedistricts2," +
                    "OfficeCities2," +
                    "OfficeProvince2," +
                    "OfficeVillage2," +
                    "OfficePostCode2," +
                    "OfficeStatusofBuilding2," +
                    "OfficeLocation2," +
                    "OfficeOtherLocation2," +
                    "OfficeAccessLocation2," +
                    "OfficeTotalofEmployee2" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+isSurveyOffice1+"'," +
                    "'"+OfficeAddress1+"'," +
                    "'"+OfficeRT1+"'," +
                    "'"+OfficeRW1+"'," +
                    "'"+Officedistricts1+"'," +
                    "'"+OfficeCities1+"'," +
                    "'"+OfficeProvince1+"'," +
                    "'"+OfficeVillage1+"'," +
                    "'"+OfficePostCode1+"'," +
                    "'"+OfficeStatusofBuilding1+"'," +
                    "'"+OfficeLocation1+"'," +
                    "'"+OfficeOtherLocation1+"'," +
                    "'"+OfficeAccessLocation1+"'," +
                    "'"+OfficeTotalofEmployee1+"'," +
                    "'"+isSurveyOffice2+"'," +
                    "'"+OfficeAddress2+"'," +
                    "'"+OfficeRT2+"'," +
                    "'"+OfficeRW2+"'," +
                    "'"+Officedistricts2+"'," +
                    "'"+OfficeCities2+"'," +
                    "'"+OfficeProvince2+"'," +
                    "'"+OfficeVillage2+"'," +
                    "'"+OfficePostCode2+"'," +
                    "'"+OfficeStatusofBuilding2+"'," +
                    "'"+OfficeLocation2+"'," +
                    "'"+OfficeOtherLocation2+"'," +
                    "'"+OfficeAccessLocation2+"'," +
                    "'"+OfficeTotalofEmployee2+"'" +
                    ")");
            i++;
        }
        queries.add(i,"update dsf_mobile_orders set status='97'," +
                "dblinkStatus='C'," +
                "isCustomerCalled='1'," +
                "isCustomerMet='1'," +
                "updatedDate='"+Utils.dateFull()+"'," +
                "recommendation='"+Recomendation+"'," +
                "surveyResult='"+SurveyResult+"' " +
                "WHERE orderID='"+orderID+"'");
        return queries;
    }
    public ArrayList<String> individualSurvey(Dao dao,JSONObject data){
        ArrayList<String> queries=new ArrayList<>();
        String SurveyResult=(data.has("SurveyResult"))? data.getString("SurveyResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Recomendation=(data.has("Recomendation"))? data.getString("Recomendation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String orderID=(data.has("survey-orderId"))? data.getString("survey-orderId").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String fppNo=(data.has("fpp_no"))? data.getString("fpp_no").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject dataOrder=dao.getDataByQuery("SELECT branchID,surveyorID FROM dsf_mobile_orders WHERE orderID='"+orderID+"'");
        String branchID=dataOrder.getJSONArray("data").getJSONObject(0).getString("branchID");
        String surveyorID=dataOrder.getJSONArray("data").getJSONObject(0).getString("surveyorID");
        int i=0;
        String CustomerType="INDIVIDU";
        String BPKBStatus="";
        String BPKBData=(data.has("BPKBData"))? data.getString("BPKBData").replaceAll("['\"\\\\]", "\\\\$0") : "";
        if(BPKBData.equals("BIS2")){
            BPKBStatus=(data.has("StatusBPKB-readable"))? data.getString("StatusBPKB-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        }else{
            BPKBStatus="FPP";
        }
        String BPKBCustomerName=(data.has("BPKBName"))? data.getString("BPKBName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        if(BPKBCustomerName.equals("")){
            BPKBCustomerName=(data.has("BPKBCompanyName"))? data.getString("BPKBCompanyName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        }
        String BPKBTitleofCustomer=(data.has("BPKBTitle"))? data.getString("BPKBTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        if(BPKBTitleofCustomer.equals("")){
            BPKBTitleofCustomer=(data.has("BPKBCompanyType-readable"))? data.getString("BPKBCompanyType-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        }
        String BPKBCustomerRelation=(data.has("BPKBRelation-readable"))? data.getString("BPKBRelation-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBCustomerRelationOther=(data.has("BPKBRelationOther"))? data.getString("BPKBRelationOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBMaritalStatus=(data.has("BPKBMaritalStatus-readable"))? data.getString("BPKBMaritalStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBSpouseName=(data.has("BPKBSpouseName"))? data.getString("BPKBSpouseName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BPKBSpouseTitle=(data.has("BPKBSpouseTitle"))? data.getString("BPKBSpouseTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessStatus=(data.has("BPKBBusinessStatus-readable"))? data.getString("BPKBBusinessStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessStatusOther=(data.has("BPKBBusinessStatusOther"))? data.getString("BPKBBusinessStatusOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordBI=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_bpkb_information " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordBI.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_bpkb_information SET " +
                    "BPKBStatus='"+BPKBStatus+"'," +
                    "CustomerName='"+BPKBCustomerName+"'," +
                    "TitleofCustomer='"+BPKBTitleofCustomer+"'," +
                    "CustomerRelation='"+BPKBCustomerRelation+"'," +
                    "CustomerRelationOther='"+BPKBCustomerRelationOther+"'," +
                    "MaritalStatus='"+BPKBMaritalStatus+"'," +
                    "SpouseName='"+BPKBSpouseName+"'," +
                    "SpouseTitle='"+BPKBSpouseTitle+"'," +
                    "BusinessStatus='"+BusinessStatus+"'," +
                    "BusinessStatusOther='"+BusinessStatusOther+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_bpkb_information(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "BPKBStatus," +
                    "CustomerName," +
                    "TitleofCustomer," +
                    "CustomerRelation," +
                    "CustomerRelationOther," +
                    "MaritalStatus," +
                    "SpouseName," +
                    "SpouseTitle," +
                    "BusinessStatus," +
                    "BusinessStatusOther" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+BPKBStatus+"'," +
                    "'"+BPKBCustomerName+"'," +
                    "'"+BPKBTitleofCustomer+"'," +
                    "'"+BPKBCustomerRelation+"'," +
                    "'"+BPKBCustomerRelationOther+"'," +
                    "'"+BPKBMaritalStatus+"'," +
                    "'"+BPKBSpouseName+"'," +
                    "'"+BPKBSpouseTitle+"'," +
                    "'"+BusinessStatus+"'," +
                    "'"+BusinessStatusOther+"'" +
                    ")");
            i++;
        }
        String TradeCustomerName1=(data.has("FirstTradeName"))? data.getString("FirstTradeName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTitleofCustomer1=(data.has("FirstTradeTitle"))? data.getString("FirstTradeTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeCustomerRelation1=(data.has("FirstTradeRelation"))? data.getString("FirstTradeRelation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTelephone1=(data.has("FirstTradeTelp"))? data.getString("FirstTradeTelp").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeMobilePhone1=(data.has("FirstTradeHandphone"))? data.getString("FirstTradeHandphone").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeResult1=(data.has("FirstTradeResult"))? data.getString("FirstTradeResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeCustomerName2=(data.has("SecondTradeName"))? data.getString("SecondTradeName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTitleofCustomer2=(data.has("SecondTradeTitle"))? data.getString("SecondTradeTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeCustomerRelation2=(data.has("SecondTradeRelation"))? data.getString("SecondTradeRelation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeTelephone2=(data.has("SecondTradeTelp"))? data.getString("SecondTradeTelp").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeMobilePhone2=(data.has("SecondTradeHandphone"))? data.getString("SecondTradeHandphone").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TradeResult2=(data.has("SecondTradeResult"))? data.getString("SecondTradeResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordTC=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_trade_checking " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordTC.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_trade_checking SET " +
                    "TradeCustomerName1='"+TradeCustomerName1+"'," +
                    "TradeTitleofCustomer1='"+TradeTitleofCustomer1+"'," +
                    "TradeCustomerRelation1='"+TradeCustomerRelation1+"'," +
                    "TradeTelephone1='"+TradeTelephone1+"'," +
                    "TradeMobilePhone1='"+TradeMobilePhone1+"'," +
                    "TradeResult1='"+TradeResult1+"'," +
                    "TradeCustomerName2='"+TradeCustomerName2+"'," +
                    "TradeTitleofCustomer2='"+TradeTitleofCustomer2+"'," +
                    "TradeCustomerRelation2='"+TradeCustomerRelation2+"'," +
                    "TradeTelephone2='"+TradeTelephone2+"'," +
                    "TradeMobilePhone2='"+TradeMobilePhone2+"'," +
                    "TradeResult2='"+TradeResult2+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_trade_checking(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "TradeCustomerName1," +
                    "TradeTitleofCustomer1," +
                    "TradeCustomerRelation1," +
                    "TradeTelephone1," +
                    "TradeMobilePhone1," +
                    "TradeResult1," +
                    "TradeCustomerName2," +
                    "TradeTitleofCustomer2," +
                    "TradeCustomerRelation2," +
                    "TradeTelephone2," +
                    "TradeMobilePhone2," +
                    "TradeResult2" +
                    ")VALUES(" +
                    "'"+branchID+"',"+
                    "'"+orderID+"',"+
                    "'"+fppNo+"',"+
                    "'"+TradeCustomerName1+"'," +
                    "'"+TradeTitleofCustomer1+"'," +
                    "'"+TradeCustomerRelation1+"'," +
                    "'"+TradeTelephone1+"'," +
                    "'"+TradeMobilePhone1+"'," +
                    "'"+TradeResult1+"'," +
                    "'"+TradeCustomerName2+"'," +
                    "'"+TradeTitleofCustomer2+"'," +
                    "'"+TradeCustomerRelation2+"'," +
                    "'"+TradeTelephone2+"'," +
                    "'"+TradeMobilePhone2+"'," +
                    "'"+TradeResult2+"'" +
                    ")");
            i++;
        }
        String Photo1Loc=(data.has("Photo1Loc"))? data.getString("Photo1Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo1Decription=(data.has("Photo1Desc"))? data.getString("Photo1Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo2Loc=(data.has("Photo2Loc"))? data.getString("Photo2Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo2Decription=(data.has("Photo2Desc"))? data.getString("Photo2Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo3Loc=(data.has("Photo3Loc"))? data.getString("Photo3Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo3Decription=(data.has("Photo3Desc"))? data.getString("Photo3Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo4Loc=(data.has("Photo4Loc"))? data.getString("Photo4Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo4Decription=(data.has("Photo4Desc"))? data.getString("Photo4Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo5Loc=(data.has("Photo5Loc"))? data.getString("Photo5Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo5Decription=(data.has("Photo5Desc"))? data.getString("Photo5Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo6Loc=(data.has("Photo6Loc"))? data.getString("Photo6Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo6Decription=(data.has("Photo6Desc"))? data.getString("Photo6Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo7Loc=(data.has("Photo7Loc"))? data.getString("Photo7Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo7Decription=(data.has("Photo7Desc"))? data.getString("Photo7Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo8Loc=(data.has("Photo8Loc"))? data.getString("Photo8Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo8Decription=(data.has("Photo8Desc"))? data.getString("Photo8Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo9Loc=(data.has("Photo9Loc"))? data.getString("Photo9Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo9Decription=(data.has("Photo9Desc"))? data.getString("Photo9Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo10Loc=(data.has("Photo10Loc"))? data.getString("Photo10Loc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Photo10Decription=(data.has("Photo10Desc"))? data.getString("Photo10Desc").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordUP=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_upload_documentation " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordUP.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_upload_documentation SET " +
                    "Photo1Loc='"+Photo1Loc+"'," +
                    "Photo1Decription='FOTO RUMAH'," +
                    "Photo2Loc='"+Photo2Loc+"'," +
                    "Photo2Decription='"+Photo2Decription+"'," +
                    "Photo3Loc='"+Photo3Loc+"'," +
                    "Photo3Decription='"+Photo3Decription+"'," +
                    "Photo4Loc='"+Photo4Loc+"'," +
                    "Photo4Decription='"+Photo4Decription+"'," +
                    "Photo5Loc='"+Photo5Loc+"'," +
                    "Photo5Decription='"+Photo5Decription+"'," +
                    "Photo6Loc='"+Photo6Loc+"'," +
                    "Photo6Decription='"+Photo6Decription+"'," +
                    "Photo7Loc='"+Photo7Loc+"'," +
                    "Photo7Decription='"+Photo7Decription+"'," +
                    "Photo8Loc='"+Photo8Loc+"'," +
                    "Photo8Decription='"+Photo8Decription+"'," +
                    "Photo9Loc='"+Photo9Loc+"'," +
                    "Photo9Decription='"+Photo9Decription+"'," +
                    "Photo10Loc='"+Photo10Loc+"'," +
                    "Photo10Decription='"+Photo10Decription+"' " +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_upload_documentation (" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "Photo1Loc," +
                    "Photo1Decription," +
                    "Photo2Loc," +
                    "Photo2Decription," +
                    "Photo3Loc," +
                    "Photo3Decription," +
                    "Photo4Loc," +
                    "Photo4Decription," +
                    "Photo5Loc," +
                    "Photo5Decription," +
                    "Photo6Loc," +
                    "Photo6Decription," +
                    "Photo7Loc," +
                    "Photo7Decription," +
                    "Photo8Loc," +
                    "Photo8Decription," +
                    "Photo9Loc," +
                    "Photo9Decription," +
                    "Photo10Loc," +
                    "Photo10Decription" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+Photo1Loc+"'," +
                    "'FOTO RUMAH'," +
                    "'"+Photo2Loc+"'," +
                    "'"+Photo2Decription+"'," +
                    "'"+Photo3Loc+"'," +
                    "'"+Photo3Decription+"'," +
                    "'"+Photo4Loc+"'," +
                    "'"+Photo4Decription+"'," +
                    "'"+Photo5Loc+"'," +
                    "'"+Photo5Decription+"'," +
                    "'"+Photo6Loc+"'," +
                    "'"+Photo6Decription+"'," +
                    "'"+Photo7Loc+"'," +
                    "'"+Photo7Decription+"'," +
                    "'"+Photo8Loc+"'," +
                    "'"+Photo8Decription+"'," +
                    "'"+Photo9Loc+"'," +
                    "'"+Photo9Decription+"'," +
                    "'"+Photo10Loc+"'," +
                    "'"+Photo10Decription+"'" +
                    ")");
            i++;
        }
        String TitleofCustomer=(data.has("CustomerTitle"))? data.getString("CustomerTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String CustomerName=(data.has("CustomerName"))? data.getString("CustomerName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessSector=(data.has("CompanySector-readable"))? data.getString("CompanySector-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TypeofWork=(data.has("CustomerJob-readable"))? data.getString("CustomerJob-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone1=(data.has("CustomerHP01"))? data.getString("CustomerHP01").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone2=(data.has("CustomerHP02"))? data.getString("CustomerHP02").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Phone3=(data.has("CustomerHP03"))? data.getString("CustomerHP03").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String MainIncome=(data.has("MainSalary"))? data.getString("MainSalary").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OtherIncome=(data.has("OtherSalary"))? data.getString("OtherSalary").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DailyCost=(data.has("DailySpend"))? data.getString("DailySpend").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OtherCost=(data.has("OtherSpend"))? data.getString("OtherSpend").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NettIncome=(data.has("NettSalary"))? data.getString("NettSalary").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String BusinessSpecification=(data.has("BusinessSpec"))? data.getString("BusinessSpec").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TypeofFinancingPayment=(data.has("TypeOfFinancingPayment-readable"))? data.getString("TypeOfFinancingPayment-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String InsuranceCompany=(data.has("InsuranceCompany-readable"))? data.getString("InsuranceCompany-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String TypeOfInsurance=(data.has("TypeOfInsurance-readable"))? data.getString("TypeOfInsurance-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String PurchaseStatus=(data.has("PurchaseStatus-readable"))? data.getString("PurchaseStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String AssetsOwned=(data.has("OwnedAsset"))? data.getString("OwnedAsset").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OtherFinancingExperience=(data.has("OtherFinancingExperience"))? data.getString("OtherFinancingExperience").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Gender=(data.has("CustomerGender-readable"))? data.getString("CustomerGender-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String MaritalStatus=(data.has("CustomerStatus-readable"))? data.getString("CustomerStatus-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Nationality=(data.has("CustomerAs-readable"))? data.getString("CustomerAs-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String MotherName=(data.has("CustomerMother"))? data.getString("CustomerMother").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String SpouseName=(data.has("CustomerSpouseName"))? data.getString("CustomerSpouseName").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String SpouseTitle=(data.has("CustomerSpouseTitle"))? data.getString("CustomerSpouseTitle").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordC=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_customer " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordC.getInt("count")>0){
            queries.add(i,"UPDATE  dsf_mst_customer SET " +
                    "CustomerType='"+CustomerType+"'," +
                    "TitleofCustomer='"+TitleofCustomer+"'," +
                    "CustomerName='"+CustomerName+"'," +
                    "BusinessSector='"+BusinessSector+"'," +
                    "TypeofWork='"+TypeofWork+"'," +
                    "Phone1='"+Phone1+"'," +
                    "Phone2='"+Phone2+"'," +
                    "Phone3='"+Phone3+"'," +
                    "MainIncome='"+MainIncome+"'," +
                    "OtherIncome='"+OtherIncome+"'," +
                    "DailyCost='"+DailyCost+"'," +
                    "OtherCost='"+OtherCost+"'," +
                    "NettIncome='"+NettIncome+"'," +
                    "BusinessSpecification='"+BusinessSpecification+"'," +
                    "TypeofFinancingPayment='"+TypeofFinancingPayment+"'," +
                    "InsuranceCompany='"+InsuranceCompany+"'," +
                    "TypeOfInsurance='"+TypeOfInsurance+"'," +
                    "PurchaseStatus='"+PurchaseStatus+"'," +
                    "AssetsOwned='"+AssetsOwned+"'," +
                    "Gender='"+Gender+"'," +
                    "MaritalStatus='"+MaritalStatus+"'," +
                    "Nationality='"+Nationality+"'," +
                    "MotherName='"+MotherName+"'," +
                    "SpouseName='"+SpouseName+"'," +
                    "SpouseTitle='"+SpouseTitle+"'," +
                    "OtherFinancingExperience='"+OtherFinancingExperience+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_customer(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "CustomerType," +
                    "TitleofCustomer," +
                    "CustomerName," +
                    "BusinessSector," +
                    "TypeofWork," +
                    "Phone1," +
                    "Phone2," +
                    "Phone3," +
                    "MainIncome," +
                    "OtherIncome," +
                    "DailyCost," +
                    "OtherCost," +
                    "NettIncome," +
                    "BusinessSpecification," +
                    "TypeofFinancingPayment," +
                    "InsuranceCompany," +
                    "TypeOfInsurance," +
                    "PurchaseStatus," +
                    "AssetsOwned," +
                    "Gender," +
                    "MaritalStatus," +
                    "Nationality," +
                    "MotherName," +
                    "SpouseName," +
                    "SpouseTitle," +
                    "OtherFinancingExperience" +
                    ")" +
                    "VALUES" +
                    "(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+CustomerType+"'," +
                    "'"+TitleofCustomer+"'," +
                    "'"+CustomerName+"'," +
                    "'"+BusinessSector+"'," +
                    "'"+TypeofWork+"'," +
                    "'"+Phone1+"'," +
                    "'"+Phone2+"'," +
                    "'"+Phone3+"'," +
                    "'"+MainIncome+"'," +
                    "'"+OtherIncome+"'," +
                    "'"+DailyCost+"'," +
                    "'"+OtherCost+"'," +
                    "'"+NettIncome+"'," +
                    "'"+BusinessSpecification+"'," +
                    "'"+TypeofFinancingPayment+"'," +
                    "'"+InsuranceCompany+"'," +
                    "'"+TypeOfInsurance+"'," +
                    "'"+PurchaseStatus+"'," +
                    "'"+AssetsOwned+"'," +
                    "'"+Gender+"'," +
                    "'"+MaritalStatus+"'," +
                    "'"+Nationality+"'," +
                    "'"+MotherName+"'," +
                    "'"+SpouseName+"'," +
                    "'"+SpouseTitle+"'," +
                    "'"+OtherFinancingExperience+"'" +
                    ")");
            i++;
        }
        String isKTPCustomers=(data.has("IsIDLesee"))? data.getString("IsIDLesee").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesKTPCustomer=(data.has("TxtIDLesee"))? data.getString("TxtIDLesee").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isNPWP=(data.has("IsNPWP"))? data.getString("IsNPWP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesNPWPCustomer=(data.has("TxtNPWP"))? data.getString("TxtNPWP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isBankAccount=(data.has("IsLast3MAccount"))? data.getString("IsLast3MAccount").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesBankAccount=(data.has("TxtLast3MAccount"))? data.getString("TxtLast3MAccount").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isAdditionalDocumentBPKB=(data.has("IsOtherForBPKB"))? data.getString("IsOtherForBPKB").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesAdditionalDocumentBPKB=(data.has("TxtOtherForBPKB"))? data.getString("TxtOtherForBPKB").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isOtherSupportingDocument=(data.has("IsOtherSupport"))? data.getString("IsOtherSupport").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesOtherSupportingDocument=(data.has("TxtOtherSupport"))? data.getString("TxtOtherSupport").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isKTPSpouse=(data.has("IsIDSpouse"))? data.getString("IsIDSpouse").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesKTPSpouse=(data.has("TxtIDSpouse"))? data.getString("TxtIDSpouse").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isKK=(data.has("IsFC"))? data.getString("IsFC").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesKKCustomer=(data.has("TxtFC"))? data.getString("TxtFC").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isElectricitybill=(data.has("IsNoElec"))? data.getString("IsNoElec").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String NotesElectricitybill=(data.has("TxtNoElec"))? data.getString("TxtNoElec").replaceAll("['\"\\\\]", "\\\\$0") : "";
        JSONObject checkRecordCD=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_customer_documentation " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordCD.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_customer_documentation SET " +
                    "isKTPCustomers='"+isKTPCustomers+"',"+
                    "NotesKTPCustomer='"+NotesKTPCustomer+"',"+
                    "isNPWP='"+isNPWP+"',"+
                    "NotesNPWPCustomer='"+NotesNPWPCustomer+"',"+
                    "isBankAccount='"+isBankAccount+"',"+
                    "NotesBankAccount='"+NotesBankAccount+"',"+
                    "isAdditionalDocumentBPKB='"+isAdditionalDocumentBPKB+"',"+
                    "NotesAdditionalDocumentBPKB='"+NotesAdditionalDocumentBPKB+"',"+
                    "isOtherSupportingDocument='"+isOtherSupportingDocument+"',"+
                    "isKTPSpouse='"+isKTPSpouse+"',"+
                    "NotesKTPSpouse='"+NotesKTPSpouse+"',"+
                    "isKK='"+isKK+"',"+
                    "NotesKKCustomer='"+NotesKKCustomer+"',"+
                    "isElectricitybill='"+isElectricitybill+"',"+
                    "NotesElectricitybill='"+NotesElectricitybill+"',"+
                    "NotesOtherSupportingDocument='"+NotesOtherSupportingDocument+"'"+
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_customer_documentation(" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "isKTPCustomers," +
                    "NotesKTPCustomer," +
                    "isNPWP," +
                    "NotesNPWPCustomer," +
                    "isBankAccount," +
                    "NotesBankAccount," +
                    "isAdditionalDocumentBPKB," +
                    "NotesAdditionalDocumentBPKB," +
                    "isOtherSupportingDocument," +
                    "isKTPSpouse,"+
                    "NotesKTPSpouse,"+
                    "isKK,"+
                    "NotesKKCustomer,"+
                    "isElectricitybill,"+
                    "NotesElectricitybill,"+
                    "NotesOtherSupportingDocument" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+isKTPCustomers+"'," +
                    "'"+NotesKTPCustomer+"'," +
                    "'"+isNPWP+"'," +
                    "'"+NotesNPWPCustomer+"'," +
                    "'"+isBankAccount+"'," +
                    "'"+NotesBankAccount+"'," +
                    "'"+isAdditionalDocumentBPKB+"'," +
                    "'"+NotesAdditionalDocumentBPKB+"'," +
                    "'"+isOtherSupportingDocument+"'," +
                    "'"+isKTPSpouse+"',"+
                    "'"+NotesKTPSpouse+"',"+
                    "'"+isKK+"',"+
                    "'"+NotesKKCustomer+"',"+
                    "'"+isElectricitybill+"',"+
                    "'"+NotesElectricitybill+"',"+
                    "'"+NotesOtherSupportingDocument+"'" +
                    ")");
            i++;
        }
        String isSurveyOffice=(data.has("IsOffice"))? data.getString("IsOffice").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeAddress=(data.has("OfficeAddress"))? data.getString("OfficeAddress").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeRT=(data.has("OfficeRT"))? data.getString("OfficeRT").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeRW=(data.has("OfficeRW"))? data.getString("OfficeRW").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeVillage=(data.has("OfficeVillage-readable"))? data.getString("OfficeVillage-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Officedistricts=(data.has("OfficeRegion-readable"))? data.getString("OfficeRegion-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeCities=(data.has("OfficeCity-readable"))? data.getString("OfficeCity-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeProvince=(data.has("OfficeProvince-readable"))? data.getString("OfficeProvince-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficePostCode=(data.has("OfficeZIP"))? data.getString("OfficeZIP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeStatusofBuilding=(data.has("OfficeBuildingOwnership-readable"))? data.getString("OfficeBuildingOwnership-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeLocation=(data.has("OfficeLocation-readable"))? data.getString("OfficeLocation-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeOtherLocation=(data.has("OfficeLocationOther"))? data.getString("OfficeLocationOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeAccessLocation=(data.has("OfficeAccess-readable"))? data.getString("OfficeAccess-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String OfficeTotalofEmployee=(data.has("OfficeNumberEmployer"))? data.getString("OfficeNumberEmployer").replaceAll("['\"\\\\]", "\\\\$0") : "";

        String isSurveyDomicilie=(data.has("IsResi"))? data.getString("IsResi").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieAddress=(data.has("ResiAddress"))? data.getString("ResiAddress").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieRT=(data.has("ResiRT"))? data.getString("ResiRT").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieRW=(data.has("ResiRW"))? data.getString("ResiRW").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieCities=(data.has("ResiCity-readable"))? data.getString("ResiCity-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieProvince=(data.has("ResiProvince-readable"))? data.getString("ResiProvince-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Domiciliedistricts=(data.has("ResiRegion-readable"))? data.getString("ResiRegion-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieVillage=(data.has("ResiVillage-readable"))? data.getString("ResiVillage-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomiciliePostCode=(data.has("ResiZIP"))? data.getString("ResiZIP").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieStatusofBuilding=(data.has("ResiBuildingOwnership-readable"))? data.getString("ResiBuildingOwnership-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieGarage=(data.has("ResiGarage-readable"))? data.getString("ResiGarage-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieLocation=(data.has("ResiLocation-readable"))? data.getString("ResiLocation-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieOtherLocation=(data.has("ResiLocationOther"))? data.getString("ResiLocationOther").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieAccessLocation=(data.has("ResiAccess-readable"))? data.getString("ResiAccess-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String DomicilieBuildingEnviroment=(data.has("ResiBuildingCondition-readable"))? data.getString("ResiBuildingCondition-readable").replaceAll("['\"\\\\]", "\\\\$0") : "";

        JSONObject checkRecordSL=dao.getDataByQuery("SELECT BranchID,SurveyOrderID,FPPNo FROM dsf_mst_survey_location " +
                "WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"' ");
        if(checkRecordSL.getInt("count")>0){
            queries.add(i,"UPDATE dsf_mst_survey_location SET " +
                    "isSurveyDomicilie='"+isSurveyDomicilie+"'," +
                    "DomicilieAddress='"+DomicilieAddress+"'," +
                    "DomicilieRT='"+DomicilieRT+"'," +
                    "DomicilieRW='"+DomicilieRW+"'," +
                    "DomicilieCities='"+DomicilieCities+"'," +
                    "DomicilieProvince='"+DomicilieProvince+"'," +
                    "Domiciliedistricts='"+Domiciliedistricts+"'," +
                    "DomicilieVillage='"+DomicilieVillage+"'," +
                    "DomiciliePostCode='"+DomiciliePostCode+"'," +
                    "DomicilieStatusofBuilding='"+DomicilieStatusofBuilding+"'," +
                    "DomicilieGarage='"+DomicilieGarage+"'," +
                    "DomicilieOtherLocation='"+DomicilieLocation+"'," +
                    "DomicilieLocation='"+DomicilieOtherLocation+"'," +
                    "DomicilieBuildingEnviroment='"+DomicilieBuildingEnviroment+"'," +
                    "DomicilieAccessLocation='"+DomicilieAccessLocation+"'," +
                    "isSurveyOffice='"+isSurveyOffice+"'," +
                    "OfficeAddress='"+OfficeAddress+"'," +
                    "OfficeRT='"+OfficeRT+"'," +
                    "OfficeRW='"+OfficeRW+"'," +
                    "OfficeVillage='"+OfficeVillage+"'," +
                    "Officedistricts='"+Officedistricts+"'," +
                    "OfficeCities='"+OfficeCities+"'," +
                    "OfficeProvince='"+OfficeProvince+"'," +
                    "OfficePostCode='"+OfficePostCode+"'," +
                    "OfficeStatusofBuilding='"+OfficeStatusofBuilding+"'," +
                    "OfficeLocation='"+OfficeLocation+"'," +
                    "OfficeOtherLocation='"+OfficeOtherLocation+"'," +
                    "OfficeAccessLocation='"+OfficeAccessLocation+"'," +
                    "OfficeTotalofEmployee='"+OfficeTotalofEmployee+"'" +
                    " WHERE BranchID='"+branchID+"' AND SurveyOrderID='"+orderID+"' AND FPPNo='"+fppNo+"'");
            i++;
        }else{
            queries.add(i,"INSERT INTO dsf_mst_survey_location (" +
                    "BranchID," +
                    "SurveyOrderID," +
                    "FPPNo," +
                    "isSurveyDomicilie," +
                    "DomicilieAddress," +
                    "DomicilieRT," +
                    "DomicilieRW," +
                    "DomicilieCities," +
                    "DomicilieProvince," +
                    "Domiciliedistricts," +
                    "DomicilieVillage," +
                    "DomiciliePostCode," +
                    "DomicilieStatusofBuilding," +
                    "DomicilieGarage," +
                    "DomicilieLocation," +
                    "DomicilieOtherLocation," +
                    "DomicilieBuildingEnviroment," +
                    "DomicilieAccessLocation," +
                    "isSurveyOffice," +
                    "OfficeAddress," +
                    "OfficeRT," +
                    "OfficeRW," +
                    "Officedistricts," +
                    "OfficeCities," +
                    "OfficeProvince," +
                    "OfficeVillage," +
                    "OfficePostCode," +
                    "OfficeStatusofBuilding," +
                    "OfficeLocation," +
                    "OfficeOtherLocation," +
                    "OfficeAccessLocation," +
                    "OfficeTotalofEmployee" +
                    ")VALUES(" +
                    "'"+branchID+"'," +
                    "'"+orderID+"'," +
                    "'"+fppNo+"'," +
                    "'"+isSurveyDomicilie+"'," +
                    "'"+DomicilieAddress+"'," +
                    "'"+DomicilieRT+"'," +
                    "'"+DomicilieRW+"'," +
                    "'"+DomicilieCities+"'," +
                    "'"+DomicilieProvince+"'," +
                    "'"+Domiciliedistricts+"'," +
                    "'"+DomicilieVillage+"'," +
                    "'"+DomiciliePostCode+"'," +
                    "'"+DomicilieStatusofBuilding+"'," +
                    "'"+DomicilieGarage+"'," +
                    "'"+DomicilieLocation+"'," +
                    "'"+DomicilieOtherLocation+"'," +
                    "'"+DomicilieBuildingEnviroment+"'," +
                    "'"+DomicilieAccessLocation+"'," +
                    "'"+isSurveyOffice+"'," +
                    "'"+OfficeAddress+"'," +
                    "'"+OfficeRT+"'," +
                    "'"+OfficeRW+"'," +
                    "'"+Officedistricts+"'," +
                    "'"+OfficeCities+"'," +
                    "'"+OfficeProvince+"'," +
                    "'"+OfficeVillage+"'," +
                    "'"+OfficePostCode+"'," +
                    "'"+OfficeStatusofBuilding+"'," +
                    "'"+OfficeLocation+"'," +
                    "'"+OfficeOtherLocation+"'," +
                    "'"+OfficeAccessLocation+"'," +
                    "'"+OfficeTotalofEmployee+"'" +
                    ")");
            i++;
        }
        queries.add(i,"update dsf_mobile_orders set status='97'," +
                "isCustomerCalled='1'," +
                "isCustomerMet='1'," +
                "dblinkStatus='C'," +
                "recommendation='"+Recomendation+"'," +
                "updatedDate='"+Utils.dateFull()+"'," +
                "surveyResult='"+SurveyResult+"' " +
                "WHERE orderID='"+orderID+"'");
        return queries;
    }
    public ArrayList<String> batalSurvey(Dao dao,JSONObject data){
        ArrayList<String> queries=new ArrayList<>();
        String orderID=(data.has("survey-orderId"))? data.getString("survey-orderId").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String SurveyResult=(data.has("SurveyResult"))? data.getString("SurveyResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Recomendation=(data.has("Recomendation"))? data.getString("Recomendation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        int i=0;
        queries.add(i,"update dsf_mobile_orders set status='93', " +
                "isCustomerCalled='1'," +
                "isCustomerMet='1'," +
                "dblinkStatus='C'," +
                "recommendation='"+Recomendation+"'," +
                "updatedDate='"+Utils.dateFull()+"'," +
                "surveyResult='"+SurveyResult+"' " +
                "WHERE orderID='"+orderID+"'");
        return queries;
    }
    public ArrayList<String> pickupDocument(Dao dao,JSONObject data){
        ArrayList<String> queries=new ArrayList<>();
        String SurveyResult=(data.has("SurveyResult"))? data.getString("SurveyResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String orderID=(data.has("survey-orderId"))? data.getString("survey-orderId").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String planDate=(data.has("expectedDate"))? data.getString("expectedDate").replaceAll("['\"\\\\]", "\\\\$0") : "";
        int i=0;
        queries.add(i,"update dsf_mobile_orders set status='90', " +
                "isCustomerCalled='1'," +
                "isCustomerMet='1'," +
                "updatedDate='"+Utils.dateFull()+"'," +
                "surveyResult='"+SurveyResult+"'," +
                "newPlanDate='"+planDate+"' " +
                "WHERE orderID='"+orderID+"'");
        return queries;
    }
    public ArrayList<String> tundaKunjungan(Dao dao,JSONObject data){
        ArrayList<String> queries=new ArrayList<>();
        String orderID=(data.has("survey-orderId"))? data.getString("survey-orderId").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String planDate=(data.has("expectedDate"))? data.getString("expectedDate").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String isCalled=(data.has("isCalled"))? data.getString("isCalled").replaceAll("['\"\\\\]", "\\\\$0") : "0";
        String isMet=(data.has("isMet"))? data.getString("isMet").replaceAll("['\"\\\\]", "\\\\$0") : "0";
        String SurveyResult=(data.has("SurveyResult"))? data.getString("SurveyResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Recomendation=(data.has("Recomendation"))? data.getString("Recomendation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        int i=0;
        queries.add(i,"update dsf_mobile_orders set status='91'," +
                "newPlanDate='"+planDate+"'," +
                "updatedDate='"+Utils.dateFull()+"'," +
                "isCustomerCalled='"+isCalled+"', " +
                "isCustomerMet='"+isMet+"', " +
                "recommendation='"+Recomendation+"'," +
                "surveyResult='"+SurveyResult+"' " +
                "WHERE orderID='"+orderID+"'");
        return queries;
    }
    public ArrayList<String> ttdKontrak(Dao dao,JSONObject data){
        String SurveyResult=(data.has("SurveyResult"))? data.getString("SurveyResult").replaceAll("['\"\\\\]", "\\\\$0") : "";
        String Recomendation=(data.has("Recomendation"))? data.getString("Recomendation").replaceAll("['\"\\\\]", "\\\\$0") : "";
        ArrayList<String> queries=new ArrayList<>();
        String orderID=(data.has("survey-orderId"))? data.getString("survey-orderId").replaceAll("['\"\\\\]", "\\\\$0") : "";
        int i=0;
        queries.add(i,"update dsf_mobile_orders set status='92', " +
                "dblinkStatus='C'," +
                "isCustomerCalled='1'," +
                "isCustomerMet='1'," +
                "updatedDate='"+Utils.dateFull()+"'," +
                "recommendation='"+Recomendation+"'," +
                "surveyResult='"+SurveyResult+"' " +
                "WHERE orderID='"+orderID+"'");
        return queries;
    }
}
