package com.indocyber.dipostarsurveyserver;
/* Created by Indocyber on 11/09/2017.
*/
import org.json.JSONObject;
import spark.Request;
import spark.Response;

/**
 * Created by Indocyber on 08/07/2017.
 */
public class Errors {
    public static String error500(Request request , Response response){
        response.type("application/json");
        response.status(500);
        JSONObject json=new JSONObject();
        json.put("status",false);
        json.put("message","500 Internal Server Error");
        return json.toString();
    }
    public static String error403(){
        JSONObject json=new JSONObject();
        json.put("status",false);
        json.put("message","Not Enough Access");
        return json.toString();
    }
    public static String error404(Request request , Response response){
        response.status(404);
        response.type("application/json");
        JSONObject json=new JSONObject();
        json.put("status",false);
        json.put("message","404 No End Point Found");
        return json.toString();
    }
}
