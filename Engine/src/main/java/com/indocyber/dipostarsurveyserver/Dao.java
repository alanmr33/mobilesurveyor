package com.indocyber.dipostarsurveyserver;

/**
 * Created by Indocyber on 11/09/2017.
 */
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Indocyber on 07/07/2017.
 */
public class Dao {
    public Configuration configuration=new Configuration();
    private JSONObject responseData;
    private Connection connection=null;
    public Dao(){
        connection = null;
        connect();
    }
    public void connect(){
        try
        {
            Class.forName(configuration.className);
            /*
            open connection data
             */
            connection = DriverManager.getConnection(configuration.url,configuration.username, configuration.password);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            System.exit(2);
        }
    }
    public JSONObject getDataByQuery(String query){
        responseData = new JSONObject();
        try {
            Statement command=connection.createStatement();
            if(configuration.databaseLog) {
                System.out.println("========Query Select========");
                System.out.println(query);
            }
            ResultSet resultSet=command.executeQuery(query);
            ResultSetMetaData resultSetMetaData=resultSet.getMetaData();
            /*
            store result to json array
             */
            JSONArray result=new JSONArray();
            while (resultSet.next()){
                JSONObject json=new JSONObject();
                /*
                column mapping to json data
                 */
                for(int i=1;i<=resultSetMetaData.getColumnCount();i++){
                    if(!resultSetMetaData.getColumnName(i).equals("row")) {
                        json.put(resultSetMetaData.getColumnName(i), resultSet.getString(resultSetMetaData.getColumnName(i)));
                    }
                }
                result.put(json);
            }
            /*
            put json data result
             */
            responseData.put("status",true);
            responseData.put("data",result);
            responseData.put("count",result.length());
        } catch (SQLException e) {
            /*
            put json error
             */
            if(configuration.databaseLog) {
                System.out.println("Error :"+e.getMessage().toString());
            }
            responseData.put("status",false);
            responseData.put("message","Database Server Error");
            responseData.put("reason",e.getMessage());
        }
        return responseData;
    }
    public JSONObject crudQuery(String query) {
          /*
        create response
         */
        responseData = new JSONObject();
        try {
            Statement command = connection.createStatement();
            if(configuration.databaseLog) {
                System.out.println("===========Query Update, Insert & Delete========");
                System.out.println(query);
            }
            command.executeUpdate(query);
            responseData.put("status", true);
            responseData.put("message", "Operation Success");
        } catch (SQLException e) {
            /*
            put json error
             */
            if(configuration.databaseLog) {
                System.out.println("Error :"+e.getMessage().toString());
            }
            responseData.put("status",false);
            responseData.put("message","Database Server Error");
            responseData.put("reason",e.getMessage());
        }
        return responseData;
    }
    public JSONObject crudQueries(ArrayList<String> queries) {
        responseData = new JSONObject();
        try {
            connection.setAutoCommit(false);
            Statement command = connection.createStatement();
            System.out.println("===========TRANSACTION START========");
            for (int i=0;i<queries.size();i++) {
                if (configuration.databaseLog) {
                    System.out.println(queries.get(i));
                }
                command.executeUpdate(queries.get(i));
            }
            System.out.println("===========TRANSACTION END========");
            connection.commit();
            responseData.put("status", true);
            responseData.put("message", "Operation Success");
        } catch (SQLException e) {
            /*
            put json error
             */
            if(configuration.databaseLog) {
                System.out.println("Error :"+e.getMessage().toString());
            }
            responseData.put("status", false);
            responseData.put("message", "Database Server Error");
        }
        return responseData;
    }
    public JSONObject callProcedure(String procedure, String[] strings,Boolean select) {
        responseData = new JSONObject();
        try {
            String query="{ CALL "+procedure+"(";
            for (int i=0;i<strings.length;i++){
                if(i==0){
                    query+="'"+strings[i]+"'";
                }else{
                    query+=",'"+strings[i]+"'";
                }
            }
            query+=") }";
            CallableStatement command = connection.prepareCall(query);
            if(configuration.databaseLog) {
                System.out.println("===========CALL PROCEDURE========");
                System.out.println(query);
            }
            JSONArray result=new JSONArray();
            if(select==null || !select){
                command.executeUpdate();
            }else{
                command.executeQuery();
                ResultSet resultSet= command.getResultSet();
                ResultSetMetaData resultSetMetaData=resultSet.getMetaData();
                /*
                store result to json array
                 */
                while (resultSet.next()){
                    JSONObject json=new JSONObject();
                /*
                column mapping to json data
                 */
                    for(int c=1;c<=resultSetMetaData.getColumnCount();c++){
                        if(!resultSetMetaData.getColumnName(c).equals("row")) {
                            json.put(resultSetMetaData.getColumnName(c), resultSet.getString(resultSetMetaData.getColumnName(c)));
                        }
                    }
                    result.put(json);
                }
            }
            responseData.put("status", true);
            responseData.put("message", "Operation Success");
            responseData.put("data", result);
        } catch (SQLException e) {
            /*
            put json error
             */
            if(configuration.databaseLog) {
                System.out.println("Error :"+e.getMessage().toString());
            }
            responseData.put("status",false);
            responseData.put("message","Database Server Error");
            responseData.put("reason",e.getMessage());
            responseData.put("data", new JSONArray());
        }
        return responseData;
    }
    public Connection getConnection(){
        return connection;
    }
    public void close(){
        try {
            if(!connection.isClosed()){
               connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

