package com.indocyber.dipostarsurveyserver;

import net.coobird.thumbnailator.Thumbnails;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.*;
import java.sql.ResultSet;

/**
 * Created by Indocyber on 13/03/2018.
 */
public class JobResize implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        try {
            Dao dao = new Dao();
            int resized=0;
            int unresized=0;
            System.out.println("==========CRON JOB START==========");
            System.out.println("Scheduler Resize Start @ "+Utils.dateFull());
            dao.crudQuery("EXECUTE sp_dsf_dipolink_orders");
            JSONObject query = dao
                    .getDataByQuery("select orderID, spkFilename from dsf_mobile_orders " +
                            "where status='95'");
            if(query.getBoolean("status")) {
                JSONArray data = query.getJSONArray("data");
                System.out.println("Total   :  "+data.length());
                for (int i = 0; i < data.length(); i++) {
                    if (data.getJSONObject(i).has("spkFilename")) {
                        String extension = data.getJSONObject(i).getString("spkFilename");
                        int lengthExten = extension.length();
                        int awalGetExten = lengthExten - 4;
                        String exten = extension.substring(awalGetExten, lengthExten);
                        File file = File.createTempFile("tempDownload", exten);
                        File file2 = File.createTempFile("cob", exten);
                        if (exten.equals(".jpg") || exten.equals(".png") || exten.equals(".gif")) {
                            String queryx = "";
                            byte[] fileBytes;
                            java.sql.PreparedStatement pstmt;
                            queryx = ("SELECT spkFile FROM dsf_mobile_orders WHERE orderID = '" +
                                    data.getJSONObject(i).getString("orderID") + "'");
                            java.sql.Statement state = dao.getConnection().createStatement();
                            ResultSet rs = state.executeQuery(queryx);
                            if (rs.next()) {
                                fileBytes = rs.getBytes(1);
                                OutputStream targetFile = new FileOutputStream(file);
                                targetFile.write(fileBytes);
                                targetFile.close();
                                Thumbnails.of(file)
                                        .size(400, 400)
                                        .outputQuality(0.8)
                                        .toFile(file2);
                            }
                            InputStream streamDataResaize = new FileInputStream(file2);
                            String query2;
                            java.sql.PreparedStatement pstmt2;
                            query2 = ("update dsf_mobile_orders "
                                    + "Set "
                                    + "status = ? , "
                                    + "spkFile = ? "
                                    + "where orderID = ?");
                            pstmt2 = dao.getConnection().prepareStatement(query2);
                            pstmt2.setString(1, "87");
                            pstmt2.setBinaryStream(2, streamDataResaize);
                            pstmt2.setString(3, data.getJSONObject(i).getString("orderID"));
                            pstmt2.executeUpdate();
                            streamDataResaize.close();
                            resized++;
                        } else {
                            String query2;
                            java.sql.PreparedStatement pstmt2;
                            query2 = ("update dsf_mobile_orders "
                                    + "Set "
                                    + "status = ? "
                                    + "where orderID = ?");
                            pstmt2 = dao.getConnection().prepareStatement(query2);
                            pstmt2.setString(1, "87");
                            pstmt2.setString(2, data.getJSONObject(i).getString("orderID"));
                            pstmt2.executeUpdate();
                            unresized++;
                        }
                    } else {
                        String query3;
                        java.sql.PreparedStatement pstmt3;
                        query3 = ("update dsf_mobile_orders "
                                + "Set "
                                + "status = ? "
                                + "where orderID = ?");
                        pstmt3 = dao.getConnection().prepareStatement(query3);
                        pstmt3.setString(1, "87");
                        pstmt3.setString(2, data.getJSONObject(i).getString("orderID"));
                        pstmt3.executeUpdate();
                        unresized++;
                    }
                }
            }else {
                System.err.println(query);
            }
            System.out.println("Resized   :  "+resized);
            System.out.println("UnResized :  "+unresized);
            System.out.println("Scheduler Resize Done @ "+Utils.dateFull());
            System.out.println("==========JOB END==========");
            dao.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
